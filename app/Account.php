<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table ='account';
    protected $fillable=[
        'code',
        'name' ,
        'category' ,
        'account_group' ,
        'print_order' ,
        'before_word',
        'after_word' ,
        'active'
    ];
    public $primaryKey='code';
    public $incrementing=false;
    public $timestamps=false;

    public function accountCategories(){

        return $this->belongsTo(AccountCategory::class,'category','code');
    }

}
