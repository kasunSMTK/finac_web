<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCategory extends Model
{
    protected $table='account_category';
    protected $fillable=[
        'code',
        'name' ,
        'parent' ,
        'type' ,
        'path'

    ];
    public $primaryKey='code';
    public $incrementing=false;
    public $timestamps=false;
}
