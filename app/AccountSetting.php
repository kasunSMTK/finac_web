<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSetting extends Model
{
    protected $table='account_setting';
   protected $fillable=[
       'code',
       'name' ,
       'transaction_type' ,
       'credit_or_debit' ,
       'account' ,
       'active'
   ];
    public $primaryKey='code';
    public $incrementing=false;
    public $timestamps=false;

    public function accounts(){

        return $this->belongsTo(Account::class,'account','code');
    }
}
