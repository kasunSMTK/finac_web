<?php

namespace App;

use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class AccountTransaction extends Model
{
    protected $table = 'account_transaction';
    protected $fillable = [
        'index_no',
        'transaction_date',
        'branch',
        'account_setting',
        'description',
        'account',
        'credit_amount',
        'debit_amount',
        'transaction',
        'transaction_type',
        'type',
        'loan',
        'status'
    ];
    public $primaryKey = 'index_no';
    public $incrementing = false;
    public $timestamps = false;

    public function accounts()
    {

        return $this->belongsTo(Account::class, 'account', 'code');
    }

    public function accountSettings()
    {

        return $this->belongsTo(AccountSetting::class, 'account_setting', 'code');
    }

    public function setAccountTransaction($transaction_type, $setting_code, $description, $amount, $transaction, $AUTO)
    {

            $account_setting = (new AccountSetting())->
            where('code', $setting_code)->
            where('transaction_type', $transaction_type)->
            where('active', 1)->first();

            $table_col = [];
            if ($account_setting->credit_or_debit == 'CREDIT') {

                $table_col = [
                    'transaction_date' => Session::get('working_date'),
                    'branch' => Session::get('branch_id'),
                    'account_setting' => $account_setting->code,
                    'description' => $description,
                    'account' => $account_setting->account,
                    'credit_amount' => $amount,
                    'debit_amount' => 0.0,
                    'transaction' => $transaction,
                    'transaction_type' => $transaction_type,
                    'type' => $AUTO,
                    'status' => ApplicationVarible::$ACTIVE,
                ];

            } else {
                $table_col = [
                    'transaction_date' => Session::get('working_date'),
                    'branch' => Session::get('branch_id'),
                    'account_setting' => $account_setting->code,
                    'description' => $description,
                    'account' => $account_setting->account,
                    'credit_amount' => 0.0,
                    'debit_amount' => $amount,
                    'transaction' => $transaction,
                    'transaction_type' => $transaction_type,
                    'type' => $AUTO,
                    'status' => ApplicationVarible::$ACTIVE,
                ];

            }
            $this->create($table_col);




    }

    public function setManualAccountTransaction(
        $transaction_type,
        $account,
        $credit_or_debit,
        $description,
        $amount,
        $transaction,
        $AUTO)
    {

        $table_col = [];
        if ($credit_or_debit === 'CREDIT') {

            $table_col = [
                'transaction_date' => Session::get('working_date'),
                'branch' => Session::get('branch_id'),
                'account_setting' => null,
                'description' => $description,
                'account' => $account,
                'credit_amount' => $amount,
                'debit_amount' => 0.0,
                'transaction' => $transaction,
                'transaction_type' => $transaction_type,
                'type' => $AUTO,
                'status' => ApplicationVarible::$ACTIVE,
            ];

        } else {
            $table_col = [
                'transaction_date' => Session::get('working_date'),
                'branch' => Session::get('branch_id'),
                'account_setting' => null,
                'description' => $description,
                'account' => $account,
                'credit_amount' => 0.0,
                'debit_amount' => $amount,
                'transaction' => $transaction,
                'transaction_type' => $transaction_type,
                'type' => $AUTO,
                'status' => ApplicationVarible::$ACTIVE,
            ];

        }
        $this->create($table_col);
    }


    public function PaymentAccountTransaction(
        $transaction,
        $transaction_type,
        $description,
        $setting_code,
        $amount,
        $AUTO,
        $settlement,
        $advanced_payment,
        $bank_account = null)
    {
        $i = 0;
        $penalty_total = 0.0;
        $payment_amount = (floatval($amount) - floatval($advanced_payment)) > 0 ?
            (floatval($amount) - floatval($advanced_payment)) :
            0.0;

        if (isset($settlement)) {
            foreach ($settlement as $key => $row) {
                foreach ($row as $kay1 => $row1) {
                    foreach ($row1 as $kay2 => $row2) {
                        foreach ($row2 as $kay3 => $row3) {
                            foreach ($row3 as $kay4 => $row5) {


                                if (isset($row5)) {

                                    switch ($kay4) {

                                        case "LOAN_PANALTY":
                                            $penalty_total += floatval($row5);
                                            break;
                                    }
                                    $i++;
                                }
                            }
                        }
                    }
                }
            }

        }


        $payment_setting = (new PaymentSetting())->
        where('transaction_type', $transaction_type)->
        where('active', 1)->get();

        $table_col = [];
      //  if (isset($payment_setting)) {

            foreach ($payment_setting as $Payments_list) {
                switch ($Payments_list['type']) {

                    case "CASH":
                        if ($Payments_list['credit_or_debit'] === 'CREDIT') {

                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => $amount,
                                'debit_amount' => 0.0,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => $AUTO,
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => 0.0,
                                'debit_amount' => $amount,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => $AUTO,
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        }

                        if (isset($advanced_payment) && $advanced_payment > 0) {
                            //credit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$RECEIPT_ADVANCED_CREDIT_CODE,
                                "Cash Receipt Advanced Amount",
                                $advanced_payment,
                                $transaction,
                                $AUTO
                            );
                        }
                        $this->create($table_col);

                        // penalty due

                        if ($penalty_total > 0) {
                            //credit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$RECEIPT_PANELTY_CREDIT_CODE,
                                "Penalty Amount credit",
                                $penalty_total,
                                $transaction,
                                $AUTO
                            );
                            //debit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$RECEIPT_INT_PNL_DEBIT_CODE,
                                "Penalty Amount debit",
                                $penalty_total,
                                $transaction,
                                $AUTO
                            );
                        }
                        break;
                    case "BANK":
                        if ($bank_account != null) {
                            if ($Payments_list['credit_or_debit'] === 'CREDIT') {

                                $table_col = [
                                    'transaction_date' => Session::get('working_date'),
                                    'branch' => Session::get('branch_id'),
                                    'account_setting' => null,
                                    'description' => $Payments_list['name'],
                                    'account' => $bank_account,
                                    'credit_amount' => $amount,
                                    'debit_amount' => 0.0,
                                    'transaction' => $transaction,
                                    'transaction_type' => $transaction_type,
                                    'type' => $AUTO,
                                    'status' => ApplicationVarible::$ACTIVE,
                                ];

                            } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                                $table_col = [
                                    'transaction_date' => Session::get('working_date'),
                                    'branch' => Session::get('branch_id'),
                                    'account_setting' => null,
                                    'description' => $Payments_list['name'],
                                    'account' => $bank_account,
                                    'credit_amount' => 0.0,
                                    'debit_amount' => $amount,
                                    'transaction' => $transaction,
                                    'transaction_type' => $transaction_type,
                                    'type' => $AUTO,
                                    'status' => ApplicationVarible::$ACTIVE,
                                ];

                            }
                        }

                        if (isset($advanced_payment) && $advanced_payment > 0) {
                            //credit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$BRECEIPT_ADVANCED_CREDIT_CODE,
                                "Bank Receipt Advanced Amount",
                                $advanced_payment,
                                $transaction,
                                $AUTO
                            );
                        }
                        $this->create($table_col);

                        // penalty due

                        if ($penalty_total > 0) {
                            //credit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$RECEIPT_PANELTY_CREDIT_CODE,
                                "Penalty Amount credit",
                                $penalty_total,
                                $transaction,
                                $AUTO
                            );
                            //debit
                            $this->setAccountTransaction(
                                $transaction_type,
                                AccountInterface::$RECEIPT_INT_PNL_DEBIT_CODE,
                                "Penalty Amount debit",
                                $penalty_total,
                                $transaction,
                                $AUTO
                            );
                        }
                        break;
                    case "CHEQUE":
                        if ($Payments_list['credit_or_debit'] === 'CREDIT') {

                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => $amount,
                                'debit_amount' => 0.0,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => $AUTO,
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => 0.0,
                                'debit_amount' => $amount,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => $AUTO,
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        }


                        $this->create($table_col);
                        break;

                }


            }

        $this->setAccountTransaction($transaction_type, $setting_code, $description, $payment_amount, $transaction, $AUTO);


    }

    public function ChequeIssueAccountTransaction(
        $transaction,
        $transaction_type,
        $description,
        $amount,
        $AUTO,
        $settlement,
        $advanced_payment)
    {
        $i = 0;
        $penalty_total = 0.0;
        $payment_amount = (floatval($amount) - floatval($advanced_payment)) > 0 ?
            (floatval($amount) - floatval($advanced_payment)) :
            0.0;

        if (isset($settlement)) {
            foreach ($settlement as $key => $row) {
                foreach ($row as $kay1 => $row1) {
                    foreach ($row1 as $kay2 => $row2) {
                        foreach ($row2 as $kay3 => $row3) {
                            foreach ($row3 as $kay4 => $row5) {


                                if (isset($row5)) {

                                    switch ($kay4) {

                                        case "LOAN_PANALTY":
                                            $penalty_total += floatval($row5);
                                            break;
                                    }
                                    $i++;
                                }
                            }
                        }
                    }
                }
            }

        }/*end */
        $this->setAccountTransaction($transaction_type, AccountInterface::$CHEQUE_RECEIPT_AMOUNT_DEBIT_CODE, $description, $amount, $transaction, $AUTO);
        $this->setAccountTransaction($transaction_type, AccountInterface::$CHEQUE_RECEIPT_AMOUNT_CREDIT_CODE, $description, $payment_amount, $transaction, $AUTO);

        if (isset($advanced_payment) && $advanced_payment > 0) {
            //credit
            $this->setAccountTransaction(
                $transaction_type,
                AccountInterface::$CHEQUE_RECEIPT_ADVANCED_CREDIT_CODE,
                "Cheque Receipt Advanced Amount",
                $advanced_payment,
                $transaction,
                $AUTO
            );
        }

        // penalty due
        if ($penalty_total > 0) {
            //credit
            $this->setAccountTransaction(
                $transaction_type,
                AccountInterface::$CHEQUE_RECEIPT_PANELTY_CREDIT_CODE,
                "Penalty Amount credit",
                $penalty_total,
                $transaction,
                $AUTO
            );
            //debit
            $this->setAccountTransaction(
                $transaction_type,
                AccountInterface::$CHEQUE_RECEIPT_INT_PNL_DEBIT_CODE,
                "Penalty Amount debit",
                $penalty_total,
                $transaction,
                $AUTO
            );
        }








    }
    public function MixPaymentAccountTransaction(
        $request,
        $transaction,
        $transaction_type,
        $description,
        $setting_code,
        $pay_amount,
        $cash,
        $cheque,
        $account,
        $bank)
    {
        $i = 0;
        $penalty_total = 0.0;
        $payment_amount = (floatval($pay_amount));

        $account_code="";
        $pay_amount=0.0;
        $ch_amount = 0.0;



        $payment_setting = (new PaymentSetting())->
        where('transaction_type', $transaction_type)->
        where('active', 1)->get();

        $table_col = [];
        $table_col1 = [];
        $table_col2 = [];

        $pay_information=[];

        foreach ($payment_setting as $Payments_list) {
            switch ($Payments_list['type']) {

                case "CASH":
                    if(isset($cash) && floatval($cash)>0) {


                        if ($Payments_list['credit_or_debit'] === 'CREDIT') {

                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => $cash,
                                'debit_amount' => 0.0,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                            $table_col = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => 0.0,
                                'debit_amount' => $cash,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        }
                        $this->create($table_col);

                    }
                    break;
                case "ACCOUNT":
                    if ($account != null) {

                        foreach ($account as $key => $raw){




                            if($key == "account"){
                                $account_code = $raw;
                            }else if($key == "payment_account"){
                                $pay_amount = $raw;

                            }


                        }

                        if ($Payments_list['credit_or_debit'] === 'CREDIT') {

                            $table_col1 = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $account_code,
                                'credit_amount' => $pay_amount,
                                'debit_amount' => 0.0,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                            $table_col1 = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $account_code,
                                'credit_amount' => 0.0,
                                'debit_amount' => $pay_amount,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        }

                        $this->create($table_col1);

                    }

                    break;
                case "CHEQUE":
                    if(isset($cheque) && sizeof($cheque)>0) {

                        if ($Payments_list['credit_or_debit'] === 'CREDIT') {


                            foreach ($cheque as $key => $raw) {
                                foreach ($raw as $key1 => $raw1) {

                                    if ($key1 == "amount") {
                                        $ch_amount += floatval($raw1);
                                    }

                                }

                            }


                            $table_col2 = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => $ch_amount,
                                'debit_amount' => 0.0,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];

                        } else if ($Payments_list['credit_or_debit'] === 'DEBIT') {
                            $table_col2 = [
                                'transaction_date' => Session::get('working_date'),
                                'branch' => Session::get('branch_id'),
                                'account_setting' => null,
                                'description' => $Payments_list['name'],
                                'account' => $Payments_list['account'],
                                'credit_amount' => 0.0,
                                'debit_amount' => $ch_amount,
                                'transaction' => $transaction,
                                'transaction_type' => $transaction_type,
                                'type' => 'AUTO',
                                'status' => ApplicationVarible::$ACTIVE,
                            ];


                        }
                        $this->create($table_col2);


                    }
                    break;

            }


        }
            //payment information

        $payment_object = (new Payment())->storePayment(
            $request->session()->get('branch_id'),
            $request->client_code,
            $request->session()->get('cashier_session'),
            $cash,
            $ch_amount, floatval($pay_amount)>0?$pay_amount:0, $bank,
            $transaction,
            ApplicationVarible::$VOUCHER_TRANSACTION_CODE
        );

        if(isset($cheque) && sizeof($cheque)>0) {
            //cheque information
            (new Cheque())->companyStore(
                $request->cheque_information,
                $transaction,
                ApplicationVarible::$VOUCHER_TRANSACTION_CODE,
                $request->client_code,
                $payment_object->index_no,
                ApplicationVarible::$COMPANY,
                ApplicationVarible::$CHQ_PENDING
            );
        }

        $this->setAccountTransaction($transaction_type, $setting_code, $description, $payment_amount, $transaction, 'AUTO');


    }



}
