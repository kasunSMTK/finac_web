<?php

namespace App;

use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class AdvancedPayment extends Model
{
    protected $table = 'advanced_payment';
    protected $fillable = [
        'index_no',
        'transaction_date',
        'branch',
        'client',
        'loan',
        'installment_no',
        'transaction',
        'transaction_type',
        'description',
        'amount',
        'balance_amount',
        'due_date',
        'settlement_type',
        'account',
        'send',
        'status'
    ];
    public $primaryKey = 'index_no';
    public $incrementing = false;
    public $timestamps = false;

    public function storeAdvancedPayment($settlement, $loan, $transaction, $client)
    {
        $i = 0;
        $advanced_total=0.0;
        foreach ($settlement as $key => $row) {
            foreach ($row as $kay1 => $row1) {
                foreach ($row1 as $kay2 => $row2) {
                    foreach ($row2 as $kay3 => $row3) {
                        foreach ($row3 as $kay4 => $row5) {

                            //    dd($kay1 . '   ' . $kay2 . ' s ' . $kay3 . ' b ' . $kay4 . ' t ' . $row5);
                            if (isset($row5)) {
                                $due_date = date($kay3);
                                $working_date = date(Session::get('working_date'));
                                if ($due_date > $working_date) {
                                    switch ($kay4) {
                                        case "LOAN_INTEREST":
                                            $advanced_payment = [
                                                "transaction_date" => Session::get('working_date'),
                                                'branch' => Session::get('branch_id'),
                                                'client' => $client,
                                                'loan' => $loan,
                                                'installment_no' => null,
                                                'transaction' => $transaction,
                                                'transaction_type' => "LOAN",
                                                'description' => "Loan Interest",
                                                'amount' => $row5,
                                                'balance_amount' => $row5,
                                                'due_date' => $kay3,
                                                'settlement_type' => ApplicationVarible::$LOAN_INTEREST,
                                                'account' => "31-100",
                                                'send' => "0",
                                                'status' => "ACTIVE",
                                            ];
                                            $this->create($advanced_payment);
                                            $advanced_total+=floatval($row5);
                                            break;
                                        case "LOAN_CAPITAL":

                                            $advanced_payment = [
                                                "transaction_date" => Session::get('working_date'),
                                                'branch' => Session::get('branch_id'),
                                                'client' => $client,
                                                'loan' => $loan,
                                                'installment_no' => null,
                                                'transaction' => $transaction,
                                                'transaction_type' => "LOAN",
                                                'description' => "Loan Capital",
                                                'amount' => $row5,
                                                'balance_amount' => $row5,
                                                'due_date' => $kay3,
                                                'settlement_type' => ApplicationVarible::$LOAN_CAPITAL,
                                                'account' => "11-100",
                                                'send' => "0",
                                                'status' => "ACTIVE",
                                            ];
                                            $this->create($advanced_payment);
                                            $advanced_total+=floatval($row5);
                                            break;
                                    }
                                }
                                $i++;
                            }
                        }
                    }
                }
            }
        }
        return $advanced_total;

    }

}
