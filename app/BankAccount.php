<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $fillable=[
        'code' ,
        'owner' ,
        'account_number' ,
        'bank_branch' ,
        'value_account' ,
        'pending_account' ,
        'active'
    ];
    public $table='bank_account';
    public $incrementing=false;
    public $timestamps = false;
    public $primaryKey = 'code';

    public function BankBranch(){

        return $this->belongsTo(BankBranch::class,'bank_branch');
    }
    public function ValueAccount(){

        return $this->belongsTo(Account::class,'value_account');
    }
    public function PendingAccount(){

        return $this->belongsTo(Account::class,'pending_account');
    }
}
