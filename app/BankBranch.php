<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankBranch extends Model
{
  public $fillable=[
      'code' ,
      'name' ,
      'bank'
  ];
  public $table='bank_branch';
  public $incrementing=false;
  public $primaryKey = 'code';
  public $timestamps = false;
}
