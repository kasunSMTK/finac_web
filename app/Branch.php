<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table='branch';
    public $primaryKey='code';
    public $timestamps=false;
    public $incrementing=false;

    protected $fillable=[
        'code',
        'prefix' ,
        'name' ,
        'address_line1' ,
        'address_line2' ,
        'address_line3' ,
        'hotline',
        'telephone1' ,
        'telephone2' ,
        'fax' ,
        'email' ,
        'note' ,
        'working_date',
        'company' ,
        'active',
        'up_id'

    ];






}
