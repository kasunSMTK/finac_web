<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashierPoint extends Model
{
    public $table='cashier_point';
    protected $fillable=
        [
        'index_no' ,
        'name' ,
        'cashier_account' ,
        'main_account' ,
        'terminal'];

    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='index_no';

}
