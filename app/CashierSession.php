<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashierSession extends Model
{
    public $table='cashier_session';
    protected $fillable=
        [
            'index_no' ,
            'transaction_date' ,
            'cashier_point' ,
            'employee' ,
            'start_time' ,
            'end_time' ,
            'status'];

    public $timestamps=false;
    public $incrementing=false;
    protected $primaryKey='index_no';


}
