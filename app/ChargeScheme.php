<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChargeScheme extends Model
{
    protected $table='charge_scheme';

    public $primaryKey='code';
    public $incrementing = false;


    public function loanTypeChargeSchemes(){

        return $this->hasMany(LoanTypeChargeScheme::class,'charge_scheme','code');
    }

}
