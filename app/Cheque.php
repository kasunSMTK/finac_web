<?php

namespace App;

use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Cheque extends Model
{
    public $fillable = [
        'index_no',
        'branch',
        'client',
        'bank_branch',
        'payment_object',
        'account_no',
        'cheque_no',
        'amount',
        'cheque_date',
        'deposit_date',
        'realize_date',
        'return_date',
        'deposit_account',
        'status',
        'type'
    ];
    public $table = 'cheque';
   // public $incrementing = false;
    public $primaryKey = 'index_no';
    public $timestamps = false;

    public function  paymentObject(){

        return $this->belongsTo(PaymentInformation::class,'payment_object');
    }
    public function bankAccount(){
        return $this->belongsTo(BankAccount::class,'deposit_account');
    }
    public function clients(){

        return $this->belongsTo(Client::class,'client');
    }

    public function store(
        $cheque_information,
        $transaction,
        $transaction_type,
        $client,
        $payment_object,
        $type,
        $status,
        $deposit_date = null,
        $realize_date = null,
        $return_date = null,
        $deposit_account = null
    )
    {
         $bank_branch="";
         $account_no="";
         $cheque_no="";
         $amount=0;
         $cheque_date="";



        foreach ($cheque_information as $key => $row) {

            foreach ($row as $key1 => $val) {

                switch ($key1){
                    case "bank_branch":
                        $bank_branch = $val;
                        break;
                    case "account_no":
                        $account_no = $val;
                        break;
                    case "cheque_no":
                        $cheque_no = $val;
                        break;
                    case "amount":
                        $amount = $val;
                        break;
                    case "cheque_date":
                        $cheque_date = $val;
                        break;
                }


            }

            $table_set = [
                'branch' => Session::get('branch_id'),
                'client' => $client,
                'bank_branch'=>$bank_branch,
                'payment_object'=>$payment_object,
                'account_no'=>$account_no,
                'cheque_no'=>$cheque_no,
                'amount'=>$amount,
                'cheque_date'=>$cheque_date,
                'deposit_date'=>$deposit_date,
                'realize_date'=>$realize_date,
                'return_date'=>$return_date,
                'deposit_account'=>$deposit_account,
                'status'=>$status,
                'type'=>$type
            ];

            $cheque_index =   $this->create($table_set);

            (new ChequeDetails())->store(
                $cheque_index->index_no,
                $transaction,
                $transaction_type,
                $amount
            );

        }


    }
    public function companyStore(
        $cheque_information,
        $transaction,
        $transaction_type,
        $client,
        $payment_object,
        $type,
        $status,
        $deposit_date = null,
        $realize_date = null,
        $return_date = null,
        $deposit_account = null
    )
    {
        $bank_branch="";
        $account_no="";
        $cheque_no="";
        $amount=0;
        $cheque_date="";



        foreach ($cheque_information as $key => $row) {

            foreach ($row as $key1 => $val) {

                switch ($key1){
                    case "account_no":
                        $account_no = $val;
                        break;
                    case "cheque_no":
                        $cheque_no = $val;
                        break;
                    case "amount":
                        $amount = $val;
                        break;
                    case "cheque_date":
                        $cheque_date = $val;
                        break;
                }


            }

            $bank_account_details = (new BankAccount())->where('code',$account_no)->first();


            if(isset($bank_account_details)) {


                $table_set = [
                    'branch' => Session::get('branch_id'),
                    'client' => $client,
                    'bank_branch' => $bank_account_details->bank_branch,
                    'payment_object' => $payment_object,
                    'account_no' => $bank_account_details->account_number,
                    'cheque_no' => $cheque_no,
                    'amount' => $amount,
                    'cheque_date' => $cheque_date,
                    'deposit_date' => $deposit_date,
                    'realize_date' => $realize_date,
                    'return_date' => $return_date,
                    'deposit_account' => $deposit_account,
                    'status' => $status,
                    'type' => $type
                ];

                $cheque_index = $this->create($table_set);

                (new ChequeDetails())->store(
                    $cheque_index->index_no,
                    $transaction,
                    $transaction_type,
                    $amount
                );

                return true;
            }else{
                return false;
            }

        }


    }
    public function changeStatus($index){

        $information = (new PaymentInformation())->information($index);

        foreach($information as $list){

            $cheque = (new Cheque())->where('payment_object', $list['index_no'])->first();
            $cheque->status = ApplicationVarible::$CANCEL;
            $cheque->update();
        }



    }
}
