<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class ChequeDetails extends Model
{
    public $fillable=[
        'index_no' ,
        'transaction_date',
        'cheque_index' ,
        'transaction' ,
        'description',
        'amount',
        'status'
    ];
    public $table='cheque_details';
  //  public $incrementing=false;
    public $primaryKey = 'index_no';
    public $timestamps = false;

    public function cheque(){

        return $this->belongsTo(Cheque::class,'cheque_index');
    }

    public function store(
        $cheque_index,
        $transaction,
        $description,
        $amount
    ){

         $table_set=[
            'transaction_date'=>Session::get('working_date'),
            'cheque_index' =>$cheque_index,
            'transaction' =>$transaction,
            'description'=>$description,
            'amount'=>$amount,
            'status'=>"ACTIVE"
        ];
     $this->create($table_set);


    }


}
