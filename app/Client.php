<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Client extends Model
{
    use Notifiable;
    protected $fillable = [
        'code',
        'nic_no',
        'salutation',
        'name',
        'long_name',
        'route',
        'address_line1',
        'address_line2',
        'address_line3',
        'mobile',
        'telephone1',
        'telephone2',
        'fax',
        'email',
        'note',
        'maximum_allocation',
        'approved',
        'client',
        'supplier',
        'star_marks',
        'active',
        'status'
        // add all other fields
    ];

    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'code';

    public $table = 'client';

    public function router(){
       return $this->belongsTo(Route::class,'route','code');
    }

    public function toString(){

        return $this->code . ' - ' .$this->name;
    }




}
