<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLeaveDay extends Model
{
    protected $table='company_leave_day';
    public $primaryKey='code';
    public $incrementing=false;

    public $fillable=[
        'code',
        'name',
        'leave_date'
    ];

    function getLeaveDays($loan_date,$edate)
    {

        $from = date($loan_date);
        $to = date($edate);



        $leave_days = (new CompanyLeaveDay())
            ->select('leave_date')
            ->whereBetween('leave_date', [$from, $to])
            ->get();

        return $leave_days;

    }





}
