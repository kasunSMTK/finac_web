<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table='document';
    public $primaryKey='code';
    public $timestamps=false;
    /*public $incrementing=false;*/

    protected $fillable=[
        'code' ,
        'description' ,
        'owned_date' ,
        'released' ,
        'released_date' ,
        'reference_link' ,
        'note'

    ];
}
