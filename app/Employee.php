<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable=[
        'code' ,
        'nic_no' ,
        'salutation',
        'name',
        'long_name' ,
        'prefix' ,
        'address_line1' ,
        'address_line2',
        'address_line3' ,
        'mobile' ,
        'telephone1' ,
        'telephone2' ,
        'fax' ,
        'email' ,
        'note' ,
        'branch' ,
        'user_role' ,
        'login_access' ,
        'user_name' ,
        'password' ,
        'photo' ,
        'active' ,
        'is_officer',
        'user_register_id'
    ];
    protected $table='employee';
    public $timestamps=false;
    public $primaryKey='code';
    public  $incrementing=false;

    public function empbranch(){

        return $this->belongsTo(Branch::class,'branch','code')->where('active','1');
    }
}
