<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\AdvancedPayment;
use App\Loan;
use App\Payment;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PhpParser\Node\Expr\Array_;

class BankPaymentController extends Controller
{
    public function index(Request $request)
    {

        $transaction_type=ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE;
        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            where('available_receipt', '1')->
            orderBy('index_no')->
            get();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$RECEIPT_PREFIX);

            $account = (new Account())->selectRaw('code,concat(name , code)as name')->
            where('account_group','BANK_ACCOUNT')->
            where('active',1)->orderBy('name')->pluck('name','code');
            return view('transaction.payments.bank_index', compact('loan', 'referenceNo','account','transaction_type'));
        } else {

            // alert error

        }

    }


    public function store(Request $request)
    {

        $this->validate(Request(), [
            'agreement_no' => 'required',
            'payment_amount' => 'required',
            'payment_amount_confirmation' => ['required_with:payment_amount','same:payment_amount'],
            'account' => 'required'
        ]);


        try {
            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Bank Payment'
            );
            //settlement
            $settlement_ary = (new Settlement())->updateSettlementAmount(
                $request->loan_index,
                $request->settlement);
            //settlement history
            (new SettlementHistory())->saveSettlementHistories(
                $settlement_ary,
                $transaction->index_no,
                ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE,
                $request->session()->get('working_date')
            );
            //over payment

            (new Settlement())->saveOverPayments(
                $request->session()->get('branch_id'),
                $request->client_code,
                $request->loan_index,
                $transaction->index_no,
                ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE,
                $request->overAmount
            );
            //payment information

            (new Payment())->storePayment(
                $request->session()->get('branch_id'),
                $request->client_code,
                $request->session()->get('cashier_session'),
                0,
                0, 0, $request->payment_amount,
                $transaction->index_no,
                ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE
            );
            //advance payment

            $advanced_payment= (new AdvancedPayment())->storeAdvancedPayment(
                $request->settlement,
                $request->loan_index,
                $transaction->index_no,
                $request->client_code
            );


            //accounts transaction

            (new AccountTransaction())->PaymentAccountTransaction(
                $transaction->index_no,
                ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE,
                "Receipt Bank Payment",
                AccountInterface::$RECEIPT_AMOUNT_CRDIT_CODE,
                $request->payment_amount,
                AccountInterface::$TYPE_AUTO,
                $request->settlement,
                $advanced_payment,
                $request->account
            );


            DB::commit();

            return ["save", config('app.FLD')."bank_payment",$transaction->index_no,ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }
}
