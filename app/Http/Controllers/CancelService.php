<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\AdvancedPayment;
use App\Payment;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\Util\ApplicationVarible;
use Illuminate\Http\Request;

class  CancelService extends Controller
{
    public function cancel($index)
    {
        $cancelService = (new CancelService());

        $cancelService->cancelTransaction($index);

        $cancelService->cancelSettlementHistory($index);

        $cancelService->cancelAccountTransaction($index);

        $cancelService->cancelPaymentTransaction($index);

        $cancelService->cancelAdvancedPayment($index);
    }

    public function cancelTransaction($index_no)
    {
        $transaction = (new Transaction())->find($index_no);
        $transaction->status = ApplicationVarible::$CANCEL;
        $transaction->update();
    }

    public function cancelSettlementHistory($index_no)
    {

        $settlementHistories = (new SettlementHistory())->
        where('transaction', $index_no)->
        with('settlements')->
        get();

        foreach ($settlementHistories as $list) {

            $settlement = (new Settlement())->find($list->settlements->index_no);
            $settlement->balance_amount = floatval($list->settlement_amount) + floatval($list->settlements->balance_amount);
            $settlement->status = ApplicationVarible::$PENDING;


            $settlement->update();

            $updateHistories = (new SettlementHistory())->find($list->index_no);
            $updateHistories->status = ApplicationVarible::$CANCEL;
            $updateHistories->update();
        }


    }

    public function cancelAccountTransaction($index_no)
    {

        $AccountTransaction = (new AccountTransaction())->
        where('transaction', $index_no)->
        get();
        foreach ($AccountTransaction as $list) {

            $updateTransaction = (new AccountTransaction())->find($list->index_no);
            $updateTransaction->status = ApplicationVarible::$CANCEL;
            $updateTransaction->update();
        }


    }

    public function cancelPaymentTransaction($index_no)
    {

        $payment = (new Payment())->
        where('transaction', $index_no)->
        first();

        $payment->status = ApplicationVarible::$CANCEL;
        $payment->update();
    }

    public function cancelAdvancedPayment($index_no)
    {

        $AdvancedPayment = (new AdvancedPayment())->
        where('transaction', $index_no)->
        get();

        foreach($AdvancedPayment as $list){

            $updatePayment = (new AdvancedPayment())->find($list->index_no);

            $updatePayment->status = ApplicationVarible::$CANCEL;
            $updatePayment->update();
        }
    }

}
