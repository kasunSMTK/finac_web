<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\AdvancedPayment;
use App\Loan;
use App\Payment;
use App\ReportXml;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use App\Util\PHPJasperXML;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PhpParser\Node\Expr\Array_;

class CashPaymentController extends Controller
{

    public function index(Request $request)
    {

        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$RECEIPT;
            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            where('available_receipt', '1')->
            orderBy('index_no')->
            get();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$RECEIPT_PREFIX);
            return view('transaction.payments.cash_index', compact('loan', 'referenceNo','transaction_type'));
        } else {

            // alert error

        }

    }


    public function store(Request $request)
    {

        $this->validate(Request(), [
            'agreement_no' => 'required',
            'payment_amount' => 'required',
            'payment_amount_confirmation' => ['required_with:payment_amount', 'same:payment_amount'],
        ]);

        try {
            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$RECEIPT,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Cash Payment'
            );
            //settlement
            $settlement_ary = (new Settlement())->updateSettlementAmount(
                $request->loan_index,
                $request->settlement);
            //settlement history
            (new SettlementHistory())->saveSettlementHistories(
                $settlement_ary,
                $transaction->index_no,
                ApplicationVarible::$RECEIPT,
                $request->session()->get('working_date')
            );
            //over payment

            (new Settlement())->saveOverPayments(
                $request->session()->get('branch_id'),
                $request->client_code,
                $request->loan_index,
                $transaction->index_no,
                ApplicationVarible::$RECEIPT,
                $request->overAmount
            );
            //payment information

            (new Payment())->storePayment(
                $request->session()->get('branch_id'),
                $request->client_code,
                $request->session()->get('cashier_session'),
                $request->payment_amount,
                0, 0, 0,
                $transaction->index_no,
                ApplicationVarible::$RECEIPT
            );
            //advance payment

            $advanced_payment = (new AdvancedPayment())->storeAdvancedPayment(
                $request->settlement,
                $request->loan_index,
                $transaction->index_no,
                $request->client_code
            );


            //accounts transaction

            (new AccountTransaction())->PaymentAccountTransaction(
                $transaction->index_no,
                ApplicationVarible::$RECEIPT,
                "Receipt Cash Payment",
                AccountInterface::$RECEIPT_AMOUNT_CREDIT,
                $request->payment_amount,
                AccountInterface::$TYPE_AUTO,
                $request->settlement,
                $advanced_payment
            );


            DB::commit();
          //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
            return ["save", config('app.FLD')."cash_payment",$transaction->index_no,ApplicationVarible::$RECEIPT];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }

    /*public function reports($transaction_no,$transaction_type)
    {

        (new Common())->reportGenerate($transaction_no,$transaction_type);

    }*/

}
