<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\BankAccount;
use App\BankBranch;
use App\Cheque;
use App\ChequeDetails;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ChequeDepositController extends Controller
{
   public function index(Request $request){

       $common = new Common();
       $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$CHEQUE_DEPOSIT_PREFIX);
       $cheque = (new BankAccount())->selectRaw('code,concat(account_number )as name')->
       orderBy('name')->pluck('name','code');


   return view('account.cheque_transaction.deposit_index',compact('referenceNo','cheque'));
   }




}
