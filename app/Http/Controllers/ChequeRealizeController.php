<?php

namespace App\Http\Controllers;

use App\BankAccount;
use App\Transaction;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChequeRealizeController extends Controller
{
    public function index(Request $request){

        $common = new Common();
        $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$CHEQUE_REALIZE_PREFIX);
        $cheque = (new BankAccount())->selectRaw('code,concat(account_number )as name')->
        orderBy('name')->pluck('name','code');


        return view('account.cheque_transaction.realize_index',compact('referenceNo','cheque'));
    }



}
