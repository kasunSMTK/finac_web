<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\BankAccount;
use App\Cheque;
use App\ChequeDetails;
use App\PaymentInformation;
use App\TemperaryReceipt;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ChequeTransactionController extends Controller
{

    public function generateTopTableData(Request $request)
    {

        switch ($request->transaction_type) {

            case "CHEQUE_DEPOSIT";
                $data_table = (new Cheque())->
                where('status', ApplicationVarible::$CHQ_PENDING)->
                where('type', ApplicationVarible::$CLIENT)->
                with('clients')->
                get();
                break;
            case "CHEQUE_REALIZE";
                $data_table = (new Cheque())->
                where('status', ApplicationVarible::$CHQ_DEPOSIT)->
                where('type', ApplicationVarible::$CLIENT)->
                where('deposit_account', $request->bank_account)->
                with('clients')->
                get();
                break;
            case "CHEQUE_RETURN";
                $data_table = (new Cheque())->
                where('status', ApplicationVarible::$CHQ_DEPOSIT)->
                where('type', ApplicationVarible::$CLIENT)->
                where('deposit_account', $request->bank_account)->
                with('clients')->
                get();
                break;
        }


        return compact('data_table');
    }

    public function store(Request $request)
    {


        $this->validate(Request(), [
            "table_data" => "present|array|min:2",
            "bank_branch" => "required"
        ]);


        /*dd($request->all());*/

        $redirect_page="";
        try {
            DB::beginTransaction();


            if (isset($request->transaction_type)) {

                switch ($request->transaction_type) {


                    case "CHEQUE_DEPOSIT" :
                    //transaction
                    $transaction = (new Transaction())->saveTransaction(
                        $request->session()->get('working_date'),
                        ApplicationVarible::$CHEQUE_DEPOSIT_TRANSACTION_CODE,
                        $request->reference_no,
                        $request->document_no,
                        null,
                        $request->session()->get('branch_id'),
                        null,
                        ApplicationVarible::$ACTIVE
                    );

                    // transaction history
                    (new TransactionHistory())->saveTransactionHistory(
                        $transaction->index_no,
                        'NEW',
                        $request->session()->get('user_id'),
                        'Cheque Deposit'
                    );

                    //cheque
                    for ($i = 1; $i < sizeof($request->table_data); $i++) {

                        $cheque = (new Cheque())->find($request->table_data[$i][0]);
                        $cheque->status = ApplicationVarible::$CHQ_DEPOSIT;
                        $cheque->deposit_account = $request->bank_branch;
                        $cheque->deposit_date = $request->transaction_date;
                        $cheque->update();

                        //cheque details
                        (new ChequeDetails())->store(
                            $request->table_data[$i][0],
                            $transaction->index_no,
                            ApplicationVarible::$CHEQUE_DEPOSIT_TRANSACTION_CODE,
                            $request->table_data[$i][3]
                        );

                        $account_object = (new BankAccount())->
                        where('code', $request->bank_branch)->
                        first();

                        (new AccountTransaction())->setManualAccountTransaction(
                            ApplicationVarible::$CHEQUE_DEPOSIT_TRANSACTION_CODE,
                            $account_object->pending_account,
                            AccountInterface::$DEBIT,
                            "Cheque Deposit From " . $account_object->pending_account,
                            $request->table_data[$i][3],
                            $transaction->index_no,
                            AccountInterface::$TYPE_AUTO
                        );

                        (new AccountTransaction())->setAccountTransaction(
                            ApplicationVarible::$CHEQUE_DEPOSIT_TRANSACTION_CODE,
                            AccountInterface::$DEPOSIT_AMOUNT_CREDIT_CODE,
                            "Cheque Deposit Amount",
                            $request->table_data[$i][3],
                            $transaction->index_no,
                            AccountInterface::$TYPE_AUTO
                        );
                    }

                        $redirect_page="cheque_deposit";

                    break;

                    case "CHEQUE_REALIZE" :
                        //transaction
                        $transaction = (new Transaction())->saveTransaction(
                            $request->session()->get('working_date'),
                            ApplicationVarible::$CHEQUE_REALIZE_TRANSACTION_CODE,
                            $request->reference_no,
                            $request->document_no,
                            null,
                            $request->session()->get('branch_id'),
                            null,
                            ApplicationVarible::$ACTIVE
                        );

                        // transaction history
                        (new TransactionHistory())->saveTransactionHistory(
                            $transaction->index_no,
                            'NEW',
                            $request->session()->get('user_id'),
                            'Cheque Realize'
                        );

                        //cheque
                        for ($i = 1; $i < sizeof($request->table_data); $i++) {

                            $cheque = (new Cheque())->find($request->table_data[$i][0]);
                            $cheque->status = ApplicationVarible::$CHQ_REALIZE;
                            $cheque->realize_date = $request->transaction_date;
                            $cheque->update();

                            //cheque details
                            (new ChequeDetails())->store(
                                $request->table_data[$i][0],
                                $transaction->index_no,
                                ApplicationVarible::$CHEQUE_REALIZE_TRANSACTION_CODE,
                                $request->table_data[$i][3]
                            );

                            //temporary receipt
                            $transaction_no=$cheque->paymentObject->transaction;

                            $temp_receipt=(new TemperaryReceipt())->where('transaction',$transaction_no)->first();
                            $temp_receipt->status=ApplicationVarible::$REALIZE;
                            $temp_receipt->update();

                            //account

                            $account_object = (new BankAccount())->
                            where('code', $request->bank_branch)->
                            first();

                            (new AccountTransaction())->setManualAccountTransaction(
                                ApplicationVarible::$CHEQUE_REALIZE_TRANSACTION_CODE,
                                $account_object->pending_account,
                                AccountInterface::$CREDIT,
                                "Cheque realize From " . $account_object->pending_account,
                                $request->table_data[$i][3],
                                $transaction->index_no,
                                AccountInterface::$TYPE_AUTO
                            );
                            (new AccountTransaction())->setManualAccountTransaction(
                                ApplicationVarible::$CHEQUE_REALIZE_TRANSACTION_CODE,
                                $account_object->value_account,
                                AccountInterface::$DEBIT,
                                "Cheque realize From " . $account_object->value_account,
                                $request->table_data[$i][3],
                                $transaction->index_no,
                                AccountInterface::$TYPE_AUTO
                            );


                        }
                        $redirect_page="cheque_realize";



                        break;

                        case "CHEQUE_RETURN" :
                        //transaction
                        $transaction = (new Transaction())->saveTransaction(
                            $request->session()->get('working_date'),
                            ApplicationVarible::$CHEQUE_RETURN_TRANSACTION_CODE,
                            $request->reference_no,
                            $request->document_no,
                            null,
                            $request->session()->get('branch_id'),
                            null,
                            ApplicationVarible::$ACTIVE
                        );

                        // transaction history
                        (new TransactionHistory())->saveTransactionHistory(
                            $transaction->index_no,
                            'NEW',
                            $request->session()->get('user_id'),
                            'Cheque Return'
                        );

                        //cheque
                        for ($i = 1; $i < sizeof($request->table_data); $i++) {

                            $cheque = (new Cheque())->find($request->table_data[$i][0]);
                            $cheque->status = ApplicationVarible::$CHQ_RETURN;
                            $cheque->return_date = $request->transaction_date;
                            $cheque->update();

                            //cheque details
                            (new ChequeDetails())->store(
                                $request->table_data[$i][0],
                                $transaction->index_no,
                                ApplicationVarible::$CHEQUE_RETURN_TRANSACTION_CODE,
                                $request->table_data[$i][3]
                            );

                            //temporary receipt
                            $transaction_no=$cheque->paymentObject->transaction;

                            $temp_receipt=(new TemperaryReceipt())->where('transaction',$transaction_no)->first();
                            $temp_receipt->status=ApplicationVarible::$CANCEL;
                            $temp_receipt->update();

                            //account

                            $account_object = (new BankAccount())->
                            where('code', $request->bank_branch)->
                            first();

                            (new AccountTransaction())->setAccountTransaction(
                                ApplicationVarible::$CHEQUE_RETURN_TRANSACTION_CODE,
                                AccountInterface::$RETURN_AMOUNT_DEBIT_CODE,
                                "Cheque Return Amount",
                                $request->table_data[$i][3],
                                $transaction->index_no,
                                AccountInterface::$TYPE_AUTO
                            );
                            (new AccountTransaction())->setManualAccountTransaction(
                                ApplicationVarible::$CHEQUE_RETURN_TRANSACTION_CODE,
                                $account_object->pending_account,
                                AccountInterface::$CREDIT,
                                "Cheque Return From " . $account_object->pending_account,
                                $request->table_data[$i][3],
                                $transaction->index_no,
                                AccountInterface::$TYPE_AUTO
                            );


                        }
                        $redirect_page="cheque_return";



                        break;

                }
                DB::commit();
                return ["save",config('app.FLD').$redirect_page];
            }


        } catch (Exception $e) {

            DB::rollback();
            return $e;
        }


    }


}
