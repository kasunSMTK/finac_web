<?php

namespace App\Http\Controllers;

use App\Client;
use App\Notifications\NotifyCustomer;
use App\Route;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Mockery\Exception;
use File;


class ClientController extends Controller
{
    public function index()
    {

        /*$Clients = Client::all();*/
        $Clients = Client::with('router')->orderBy('name')->get();
        /* @Client::paginate('20'); */

        return view('registration.client.index', compact('Clients'));

    }

    public function create()
    {

        $client = new Client();
//        $route = (new Route())->orderBy('code')->get(['code','name']);
        $route = (new Route())->orderBy('name')->pluck('name', 'code');
        return view('registration.client.create', compact('client', 'route'));

    }

    public function store(Request $request)
    {
        /*  $Clients = new Customer($request->all());

          $Clients->save();*/

        $this->validate($request, [
            //put fields to be validated here
            'code' => 'required|unique:client',
            'nic_no' => 'required|max:13',
            'name' => 'required|max:30',
            'maximum_allocation' => 'numeric',
            'p_image' => 'mimes:jpeg,jpg,png|max:2048'
        ]);


        try {

            DB::beginTransaction();

            $client = new Client($request->all());
            $client->approved = 0;
            $client->client = 1;
            $client->supplier = 0;
            $client->active = 0;
            $client->status = 'ACTIVE';
            $client->save();

            //image save
            $path = public_path() . '\\user\\image';


            if (!file_exists($path)) {

                File::makeDirectory($path, $mode = 0777, true, true);

            } else if ($request->hasfile('p_image')) {


                $path2 = public_path() . '\\user\\image\\';


                $imageName = $request->code . '.' . 'png';
                $request->p_image->move($path2, $imageName);


            }

            $client = Client::find($request->code);
            Client::find($request->code)->notify(new NotifyCustomer($client));

            DB::commit();
            return redirect('clients/create')->with('alert', 'save!');
        } catch (Exception $ex) {
            DB::rollback();
            return $ex;
        }

    }

    public function edit($code)
    {


        $client = (new Client())->find($code);
        $route = (new Route())->orderBy('name')->pluck('name', 'code');

        return view('registration.client.edit', compact('client', 'route'));

    }

    public function update(Request $request, $code)
    {

        $this->validate($request, [
            //put fields to be validated here
            'code' => 'required',
            'nic_no' => 'required|max:13',
            'name' => 'required|max:30',
            'maximum_allocation' => 'numeric',
            'p_image' => 'mimes:jpeg,jpg,png|max:2048'
        ]);

        try {


            $client = (new Client())->find($code);
            $client->update($request->all());


            //image save
            $path = public_path() . '\\user\\image';



            if (!file_exists($path)) {

                File::makeDirectory($path, $mode = 0777, true, true);

            } else if ($request->hasfile('p_image')) {


                $path2 = public_path() . '\\user\\image\\';


                $imageName = $request->code . '.' . 'png';
                $request->p_image->move($path2, $imageName);


            }
            $client = Client::find($code);
            Client::find($code)->notify(new NotifyCustomer($client));

            return redirect('clients')->with('alert', 'Updated!');
        } catch (Exception $e) {
            return $e;
        }


    }

    public function getClients(Request $request)
    {
        $guarantors = (new Client())->find($request->code);
        return compact('guarantors');
    }

}
