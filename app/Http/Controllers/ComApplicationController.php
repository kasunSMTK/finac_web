<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Loan;
use App\Settlement;
use App\TemperaryReceipt;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComApplicationController extends Controller
{


   /* public function referenceNoGenerate(Request $request)
    {

        $user = $request->session()->get('user_id');

        $prefix =(new Employee())->select('prefix')->where('code',$user)->first();

        $ldate = substr(date('YmdHis'), 2);

        $reference = "LON".$prefix. $ldate;

      //  var_dump('111  ' . $reference);
        return $reference;

    }*/
    public function getData(Request $request)
    {


        $data_list = (new Loan())->where('agreement_no', $request->agreement_no)->first();
        $type_editble_text =false;

        $temporary_receipt=[];
        $settlement = (new Settlement())->
        where('loan', $data_list->index_no)->
        where('status', 'PENDING')->
        whereHas('settlementType', function ($query) {
            $query->where('credit_or_debit', 'CREDIT');
        })->orderBy('due_date')->with('settlementType')
            ->get();

        if($request->transaction_type==ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE){
            $temporary_receipt = (new TemperaryReceipt())->
            where('status', ApplicationVarible::$REALIZE)->
            where('loan',$data_list->index_no)->
            orderBy('index_no')->
            first();

            $request->session()->put('temporary_receipt', $temporary_receipt->index_no);
            $type_editble_text =true;

        }else{
            $temporary_receipt=[];
            $type_editble_text =false;

        }

        return compact('data_list', 'settlement','temporary_receipt','type_editble_text');
    }

    public function getDataVoucher(Request $request)
    {


        $data_list = (new Loan())->where('agreement_no', $request->agreement_no)->first();



        $settlement = (new Settlement())->
        where('loan', $data_list->index_no)->
        where('status', 'PENDING')->
        where('settlement_type', 'LOAN_AMOUNT')->
        whereHas('settlementType', function ($query) {
            $query->where('credit_or_debit', 'DEBIT');
        })->orderBy('due_date')->with('settlementType')
            ->get();



        return compact('data_list', 'settlement');
    }
    public function UpdateLoan($loan_index){

        if($this->changeReceiptAvailability($loan_index)){
            $loan = (new Loan())->findOrFail($loan_index);

            $loan->available_receipt = 0;
            $loan->update();
        }else{
       /*     $loan = (new Loan())->findOrFail($loan_index);

            $loan->available_receipt = 1;
            $loan->update();*/
        }

    }

    public function changeReceiptAvailability($loan_index){
        $settlement = (new Settlement())->
        select(DB::raw('sum(amount) as amount'),DB::raw('sum(balance_amount) as balance_amount'))->
        where('status',ApplicationVarible::$PENDING)->
        where('loan',$loan_index)->
        whereHas('settlementType',function ($query){
            $query->where('credit_or_debit',AccountInterface::$CREDIT);
        })->first();

        if(doubleval($settlement->balance_amount)<=0.0){

            return true;
        }
        return false;
    }
    public function getDataRebate(Request $request)
    {


        $data_list = (new Loan())->where('agreement_no', $request->agreement_no)->first();

        $settlement = (new Settlement())->
        where('loan', $data_list->index_no)->
        where('status', 'PENDING')->
        where('settlement_type', '<>','LOAN_CAPITAL')->
        whereHas('settlementType', function ($query) {
            $query->where('credit_or_debit', 'CREDIT');
        })->orderBy('due_date')->with('settlementType')
            ->get();




        return compact('data_list', 'settlement');
    }
}
