<?php

namespace App\Http\Controllers;

use App\CompanyLeaveDay;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class CompanyLeaveDayController extends Controller
{


    function getLeaveDays(Request $request)
    {


/*        $from = Carbon::parse($request->loanDate)->startOfDay();
        $to = Carbon::parse($request->endDate)->endOfDay();*/

        $fdate = $request->loanDate;
        $edate = $request->endDate;

      $from = date($fdate);
        $to = date($edate);



        $leave_days = (new CompanyLeaveDay())
            ->select('leave_date')
            ->whereBetween('leave_date', [$from, $to])
            ->get();


/*    var_dump("sss  ".$leave_days);*/

        /*return json_encode($leave_days);*/
        return compact('leave_days');

    }


}
