<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\AdvancedPayment;
use App\BankBranch;
use App\Loan;
use App\Payment;
use App\Settlement;
use App\SettlementHistory;
use App\TemperaryReceipt;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PhpParser\Node\Expr\Array_;

class IssueReceiptChequeController extends Controller
{
    public function index(Request $request)
    {

        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE;
            $loan = (new TemperaryReceipt())->
            where('status', ApplicationVarible::$REALIZE)->
            orderBy('index_no')->
            get();

            /*foreach ($temporary_receipt as $value){

                array_push($loan,$value->loans);

            }*/
            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$RECEIPT_PREFIX);

            return view('transaction.payments.cheque_issue_index', compact('loan', 'referenceNo','transaction_type'));
        } else {

            // alert error

        }

    }
    public function store(Request $request){


        $this->validate(Request(), [
            'agreement_no' => 'required',
            'chq_payment_amount' => 'required',

        ]);

        try {

            //transaction
            if(!(intval($request->session()->get('temporary_receipt'))>0)){



                return "Save Failed Please Refresh Browser.";
            }else{
                DB::beginTransaction();

                $transaction = (new Transaction())->saveTransaction(
                    $request->session()->get('working_date'),
                    ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE,
                    $request->reference_no,
                    $request->document_no,
                    $request->loan_index,
                    $request->session()->get('branch_id'),
                    $request->client_code,
                    ApplicationVarible::$ACTIVE
                );


                (new TransactionHistory())->saveTransactionHistory(
                    $transaction->index_no,
                    'NEW',
                    $request->session()->get('user_id'),
                    'Cheque Payment'
                );
                //settlement
                $settlement_ary = (new Settlement())->updateSettlementAmount(
                    $request->loan_index,
                    $request->settlement);
                //settlement history
                (new SettlementHistory())->saveSettlementHistories(
                    $settlement_ary,
                    $transaction->index_no,
                    ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE,
                    $request->session()->get('working_date')
                );
                //over payment

                (new Settlement())->saveOverPayments(
                    $request->session()->get('branch_id'),
                    $request->client_code,
                    $request->loan_index,
                    $transaction->index_no,
                    ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE,
                    $request->overAmount
                );

                //advance payment

                $advanced_payment = (new AdvancedPayment())->storeAdvancedPayment(
                    $request->settlement,
                    $request->loan_index,
                    $transaction->index_no,
                    $request->client_code
                );


                //accounts transaction

                (new AccountTransaction())->ChequeIssueAccountTransaction(
                    $transaction->index_no,
                    ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE,
                    "Received cheque Payment",
                    $request->chq_payment_amount,
                    AccountInterface::$TYPE_AUTO,
                    $request->settlement,
                    $advanced_payment
                );
                //update loan

                (new ComApplicationController())->UpdateLoan($request->loan_index);

                //temporary receipt status change
                (new TemperaryReceipt())->updateStates(
                    $request->session()->get('temporary_receipt'),
                    ApplicationVarible::$SETTLED
                    );


                DB::commit();

                $request->session()->put('temporary_receipt', 0);
                //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
                return ["save", config('app.FLD')."cheque_payment_issue",$transaction->index_no,ApplicationVarible::$CHEQUE_RECEIPT_TRANSACTION_CODE];
            }
          //  var_dump($request->session()->get('temporary_receipt'));

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }

    }
}
