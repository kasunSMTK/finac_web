<?php

namespace App\Http\Controllers;

use App\Client;
use App\Loan;
use App\Util\ApplicationVarible;
use Illuminate\Http\Request;

class LoanApplicationApprovalController extends Controller
{

    public function index()
    {

        $approval_list = (new Loan())->select(
            'index_no',
            'application_reference_no',
            'application_document_no',
            'application_transaction_date',
            'client',
            'loan_type',
            'loan_amount',
            'installment_amount',
            'installment_count',
            'interest_rate',
            'panalty',
            'panalty_type',
            'panalty_rate',
            'panalty_amount',
            'grace_days',
            'reason')
            ->with('clientString')->with('loanTypeString')
            ->where('status',ApplicationVarible::$APPLICATION_PENDING)->orderBy('application_transaction_date')
            ->get();


        return view('transaction.loan_application.loan_approval.index',compact('approval_list'));
    }

    public function targetView(){

        return rederect('transaction.loan_application.loan_approval.loanApproveAction');
    }

    public function store(Request $request){

    }



    public function approve($index_no){

        $loan = (new Loan())->find($index_no);

        $loan->status = ApplicationVarible::$APPLICATION_ACCEPT;
        $loan->update();

        return redirect('loan_application_approval')->with('alert','Loan Application Approve Success!');

    }

    public function reject($index_no){

        $loan = (new Loan())->find($index_no);

        $loan->status = ApplicationVarible::$APPLICATION_REJECT;
        $loan->update();

        return redirect('loan_application_approval')->with('alert','Loan Application Approve Success!');



    }
}
