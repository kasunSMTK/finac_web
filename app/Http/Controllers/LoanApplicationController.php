<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Client;
use App\Document;
use App\Loan;
use App\loan_type;
use App\LoanDocument;
use App\LoanGroup;
use App\LoanGuarantor;
use App\LoanType;
use App\Settlement;
use App\Transaction;
use App\TransactionHistory;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;


class LoanApplicationController extends Controller
{
    private $getRefNo;


    public function index(Request $request)
    {

        $loanAppDetails = Loan::with('clients')
            ->whereIn('status', [ApplicationVarible::$APPLICATION_REJECT, ApplicationVarible::$APPLICATION_PENDING])->get();



       // $this->getRefNo = new ComApplicationController();

        return view('transaction.loan_application.index', compact('loanAppDetails'));

    }

    public function create(Request $request)
    {


        $loan = new Loan();
        $clients = (new Client())->selectRaw('code,concat(code," - ",name) as name')->where('active', '1')->orderBy('name')->pluck('name', 'code');
        $loanTypes = (new LoanType())->where('active', '1')->orderBy('name')->pluck('name', 'code');
        $loanGroups = (new LoanGroup())->orderBy('name')->pluck('name', 'code');
        $suppliers = (new Client())->where('supplier', '1')->orderBy('name')->pluck('name', 'code');




        /* $newval = $this->getRefNo->referenceNoGenerate($request);

         var_dump($newval );*/
        $common = new Common();
        $referenceNo = $common->referenceNoGenerate($request,ApplicationVarible::$LOAN_APPLICATION_PREFIX);
        $button_caption = 'Save';


        return view('transaction.loan_application.create',
            compact(
                'loan',
                'clients',
                'loanTypes',
                'loanGroups',
                'suppliers',
                'referenceNo',
                'button_caption'
            ));
    }

    public function store(Request $request)
    {

        $this->validate(Request(),
            [
                'client' => 'required|not_in',
                'expected_loan_date' => 'required',
                'loan_type' => 'required',
                'loan_amount' => 'required',
                'payment_term' => 'required',
                'interest_rate' => 'required',
                'installment_count' => 'required',
                'installment_amount' => 'required'
            ]
        );

        try {
            DB::beginTransaction();


            $newarray = [
                'application_reference_no' => $request->application_reference_no,
                'application_transaction_date' => Session::get('working_date'),
                'application_document_no' => $request->application_document_no == null || empty($request->application_document_no) ?
                    '' : $request->application_document_no,
                'client' => $request->client,
                'supplier' => $request->supplier,
                'loan_type' => $request->loan_type,
                'loan_group' => $request->loan_group,
                'loan_amount' => $request->loan_amount,
                'installment_amount' => $request->installment_amount,
                'installment_count' => $request->installment_count,
                'interest_rate' => $request->interest_rate,
                'expected_loan_date' => $request->expected_loan_date,
                'payment_term' => $request->payment_term,
                'panalty_rate' => $request->panalty_rate,
                'panalty' => $request->chk_Penalty == 'on' ? '1' : '0',
                'panalty_amount' => $request->panalty_amount == '' ? '0' : $request->panalty_amount,
                'grace_days' => $request->grace_days,
                'branch' => $request->session()->get('branch_id'), // session variable
                'available_receipt' => '0',
                'available_voucher' => '0',
                'profile' => 'LOAN',
                'situation' => 'LOAN',
                'status' => ApplicationVarible::$APPLICATION_PENDING,
                'print_do' => '0',
                'rebate_approve' => '0',
                'receipt_block' => '0',
                'is_npl' => '0',
                'reason' => $request->reason == null || empty($request->reason) ?
                    '' : $request->reason,
                'note' => $request->note == null || empty($request->note) ?
                    '' : $request->note

            ];


            $newloan = (new Loan())->create($newarray);
            $newloan->save();

            //transaction
            $transaction = [
                'transaction_date' => Session::get('working_date'),
                'transaction_type' => ApplicationVarible::$LOAN_APPLICATION,
                'reference_no' => $request->application_reference_no,
                'document_no' => $request->application_document_no == null || empty($request->application_document_no) ?
                    '' : $request->application_document_no,
                'loan' => $newloan->index_no,
                'branch' => $request->session()->get('branch_id'),
                'client' => $request->client,
                'status' => ApplicationVarible::$ACTIVE

            ];
            //  var_dump($newloan->index_no);
            $transactionSave = (new Transaction())->create($transaction);
            $transactionSave->save();

            //transaction history
            $transaction = (new TransactionHistory());

            $transaction->transaction = $transactionSave->index_no;
            $transaction->action = 'NEW';
            $transaction->employee = $request->session()->get('user_id'); //session variable employee code
            $transaction->note = 'Loan Application';

            $transaction->save();

            // settlement save

            if (isset($request->tbl_scheme)) {
                foreach ($request->tbl_scheme as $key => $row1) {
                    foreach ($row1 as $key1 => $row2) {
                        foreach ($row2 as $key2 => $row3) {
                            if ((float)$row3 > 0) {
                                $settlement = [
                                    'transaction_date' => Session::get('working_date'),
                                    'branch' => $request->session()->get('branch_id'), // session variable
                                    'client' => $request->client,
                                    'loan' => $newloan->index_no,
                                    'transaction' => $transactionSave->index_no,    // TRANSACTION
                                    'transaction_type' => ApplicationVarible::$LOAN_APPLICATION,
                                    'description' => $key2,
                                    'amount' => $row3,
                                    'balance_amount' => $row3,
                                    'due_date' => $request->application_transaction_date,
                                    'settlement_type' => ApplicationVarible::$APPLICATION_CHARGE,
                                    'status' => ApplicationVarible::$PENDING,
                                    'account' => $key1
                                ];

                                $settlementSave = (new Settlement())->create($settlement);
                                $settlementSave->save();
                            }
                        }

                    }
                }
            }

            //loan guarantor save

            $loanGuarantor = (new LoanGuarantor());


            if (isset($request->tbl_guarantor)) {

                foreach ($request->tbl_guarantor as $key => $row1) {

                    $loanGuarantor->loan = $newloan->index_no;
                    $loanGuarantor->guarantor = $key;

                    $loanGuarantor->save();

                }
            }

            //loan document save
            $filecount = 1;
            if (isset($request->tbl_documents)) {

                foreach ($request->tbl_documents as $key => $row1) {


                    $loan_document = (new LoanDocument());
                    $documentList = [
                        'description' => $key,
                        'owned_date' => Session::get('working_date'),
                        'reference_link' => $row1,
                        'note' => $newloan->index_no];

                    $document_result = (new Document())->create($documentList);
                    $document_result->save();

                    $loan_document->loan = $newloan->index_no;
                    $loan_document->document = $document_result->code;
                    $loan_document->save();

                    $path = public_path() . '/user/document/' . $request->application_reference_no;
                    if (!file_exists($path)) {

                        File::makeDirectory($path, $mode = 0777, true, true);
                    } else {
                        $files = File::files($path);

                        $filecount = count($files);
                        $filecount += 1;
                    }

                    // define('DIRECTORY', $path);

                    $content = file_get_contents('c://user_document/' . $row1);
                    File::put($path . '/' . $document_result->code  . '.png', $content);




                }
            }


            DB::commit();
            return ["save", config('app.FLD')."loan_application/create"];

        } catch (Exception $ex) {
            DB::rollback();

            return $ex;
        }


    }

    public function remove($index_no)
    {
        try{
            DB::beginTransaction();
            $loan = (new Loan())->find($index_no);
            $settlement = (new Settlement())->where('loan',$index_no)->first();
            $transaction = (new Transaction())->where('loan',$index_no)->first();

            $loan->status = ApplicationVarible::$APPLICATION_CANCEL;
            $settlement->status = ApplicationVarible::$CANCEL;
            $transaction->status = ApplicationVarible::$CANCEL;

            $settlement->update();
            $loan->update();
            $transaction->update();
            DB::commit();

            return redirect('loan_application')->with('alert', 'Loan Application Cancel Success!');
        }catch(Exception $e){

            return redirect('loan_application_approval')->with('alert','Loan Application Cancel Fail!');
            DB::rollBack();

        }


    }












//       $this->validate(Request(),
//            [
//                'client'=>'required',
//                'expected_loan_date'=>'required',
//                'loan_type'=>'required',
//                'loan_amount'=>'required',
//                'payment_term'=>'required',
//                'interest_rate'=>'required',
//                'installment_count'=>'required',
//                'installment_amount'=>'required'
//            ]
//        );
//
//        try{
//        /*    var_dump($request->loan_amount);*/
//
//     /*  dd($request->all());*/
//
//
//          DB::beginTransaction();
//
//            $loan =new Loan($request->all());
//            $loan->branch='001'; // session variable
//            $loan->available_receipt='0';
//            $loan->available_voucher='0';
//            $loan->profile='LOAN';
//            $loan->situation='LOAN';
//            $loan->status='APPLICATION_PENDING';
//            $loan->print_do='0';
//            $loan->rebate_approve='0';
//            $loan->receipt_block='0';
//            $loan->is_npl='0';
//            $loan->panalty= $request->chk_Penalty=='on'?'1':'0';
//            $loan->panalty_amount= $request->panalty_amount==''?'0':$request->panalty_amount;
//            $loan->reason=$request->reason==null || empty($request->reason)?
//                '':$request->reason;
//            $loan->note=$request->note==null || empty($request->note)?
//                '':$request->note;
//            $loan->application_document_no=$request->application_document_no==null || empty($request->application_document_no)?
//                '':$request->application_document_no;
//
//
//            $loan->save();
//
//            return redirect('loan_application')->with('alert','save');
//
//            DB::commit();
//
//
//        }catch(Exception $ex){
//           DB::rollback();
//
//          //  dd($ex);
//
//
//        }

    /*
    $loan->application_reference_no = $request->application_reference_no;
    $loan->application_document_no = $request->application_document_no;
    $loan->application_transaction_date = $request->application_transaction_date;
    $loan->application_document_no=$request->application_document_no==null || empty($request->application_document_no)?
    '':$request->application_document_no;
    $loan->client = $request->client;
    $loan->supplier = $request->supplier;
    $loan->loan_type = $request->loan_type;
    $loan->loan_group = $request->loan_group;
    $loan->loan_amount = $request->loan_amount;
    $loan->installment_amount = $request->installment_amount;
    $loan->installment_count = $request->installment_count;
    $loan->interest_rate = $request->interest_rate;
    $loan->expected_loan_date = $request->expected_loan_date;
    $loan->payment_term = $request->payment_term;
    $loan->panalty_rate = $request->panalty_rate;
    $loan->panalty= $request->chk_Penalty=='on'?'1':'0';
    $loan->panalty_amount= $request->panalty_amount==''?'0':$request->panalty_amount;
    $loan->grace_days= $request->grace_days;
    $loan->grace_days= $request->grace_days;

    $loan->branch='001'; // session variable
    $loan->available_receipt='0';
    $loan->available_voucher='0';
    $loan->profile='LOAN';
    $loan->situation='LOAN';
    $loan->status='APPLICATION_PENDING';
    $loan->print_do='0';
    $loan->rebate_approve='0';
    $loan->receipt_block='0';
    $loan->is_npl='0';

    $loan->reason=$request->reason==null || empty($request->reason)?
    '':$request->reason;
    $loan->note=$request->note==null || empty($request->note)?
    '':$request->note;*/


}
