<?php

namespace App\Http\Controllers;

use App\Document;
use App\Loan;
use App\LoanDocument;
use App\Settlement;
use Illuminate\Http\Request;

class LoanApprovalDetailController extends Controller
{


    public function edit($index_no){


        $loanList =  (new Loan())->select(
            'index_no',
            'application_reference_no',
            'application_document_no',
            'application_transaction_date',
            'client',
            'loan_type',
            'loan_amount',
            'installment_amount',
            'installment_count',
            'interest_rate',
            'panalty',
            'panalty_type',
            'panalty_rate',
            'panalty_amount',
            'grace_days',
            'note',
            'reason')
            ->with('clientString')
            ->with('loanTypeString')
            ->where('index_no',$index_no)
            ->first();

        $settlements = (new Settlement())->select('due_date','description','amount')->where('loan',$index_no)->get();
        $dd = (new LoanDocument())->select('document')
                        ->where('loan', $index_no)
                        ->get();
        $doc_details = (new Document())->whereIn('code',$dd)->get();






        return view('transaction.loan_application.loan_approval.edit',compact('loanList','settlements','doc_details'));
    }





}
