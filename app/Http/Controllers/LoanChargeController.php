<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\Loan;
use App\Settlement;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class LoanChargeController extends Controller
{
    public function index(Request $request){

        $transaction_type = ApplicationVarible::$OTHER_CHARGE_TRANSACTION_CODE;

        $common = new Common();
        $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$OTHER_CHARGE);
        $loan = (new Loan())->
        where('status', ApplicationVarible::$APPLICATION_START)->
        orderBy('agreement_no')->
        get();
        $head = ['Code','Name'];
        $data = ($this->getColumnData());


        return view('transaction.loan_charges.index', compact('referenceNo','loan','transaction_type','head','data'));
    }

    function getColumnData(){

        $account = (new Account())->
        whereHas('accountCategories',function ($query){
            return $query->where('parent','3');
        })->
        get();
        $data = [];

        foreach ($account as $list){

            array_push($data,[$list->code,$list->name]);
        }

        return $data;

    }


    function store(Request $request){


        $this->validate($request, [
            'transaction_date' => 'required',
            'agreement_no' => 'required',
        ]);

        try{
            DB::beginTransaction();
            $settlement = (json_decode($request->settlement));






                $loan_index = (new Loan())->where('agreement_no',$request->agreement_no)->first();

                if(isset($loan_index) && isset($settlement)){
                    //transaction
                    $transaction = (new Transaction())->saveTransaction(
                        $request->session()->get('working_date'),
                        ApplicationVarible::$OTHER_CHARGE_TRANSACTION_CODE,
                        $request->reference_no,
                        $request->document_no,
                        $loan_index->index_no,
                        $request->session()->get('branch_id'),
                        $loan_index->client,
                        ApplicationVarible::$ACTIVE
                    );
                    (new TransactionHistory())->saveTransactionHistory(
                        $transaction->index_no,
                        'NEW',
                        $request->session()->get('user_id'),
                        'Other charge'
                    );


                    foreach ($settlement as $key => $raw){


                        (new Settlement())->saveSettlement(
                            $request->session()->get('branch_id'),
                            $loan_index->client,
                            $loan_index->index_no,
                            $transaction->index_no,
                            ApplicationVarible::$OTHER_CHARGE_TRANSACTION_CODE,
                            '',
                            $raw[2],
                            floatval($raw[3]),
                            ApplicationVarible::$OTHER_CHARGES,
                            $raw[0],
                            ApplicationVarible::$PENDING);

                        //accounts transaction

                        (new AccountTransaction())->setManualAccountTransaction(
                            ApplicationVarible::$OTHER_CHARGE_TRANSACTION_CODE,
                            $raw[0],
                            AccountInterface::$CREDIT,
                            $raw[2],
                            floatval($raw[3]),
                            $transaction->index_no,
                            'AUTO');
                    }
                    (new AccountTransaction())->setAccountTransaction(
                        ApplicationVarible::$OTHER_CHARGE_TRANSACTION_CODE,
                        AccountInterface::$OTHER_CHARGE_AMOUNT_DEBIT_CODE,
                        'Loan Charge '.$request->agreement_no,
                        $request->total_amount,
                        $transaction->index_no,
                        'AUTO');



                }else{
                    dd("Error");
                }

            DB::commit();

            return ["save", config('app.FLD')."loan_charge",ApplicationVarible::$RECEIPT];
        }catch(Exception $e){
           DB::rollBack();
            dd($e);
        }

    }
}
