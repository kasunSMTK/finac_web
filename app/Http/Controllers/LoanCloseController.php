<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Loan;
use App\Reasons;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class LoanCloseController extends Controller
{

    public function index(Request $request)
    {

        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE;
            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            orderBy('index_no')->
            get();

            $reasons = (new Reasons())->pluck('description','description');


            return view('transaction.loan_close.index', compact('loan','transaction_type','reasons'));
        } else {

        }

    }

    public function store(Request $request){

        $this->validate(Request(), [
            'agreement_no' => 'required',
            'rdo_close' => 'required',
        ]);

        try{

            DB::beginTransaction();


            if($request->rdo_close[0]=="settlement_close"){

                $this->settlementClose($request);

            }
            if($request->rdo_close[0]=="suspend_close"){

                $this->suspendClose($request);

            }
            if($request->rdo_close[0]=="reschedule_close"){

                $this->rescheduleClose($request);

            }
            if($request->rdo_close[0]=="error_close"){


                $this->errorClose($request);
            }


            DB::commit();
            return redirect('loan_close')->with('alert','Loan Close Success !');
        }catch(Exception $exception){
            DB::rollback();
            return $exception;
        }
    }

    private function settlementClose(Request $request){

        //transaction
        $transaction = (new Transaction())->saveTransaction(
            $request->session()->get('working_date'),
            ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
            $request->reference_no,
            $request->document_no,
            $request->loan_index,
            $request->session()->get('branch_id'),
            $request->client_code,
            ApplicationVarible::$ACTIVE
        );

        (new TransactionHistory())->saveTransactionHistory(
            $transaction->index_no,
            'NEW',
            $request->session()->get('user_id'),
            'Loan settlement close'
        );

        // loan status
        $loan = (new Loan())->find($request->loan_index);
        $loan->status = ApplicationVarible::$COLLECTED;
        $loan->available_receipt = false;
        $loan->available_voucher = false;
        $loan->update();

        $settlement =(new Settlement())
            ->where('status',ApplicationVarible::$PENDING)
            ->where('loan',$request->loan_index)
            ->whereIn('settlement_type',
                [
                    ApplicationVarible::$LOAN_CAPITAL,
                    ApplicationVarible::$LOAN_INTEREST,
                    ApplicationVarible::$INSURANCE,
                    ApplicationVarible::$OVER_PAY_RECEIPT,
                    ApplicationVarible::$OTHER_CHARGES,
                    ApplicationVarible::$LOAN_PENALTY
                ])->with('settlementType')
            ->get();
        $total_amount=0.0;$interest=0.0;$capital=0.0;$insurance=0.0;
        $credit_amount=0.0;$debit_amount=0.0;


        $settlement_due =(new Settlement())
            ->where('status',ApplicationVarible::$PENDING)
            ->where('loan',$request->loan_index)
            ->where('due_date','>=',$request->session()->get('working_date'))
            ->whereIn('settlement_type',
                [
                    ApplicationVarible::$LOAN_CAPITAL,
                    ApplicationVarible::$LOAN_INTEREST,
                    ApplicationVarible::$INSURANCE,
                ])->with('settlementType')
            ->get();

        foreach ($settlement_due as $key=>$raw){

            if($raw['settlement_type']==ApplicationVarible::$LOAN_CAPITAL){
                $capital += floatval($raw['amount']);
            }
            if($raw['settlement_type']==ApplicationVarible::$LOAN_INTEREST){
                $interest += floatval($raw['amount']);
            }
            if($raw['settlement_type']==ApplicationVarible::$INSURANCE){
                $insurance += floatval($raw['amount']);
            }

            $total_amount +=floatval($raw['amount']);
        }

        foreach ($settlement as $key=>$raw){


            if($raw['settlementType']['credit_or_debit']=="CREDIT"){
                $credit_amount += floatval($raw['balance_amount']);
                $this->settlementHistories(
                    $transaction->index_no,
                    $request->loan_index,
                    ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                    $request->session()->get('working_date'),
                    $raw['index_no'],
                    floatval($raw['balance_amount']),
                    floatval($raw['balance_amount'])
                );
            }
            if($raw['settlementType']['credit_or_debit']=="DEBIT"){
                $debit_amount += floatval($raw['balance_amount']);
            }


        }

        //account transaction
        if($credit_amount >0){
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$CREDIT_SETTLEMENT_CREDIT_CODE,
                "Loan Settlement Close " . $request->agreement_no,
                $credit_amount,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);

            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$CREDIT_SETTLEMENT_DEBIT_CODE,
                "Loan Settlement Close " . $request->agreement_no,
                $credit_amount,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);
        }else if($debit_amount > 0){
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$DEBIT_SETTLEMENT_CREDIT_CODE,
                "Loan Settlement Close " . $request->agreement_no,
                $debit_amount,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);

            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$DEBIT_SETTLEMENT_DEBIT_CODE,
                "Loan Settlement Close " . $request->agreement_no,
                $debit_amount,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);
        }
//due amount
        if($capital > 0){
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$CAPITAL_DUE_CREDIT_CODE,
                "Loan Settlement Close Due Amount" . $request->agreement_no,
                $capital,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);
        }
        if($interest > 0){
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$INTEREST_DUE_CREDIT_CODE,
                "Loan Settlement Close Due Amount" . $request->agreement_no,
                $interest,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);

        }
        if($insurance > 0) {
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
                AccountInterface::$INSURANCE_DUE_CREDIT_CODE,
                "Loan Settlement Close Due Amount" . $request->agreement_no,
                $insurance,
                $transaction->index_no,
                AccountInterface::$TYPE_AUTO);

        }

        (new AccountTransaction())->setAccountTransaction(
            ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
            AccountInterface::$SETTLEMENT_DUE_DEBIT_CODE,
            "Loan Settlement Close Total Amount" . $request->agreement_no,
            $total_amount,
            $transaction->index_no,
            AccountInterface::$TYPE_AUTO);




    }

    private function settlementHistories($transaction,$loan_index,$transaction_type,$transaction_date,$index_no,$settlement_amount,$balance_amount){

        $settlement_ary = [
            'transaction' => $transaction,
            'transaction_type' => $transaction_type,
            'transaction_date' => $transaction_date,
            'settlement' => $index_no,
            'settlement_amount' => $settlement_amount,
            'before_balance' => $balance_amount,
            'status' => 'ACTIVE'
        ];
        //dd($settlement_ary);
        (new SettlementHistory())->create($settlement_ary);
        $this->settlementUpdate($loan_index,$index_no);

    }
    private function settlementUpdate($loan_index,$kay1){

        (new Settlement())->where('loan', $loan_index)
            ->where('index_no', $kay1)
            ->update(["balance_amount" => 0.0,
                "status" =>  'SETTLED',"rebate_amount"=>"0"]);
    }


    private function suspendClose(Request $request){

        //transaction
        $transaction = (new Transaction())->saveTransaction(
            $request->session()->get('working_date'),
            ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
            $request->reference_no,
            $request->document_no,
            $request->loan_index,
            $request->session()->get('branch_id'),
            $request->client_code,
            ApplicationVarible::$ACTIVE,
            'SUSPEND'
        );

        (new TransactionHistory())->saveTransactionHistory(
            $transaction->index_no,
            'NEW',
            $request->session()->get('user_id'),
            'Loan suspend close'
        );

        // loan status
        $loan = (new Loan())->find($request->loan_index);
        $loan->status = ApplicationVarible::$SUSPEND;
        $loan->available_receipt = false;
        $loan->available_voucher = false;
        $loan->note = $request->note;
        $loan->update();



    }
//    private function rescheduleClose(Request $request){
//        //transaction
//        $transaction = (new Transaction())->saveTransaction(
//            $request->session()->get('working_date'),
//            ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
//            $request->reference_no,
//            $request->document_no,
//            $request->loan_index,
//            $request->session()->get('branch_id'),
//            $request->client_code,
//            ApplicationVarible::$ACTIVE,
//            'Reschedule close'
//        );
//
//        (new TransactionHistory())->saveTransactionHistory(
//            $transaction->index_no,
//            'NEW',
//            $request->session()->get('user_id'),
//            'Loan Reschedule close'
//        );
//        // loan status
//        $loan = (new Loan())->find($request->loan_index);
//        $loan->status = ApplicationVarible::$COLLECTED;
//        $loan->available_receipt = false;
//        $loan->available_voucher = false;
//        $loan->update();
//
//        $settlement =(new Settlement())
//            ->where('status',ApplicationVarible::$PENDING)
//            ->where('loan',$request->loan_index)
//            ->where('balance_amount','>',0)
//            ->with('settlementType')
//            ->get();
//        $total_amount=0.0;$cr_amount=0.0;$dr_amount=0.0;$interest=0.0;$capital=0.0;$charge=0.0;
//        $credit_amount=0.0;$debit_amount=0.0;
//
//
//        foreach ($settlement as $key=>$raw){
//
//            if($raw['settlementType']['credit_or_debit']=="CREDIT") {
//                if ($raw['settlement_type'] == ApplicationVarible::$LOAN_CAPITAL) {
//                    $capital += floatval($raw['balance_amount']);
//                }
//                if ($raw['settlement_type'] == ApplicationVarible::$LOAN_INTEREST) {
//                    $interest += floatval($raw['balance_amount']);
//                }
//                if ($raw['settlement_type'] == ApplicationVarible::$OTHER_CHARGES) {
//                    $charge += floatval($raw['balance_amount']);
//                }
//            $total_amount +=floatval($capital)+floatval($interest)+floatval($charge);
//            $cr_amount +=floatval($raw['balance_amount']);
//                $this->settlementHistories(
//                    $transaction->index_no,
//                    $request->loan_index,
//                    ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
//                    $request->session()->get('working_date'),
//                    $raw['index_no'],
//                    floatval($raw['balance_amount']),
//                    floatval($raw['balance_amount'])
//                );
//            }
//            }if($raw['settlementType']['credit_or_debit']=="DEBIT") {
//
//                $dr_amount +=floatval($raw['balance_amount']);
//                $this->settlementHistories(
//                    $transaction->index_no,
//                    $request->loan_index,
//                    ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
//                    $request->session()->get('working_date'),
//                    $raw['index_no'],
//                    floatval($raw['balance_amount']),
//                    floatval($raw['balance_amount'])
//                );
//
//
//
//            }
//
//
//    }
    private function errorClose(Request $request){
        //transaction
        $transaction = (new Transaction())->saveTransaction(
            $request->session()->get('working_date'),
            ApplicationVarible::$LOAN_CLOSE_TRANSACTION_CODE,
            $request->reference_no,
            $request->document_no,
            $request->loan_index,
            $request->session()->get('branch_id'),
            $request->client_code,
            ApplicationVarible::$ACTIVE,
            'Error close'
        );

        (new TransactionHistory())->saveTransactionHistory(
            $transaction->index_no,
            'NEW',
            $request->session()->get('user_id'),
            'Loan Error close'
        );


                DB::statement(
                    'call z_loan_cancel(?)',
                    [$request->input('loan_index')]
                );
        


    }

}
