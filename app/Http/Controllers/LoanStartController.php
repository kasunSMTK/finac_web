<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Client;
use App\Employee;
use App\Loan;
use App\Settlement;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class LoanStartController extends Controller
{


    public function index()
    {

        $loan_start_list = (new Loan())->select(
            'index_no',
            'application_reference_no',
            'application_document_no',
            'application_transaction_date',
            'client',
            'loan_type',
            'loan_amount',
            'installment_amount',
            'installment_count',
            'interest_rate',
            'panalty',
            'panalty_type',
            'panalty_rate',
            'panalty_amount',
            'grace_days',
            'payment_term',
            'reason')
            ->with('clientString')
            ->with('loanTypeString')
            ->where('status', ApplicationVarible::$APPLICATION_ACCEPT)->get();

        $employees = (new Employee())->orderBy('name')->pluck('name', 'code');

        return view('transaction.loan_application.loan_start.index', compact('loan_start_list', 'employees'));
    }

    public function store(Request $request)
    {

        /* dd($request->all());*/

        $this->validate(Request(), [
            'md_transaction_date' => 'required',
            'agreement_no' => 'required|unique:loan',
            'md_loan_officer' => 'required',
            'md_recovery_officer' => 'required',
            'md_loan_date' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $select_loan = (new Loan())->
            where('application_reference_no', $request->md_application_reference_no)->
            first();

            $select_loan->loan_reference_no = $request->md_application_reference_no;
            $select_loan->loan_document_no = $request->md_loan_document_no;
            $select_loan->loan_transaction_date = Session::get('working_date');
            $select_loan->agreement_no = $request->agreement_no;
            $select_loan->loan_date = $request->md_loan_date;
            $select_loan->available_receipt = 1;
            $select_loan->available_voucher = 1;
            $select_loan->print_do = 0;
            $select_loan->status = ApplicationVarible::$APPLICATION_START;

            $select_loan->update();

            $settlement_list = (new Settlement())->
            where('loan', $select_loan->index_no)->
            where('status', '!=', 'CANCEL')->
            where('settlement_type', 'APPLICATION_CHARGE')->get();


            //transaction
            $transaction = [
                'transaction_date' => Session::get('working_date'),
                'transaction_type' => ApplicationVarible::$LOAN_START,
                'reference_no' => $request->md_application_reference_no,
                'document_no' => $request->md_loan_document_no,
                'loan' => $select_loan->index_no,
                'branch' => $request->session()->get('branch_id'),
                'client' => $select_loan->client,
                'status' => ApplicationVarible::$ACTIVE

            ];
            //  var_dump($newloan->index_no);
            $transactionSave = (new Transaction())->create($transaction);
            $transactionSave->save();

            //transaction history
            $transaction = (new TransactionHistory());

            $transaction->transaction = $transactionSave->index_no;
            $transaction->action = 'NEW';
            $transaction->employee = $request->session()->get('user_id'); //session variable employee code
            $transaction->note = 'Loan Start';

            $transaction->save();

            //settlement

            //call common php
            $schedule = (new Common())->calculatePaymentSchedule(
                $select_loan->loanTypes->interest_method,
                $select_loan->payment_term,
                $select_loan->installment_count,
                $select_loan->interest_rate,
                $select_loan->loan_amount,
                $request->md_loan_date
            );

            $settlement = [];
            foreach ($schedule as $key => $row) {
                $Count = "";
                $Interest = "";
                $Capital = "";
                $Amount = "";
                $Pay_Date = "";

                foreach ($row as $key1 => $row2) {


                    switch ($key1) {

                        case "Count":
                            $Count = $row2;
                            break;
                        case "Interest":
                            $Interest = $row2;
                            break;
                        case "Capital":
                            $Capital = $row2;
                            break;
                        case "Amount":
                            $Amount = $row2;
                            break;
                        case "Pay_Date":
                            $Pay_Date = $row2;
                            break;

                    }
                }

                if (($Count != "" || $Count != null) &&
                    ($Interest != "" || $Interest != null) &&
                    ($Capital != "" || $Capital != null) &&
                    ($Amount != "" || $Amount != null) &&
                    ($Pay_Date != "" || $Pay_Date != null)
                ) {

                    if ($Interest > 0) {//interest

                        $settlement = [
                            'transaction_date' => Session::get('working_date'),
                            'branch' => $request->session()->get('branch_id'), // session variable
                            'client' => $request->client,
                            'loan' => $select_loan->index_no,
                            'transaction' => $transactionSave->index_no,    // TRANSACTION
                            'transaction_type' => ApplicationVarible::$LOAN_START,
                            'installment_no' => $Count,
                            'description' => 'Loan Interest',
                            'amount' => $Interest,
                            'balance_amount' => $Interest,
                            'due_date' => $Pay_Date,
                            'settlement_type' => ApplicationVarible::$LOAN_INTEREST,
                            'status' => ApplicationVarible::$PENDING,
                            'account' => ''
                        ];
                        $settlementSave = (new Settlement())->create($settlement);
                        $settlementSave->save();
                    }
                    if ($Capital > 0) {//capital
                        $settlement_cap = [
                            'transaction_date' => Session::get('working_date'),
                            'branch' => $request->session()->get('branch_id'), // session variable
                            'client' => $request->client,
                            'loan' => $select_loan->index_no,
                            'transaction' => $transactionSave->index_no,    // TRANSACTION
                            'transaction_type' => ApplicationVarible::$LOAN_START,
                            'installment_no' => $Count,
                            'description' => 'Loan capital',
                            'amount' => $Capital,
                            'balance_amount' => $Capital,
                            'due_date' => $Pay_Date,
                            'settlement_type' => ApplicationVarible::$LOAN_CAPITAL,
                            'status' => ApplicationVarible::$PENDING,
                            'account' => ''
                        ];
                        $settlement_save_cap = (new Settlement())->create($settlement_cap);
                        $settlement_save_cap->save();
                    }


                }
            }//end of for loop


            if ($select_loan->profile === "LOAN") {
                $settlement_loan_amount = [//loan amount
                    'transaction_date' => Session::get('working_date'),
                    'branch' => $request->session()->get('branch_id'), // session variable
                    'client' => $request->client,
                    'loan' => $select_loan->index_no,
                    'transaction' => $transactionSave->index_no,    // TRANSACTION
                    'transaction_type' => ApplicationVarible::$LOAN_START,
                    'installment_no' => null,
                    'description' => 'Loan Amount',
                    'amount' => $select_loan->loan_amount,
                    'balance_amount' => $select_loan->loan_amount,
                    'due_date' => Session::get('working_date'),
                    'settlement_type' => ApplicationVarible::$LOAN_AMOUNT,
                    'status' => ApplicationVarible::$PENDING,
                    'account' => ''
                ];
                $settlement_save_Loan_amount = (new Settlement())->create($settlement_loan_amount);
                $settlement_save_Loan_amount->save();
            }
            if ($select_loan->profile === "LOAN") {
                if (isset($settlement_list)) {
                    for ($i = 0; $i < sizeof($settlement_list); $i++) {

                        if ($settlement_list[$i]->description === "DOWN PAYMENT") {
                            $settlement_downpay = [//down payment
                                'transaction_date' => Session::get('working_date'),
                                'branch' => $request->session()->get('branch_id'), // session variable
                                'client' => $request->client,
                                'loan' => $select_loan->index_no,
                                'transaction' => $transactionSave->index_no,    // TRANSACTION
                                'transaction_type' => ApplicationVarible::$LOAN_START,
                                'installment_no' => null,
                                'description' => 'Down Payment',
                                'amount' => $settlement_list[$i]->amount,
                                'balance_amount' => $settlement_list[$i]->amount,
                                'due_date' => Session::get('working_date'),
                                'settlement_type' => ApplicationVarible::$LOAN_AMOUNT,
                                'status' => ApplicationVarible::$PENDING,
                                'account' => ''
                            ];
                            $settlement_save_down_payment = (new Settlement())->create($settlement_downpay);
                            $settlement_save_down_payment->save();

                        }
                    }
                }
            }
            //accounts transactions

            if ($select_loan->loanTypes->is_hp === 0) {
                //loan amount debit
                (new AccountTransaction())->setAccountTransaction(
                    ApplicationVarible::$LOAN_START,
                    AccountInterface::$LOAN_AMOUNT_DEBIT,
                    "Loan Amount " . $select_loan->agreement_no,
                    $select_loan->loan_amount,
                    $transactionSave->index_no,
                    AccountInterface::$TYPE_AUTO
                );
                //loan amount credit
                (new AccountTransaction())->setAccountTransaction(
                    ApplicationVarible::$LOAN_START,
                    AccountInterface::$LOAN_AMOUNT_CREDIT,
                    "Loan Amount " . $select_loan->agreement_no,
                    $select_loan->loan_amount,
                    $transactionSave->index_no,
                    AccountInterface::$TYPE_AUTO
                );
            } else {
                //loan hp amount debit
                (new AccountTransaction())->setAccountTransaction(
                    ApplicationVarible::$LOAN_START,
                    AccountInterface::$LOAN_AMOUNT_DEBIT,
                    "Loan Do Amount " . $select_loan->agreement_no,
                    $select_loan->loan_amount,
                    $transactionSave->index_no,
                    AccountInterface::$TYPE_AUTO
                );
                //loan hp amount credit
                (new AccountTransaction())->setAccountTransaction(
                    ApplicationVarible::$LOAN_START,
                    AccountInterface::$LOAN_AMOUNT_DO_CREDIT,
                    "Loan Do Amount " . $select_loan->agreement_no,
                    $select_loan->loan_amount,
                    $transactionSave->index_no,
                    AccountInterface::$TYPE_AUTO
                );
            }

            //document charges
            if (isset($settlement_list)) {
                $total_amount = 0.0;
                for ($i = 0; $i < sizeof($settlement_list); $i++) {

                    (new AccountTransaction())->setManualAccountTransaction(
                        ApplicationVarible::$LOAN_APPLICATION,
                        $settlement_list[$i]->account,
                        AccountInterface::$CREDIT,
                        $settlement_list[$i]->description,
                        $settlement_list[$i]->amount,
                        $transactionSave->index_no,
                        AccountInterface::$TYPE_AUTO
                    );
                    $total_amount += $settlement_list[$i]->amount;
                }
                (new AccountTransaction())->setAccountTransaction(
                    ApplicationVarible::$LOAN_APPLICATION,
                    AccountInterface::$DEFAULT_CHARGE_DEBIT,
                    "Default Charge Amount " . $select_loan->agreement_no,
                    $total_amount,
                    $transactionSave->index_no,
                    AccountInterface::$TYPE_AUTO
                );
            }


            DB::commit();

        } catch (Exception $e) {
            DB::rollback();

            return $e;

        }

        return redirect('loan_start')->with('alert','Loan Start Success !');

    }


}
