<?php

namespace App\Http\Controllers;

use App\LoanTypeChargeScheme;
use Illuminate\Http\Request;

class LoanTypeChargeSchemeController extends Controller
{

    public function getLoanCharge(Request $request)
    {

/*        $loanChargeScheme = (new LoanTypeChargeScheme())->where('loan_type',$request->loan_type_code);*/

        $loanChargeScheme = (new LoanTypeChargeScheme)
            ->where('loan_type',$request->loan_type_code)
            ->with('chargeScheme')
            ->get();



        return compact('loanChargeScheme');
    }

}
