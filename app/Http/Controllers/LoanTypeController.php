<?php

namespace App\Http\Controllers;

use App\LoanType;
use Illuminate\Http\Request;

class LoanTypeController extends Controller
{
    public function getLoanType(Request $request){
        $loan_type = (new LoanType())->find($request->loan_type_code);
        return compact('loan_type');
    }


}
