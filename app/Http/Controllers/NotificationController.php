<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NotificationController extends Controller
{
    public function get(Request $request){

//        return Notification::all();
        $user_auth=(new Permission())
            ->where('company_id',$request->session()->get('company_id'))
            ->where('project_id',$request->session()->get('project_id'))
            ->where('user_register_id',$request->session()->get('user_register_id'))
            ->orderby('module_order_no')->get();

        $notification = (new Notification())->where('read_at',null)->get();
        return $notification;
    }
    public function read(Request $request) {


       // Auth::user()->unreadNotifications()->find($request->id)->markAsRead();



        $notify = (new Notification())->where('notifiable_id',$request->id)->first();
        $notify->read_at = Session::get('working_date');
        $notify->update();


        $url = config('app.FLD').$request->type;

        return  [$url];
    }
}
