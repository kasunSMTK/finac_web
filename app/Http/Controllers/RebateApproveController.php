<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Loan;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class RebateApproveController extends Controller
{
    public function index(){

        $approval_list = (new Loan())->where('rebate_approve','1')->get();

        return view('transaction.loan_rebate_approve.index',compact('approval_list'));

    }



    public function edit(Request $request,$index)
    {


        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$REBATE_APPROVE_TRANSACTION_CODE;
            $loan = (new Loan())->where('index_no',$index)->first();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$REBATE_PREFIX);
            return view('transaction.loan_rebate_approve.edit', compact('loan','referenceNo','transaction_type'));
        } else {

            // alert error

        }

    }
    public function dataSet(Request $request)
    {



        $data_list = (new Loan())->where('agreement_no', $request->agreement_no)->with('clients')->first();


        $settlement = (new Settlement())->
        where('loan', $data_list->index_no)->
        where('status', 'PENDING')->
        where('settlement_type', '<>','LOAN_CAPITAL')->
        whereHas('settlementType', function ($query) {
            $query->where('credit_or_debit', 'CREDIT');
        })->orderBy('due_date')->with('settlementType')
            ->get();




        return compact('data_list', 'settlement');
    }
    public function store(Request $request)
    {



        $this->validate(Request(), [
            'loan_index' => 'required',
            'rebate_amount' => 'required',
            'rebate_percentage' => 'required_without_all:rebate_amount',
        ]);

        try {
            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$REBATE_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Loan Rebate Approve'
            );
            //settlement
            $settlement_ary = (new Settlement())->updateSettlementAmount(
                $request->loan_index,
                $request->settlement);

            //settlement history
            (new SettlementHistory())->saveSettlementHistories(
                $settlement_ary,
                $transaction->index_no,
                ApplicationVarible::$REBATE_TRANSACTION_CODE,
                $request->session()->get('working_date')
            );

            //account transaction

            $this->accountTransaction($settlement_ary,$transaction->index_no);


            $loan = (new Loan())->find($request->loan_index);

            $loan->rebate_approve = "0";
            $loan->update();



            DB::commit();
            //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
            return ["save", config('app.FLD')."rebate_approval"];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }
    public function reject(Request $request)
    {

        $this->validate(Request(), [
            'loan_index' => 'required',
        ]);

        try {
            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$REBATE_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Loan Rebate Approve'
            );
            //settlement
            $settlement_ary = (new Settlement())->find($request->loan_index);
            $settlement_ary->rebate_amount = 0;
            $settlement_ary->update();



            $loan = (new Loan())->find($request->loan_index);

            $loan->rebate_approve = "0";
            $loan->update();



            DB::commit();
            //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
            return ["save", config('app.FLD')."rebate_approval"];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }
    public  function accountTransaction($settlement_ary,$transaction){
        $index_no = 0;
        $settlement_amount = 0.0;
        $balance_amount = 0.0;
        $total_amount = 0.0;
        if (isset($settlement_ary)) {


            foreach ($settlement_ary as $key => $row) {
                foreach ($row as $kay1 => $row1) {

                    if ($kay1 == 'index_no') {
                        $index_no = $row1;
                    }
                    if ($kay1 == 'settlement_amount') {
                        $settlement_amount = $row1;
                    }
                    if ($kay1 == 'balance_amount') {
                        $balance_amount = $row1;
                    }

                }
                if ($index_no != '' && $settlement_amount != '' && $balance_amount != '') {

                    if(floatval($settlement_amount)>0) {

                        $settlement = (new Settlement())->find($index_no);


                        switch ($settlement->settlement_type) {

                            case "LOAN_PANALTY" :
                                (new AccountTransaction())->setAccountTransaction(
                                    ApplicationVarible::$REBATE_TRANSACTION_CODE,
                                    AccountInterface::$REBATE_PENALTY_AMOUNT_DEBIT_CODE,
                                    'Rebate Amount ',
                                    $settlement_amount,
                                    $transaction,
                                    'AUTO'
                                );


                                break;

                            case "OTHER_CHARGE" :

                                (new AccountTransaction())->setManualAccountTransaction(
                                    ApplicationVarible::$REBATE_TRANSACTION_CODE,
                                    $settlement->account,
                                    AccountInterface::$DEBIT,
                                    "Client Rebate " . $settlement->description,
                                    $settlement_amount,
                                    $transaction,
                                    'AUTO');

                                break;

                            case "LOAN_INTEREST" :
                                (new AccountTransaction())->setAccountTransaction(
                                    ApplicationVarible::$REBATE_TRANSACTION_CODE,
                                    AccountInterface::$REBATE_INTEREST_AMOUNT_DEBIT_CODE,
                                    'Rebate Amount ',
                                    $settlement_amount,
                                    $transaction,
                                    'AUTO'
                                );
                                break;

                            case "INSURANCE" :
                                (new AccountTransaction())->setAccountTransaction(
                                    ApplicationVarible::$REBATE_TRANSACTION_CODE,
                                    AccountInterface::$REBATE_INSURANCE_AMOUNT_DEBIT_CODE,
                                    'Rebate Amount ',
                                    $settlement_amount,
                                    $transaction,
                                    'AUTO'
                                );
                                break;


                        }


                        $total_amount += floatval($settlement_amount);
                        $index_no = 0;
                        $settlement_amount = 0.0;
                        $balance_amount = 0.0;
                    }

                }
            }
            (new AccountTransaction())->setAccountTransaction(
                ApplicationVarible::$REBATE_TRANSACTION_CODE,
                AccountInterface::$REBATE_AMOUNT_CREDIT_CODE,
                'Rebate Amount ',
                $total_amount,
                $transaction,
                'AUTO'
            );

        }




    }



}
