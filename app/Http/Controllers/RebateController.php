<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Loan;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class RebateController extends Controller
{
    public function index(Request $request)
    {

        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$REBATE_TRANSACTION_CODE;
            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            where('available_receipt', '1')->
            where('rebate_approve', '0')->
            orderBy('index_no')->
            get();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$REBATE_PREFIX);
            return view('transaction.loan_rebate.index', compact('loan', 'referenceNo','transaction_type'));
        } else {

            // alert error

        }

    }
    public function store(Request $request)
    {

        $this->validate(Request(), [
            'agreement_no' => 'required',
            'rebate_amount' => 'required',
            'rebate_percentage' => 'required_without_all:rebate_amount',
        ]);

        try {
            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$REBATE_APPROVE_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Loan Rebate'
            );
            //settlement
            $settlement_ary = (new Settlement())->updateRebateSettlementAmount(
                $request->loan_index,
                $request->settlement);

            //settlement history
            (new SettlementHistory())->saveSettlementHistories(
                $settlement_ary,
                $transaction->index_no,
                ApplicationVarible::$REBATE_APPROVE_TRANSACTION_CODE,
                $request->session()->get('working_date')
            );

            $loan = (new Loan())->find($request->loan_index);

            $loan->rebate_approve = "1";
            $loan->update();


            DB::commit();
            //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
            return ["save", config('app.FLD')."rebate"];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }

}
