<?php

namespace App\Http\Controllers;

use App\Loan;
use App\ReportPermissions;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReportGenerateController extends Controller
{



    public function report($transaction_no,$transaction_type){


        (new Common())->reportGenerate($transaction_no,$transaction_type);


    }public function reportPdf(Request $request){

        (new Common())->reportGeneratePdf($request);


    }
    public function reportView(Request $request){


        $param =array();
        $name ="";
        $type = str_replace("/","",$request->getRequestUri());

        $reports = (new ReportPermissions())
            ->where('company_id',$request->session()->get('company_id'))
            ->where('project_id',$request->session()->get('project_id'))
            ->where('user_register_id',$request->session()->get('user_register_id'))
            ->where('view','1')
            ->where('module_category',$type)
            ->orderby('module_order_no')->get();


        switch ($type){

            case  "loan_report" :

                $name="Loan Report";

               break;

            default :

        }

        $title = "";


        return view('reports.report',compact('name','type','reports','title'));
    }


    public function loan(){

        $head[] = ['title'=>'Agreement No'];
        $head[] = ['title'=>'Doc No'];
        $head[] = ['title'=>'Code'];
        $head[] = ['title'=>'Customer'];
        $head[] = ['title'=>'Status'];

        $data =$this->getColumnData();

        $title = "Agreement Details";

        return compact('head','data','title');

    }
    function getColumnData(){

        $data_list = (new Loan())->
        where('status','<>', ApplicationVarible::$CANCEL)->
        where('available_receipt', '1')->
        with('clients')->
        orderBy('index_no')->
        get();
        $data = [];

        foreach ($data_list as $list){

            $data[] =
                [
                    $list->agreement_no,
                    $list->document_no,
                    $list->clients->code,
                    $list->clients->name,
                    $list->status,

                ];
        }

        return $data;

    }


}
