<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\AdvancedPayment;
use App\BankBranch;
use App\Cheque;
use App\ChequeDetails;
use App\Loan;
use App\Payment;
use App\Settlement;
use App\SettlementHistory;
use App\TemperaryReceipt;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class TemporaryReceiptController extends Controller
{
    public function index(Request $request)
    {

        $transaction_type=ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE;
        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            where('available_receipt', '1')->
            orderBy('index_no')->
            get();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$RECEIPT_PREFIX);

            $cheque = (new BankBranch())->selectRaw('code,concat(name , code)as name')->
            orderBy('name')->pluck('name','code');
            return view('transaction.payments.cheque_index', compact('loan', 'referenceNo','cheque','transaction_type'));
        } else {

            // alert error

        }

    }
    public function store(Request $request)
    {

        $this->validate(Request(), [
            'agreement_no' => 'required',
            'payment_amount' => 'required',
            'payment_amount_confirmation' => ['required_with:payment_amount','same:payment_amount']

        ]);



        try {
            DB::beginTransaction();
            //transaction

            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                ''.$request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Cheque Payment'
            );

            //payment information

           $payment_object= (new Payment())->storePayment(
                $request->session()->get('branch_id'),
                $request->client_code,
                $request->session()->get('cashier_session'),
                0,
                $request->payment_amount, 0, 0,
                $transaction->index_no,
                ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE
            );


            //cheque information
            (new Cheque())->store(
                $request->cheque_information,
                $transaction->index_no,
                ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE,
                $request->client_code,
                $payment_object->index_no,
                ApplicationVarible::$CLIENT,
                ApplicationVarible::$CHQ_PENDING
                );
            //temporary receipt

            (new TemperaryReceipt())->storeTempReceipt(
                $request->loan_index,
                $request->session()->get('branch_id'),
                $transaction->index_no,
                $request->payment_amount
            );



            //accounts transaction

            (new AccountTransaction())->PaymentAccountTransaction(
                $transaction->index_no,
                ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE,
                "Receipt temporary Payment",
                AccountInterface::$TEMP_RECEIPT_AMOUNT_CREDIT_CODE,
                $request->payment_amount,
                AccountInterface::$TYPE_AUTO,
                $request->settlement,
                0,
                $request->account
            );


            DB::commit();

            return ["save",config('app.FLD')."cheque_payment",$transaction->index_no,ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE];

        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }


    }



}
