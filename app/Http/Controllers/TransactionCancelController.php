<?php

namespace App\Http\Controllers;

use App\Cheque;
use App\Loan;
use App\TemperaryReceipt;
use App\Transaction;
use App\TransactionHistory;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class TransactionCancelController extends Controller
{


    public function indexCash(Request $request)
    {

        $transaction_type = ApplicationVarible::$RECEIPT;
        if (empty($request->transaction_date)) {
            $transaction_date = Session::get('working_date');
            $transaction = (new Transaction())->
            where('transaction_type', ApplicationVarible::$RECEIPT)->
            where('status', 'ACTIVE')->
            where('transaction_date', '>=', $transaction_date)->
            with('loans', 'clients', 'amounts')->
            get();

            return view('transaction.paymentCancel.index', compact('transaction', 'transaction_date', 'transaction_type'));

        } else {
            $transaction_date = $request->transaction_date;
            $transaction = (new Transaction())->
            where('transaction_type', ApplicationVarible::$RECEIPT)->
            where('status', 'ACTIVE')->
            where('transaction_date', '>=', $transaction_date)->
            with('loans', 'clients', 'amounts')->
            get();

            return compact('transaction', 'transaction_date', 'transaction_type');

        }
    }

    public function indexBank(Request $request)
    {

        $transaction_type = ApplicationVarible::$BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE;
        if (empty($request->transaction_date)) {
            $transaction_date = Session::get('working_date');
            $transaction = (new Transaction())->
            where('transaction_type', $transaction_type)->
            where('status', 'ACTIVE')->
            where('transaction_date', '>=', $transaction_date)->
            with('loans', 'clients', 'amounts')->
            get();

            return view('transaction.paymentCancel.index', compact('transaction', 'transaction_date', 'transaction_type'));

        } else {
            $transaction_date = $request->transaction_date;
            $transaction = (new Transaction())->
            where('transaction_type', $transaction_type)->
            where('status', 'ACTIVE')->
            where('transaction_date', '>=', $transaction_date)->
            with('loans', 'clients', 'amounts')->
            get();

            return compact('transaction', 'transaction_date', 'transaction_type');

        }
    }
    public function indexTemp(Request $request)
    {
//PENDING
        $transaction_type = ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE;
        if (empty($request->transaction_date)) {
            $transaction_date = Session::get('working_date');
            $transaction = (new Transaction())->
            select('transaction.*')->
            where('transaction.transaction_type', $transaction_type)->
            where('transaction.status', 'ACTIVE')->
            where('transaction.transaction_date', '>=', $transaction_date)->
                join('payment_information', 'transaction.index_no', '=', 'payment_information.transaction')->
                join('cheque', 'payment_information.index_no', '=', 'cheque.payment_object')->
            where('cheque.status', ApplicationVarible::$PENDING)->
            with('loans', 'clients', 'amounts')->
            get();
            return view('transaction.paymentCancel.index', compact('transaction', 'transaction_date', 'transaction_type'));

        } else {
            $transaction_date = $request->transaction_date;
            $transaction = (new Transaction())->
                select('transaction.*')->
            where('transaction.transaction_type', $transaction_type)->
            where('transaction.status', 'ACTIVE')->
            where('transaction.transaction_date', '>=', $transaction_date)->
            join('payment_information', 'transaction.index_no', '=', 'payment_information.transaction')->
            join('cheque', 'payment_information.index_no', '=', 'cheque.payment_object')->
            where('cheque.status', ApplicationVarible::$PENDING)->
            with('loans', 'clients', 'amounts')->
            get();

            return compact('transaction', 'transaction_date', 'transaction_type');

        }
    }

    public function cancel($transaction_type, $index, $referenceNo, $loan_index, $client_code)
    {


        try {

            DB::beginTransaction();

            switch ($transaction_type) {

                case "RECEIPT":
                    //transaction
                    $transaction = (new Transaction())->saveTransaction(
                        session()->get('working_date'),
                        ApplicationVarible::$TRANSACTION_CANCEL_TRANSACTION_CODE,
                        $referenceNo,
                        "",
                        $loan_index,
                        session()->get('branch_id'),
                        $client_code,
                        ApplicationVarible::$ACTIVE
                    );

                    (new TransactionHistory())->saveTransactionHistory(
                        $transaction->index_no,
                        'NEW',
                        session()->get('user_id'),
                        'Payment Cancel'
                    );


                    (new Loan())->updateAvailableReceipt($loan_index);


                    $cancelService = (new CancelService());
                    $cancelService->cancel($index);

                    break;

                case "BANK_RECEIPT":
                    //transaction
                    $transaction = (new Transaction())->saveTransaction(
                        session()->get('working_date'),
                        ApplicationVarible::$TRANSACTION_CANCEL_TRANSACTION_CODE,
                        $referenceNo,
                        "",
                        $loan_index,
                        session()->get('branch_id'),
                        $client_code,
                        ApplicationVarible::$ACTIVE
                    );

                    (new TransactionHistory())->saveTransactionHistory(
                        $transaction->index_no,
                        'NEW',
                        session()->get('user_id'),
                        'Payment Cancel'
                    );


                    (new Loan())->updateAvailableReceipt($loan_index);


                    $cancelService = (new CancelService());
                    $cancelService->cancel($index);

                    break;

                case "TEMP_RECEIPT":
                    //transaction
                    $transaction = (new Transaction())->saveTransaction(
                        session()->get('working_date'),
                        ApplicationVarible::$TRANSACTION_CANCEL_TRANSACTION_CODE,
                        $referenceNo,
                        "",
                        $loan_index,
                        session()->get('branch_id'),
                        $client_code,
                        ApplicationVarible::$ACTIVE
                    );

                    (new TransactionHistory())->saveTransactionHistory(
                        $transaction->index_no,
                        'NEW',
                        session()->get('user_id'),
                        'Payment Cancel'
                    );


                    (new Loan())->updateAvailableReceipt($loan_index);
                    (new Cheque())->changeStatus($index);
                    (new TemperaryReceipt())->findAndUpdateStates($index,ApplicationVarible::$CANCEL);

                    $cancelService = (new CancelService());
                    $cancelService->cancel($index);


                    break;

            }


            DB::commit();
            return redirect()->back()->with('alert', 'Updated!');


        } catch (Exception $e) {
            DB::rallBack();
            return redirect()->back()->with('alert', 'Not Update! \n Description Found \n' . $e);
        }

    }
}

