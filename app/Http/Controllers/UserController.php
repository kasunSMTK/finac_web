<?php

namespace App\Http\Controllers;

use App\Branch;
use App\CashierSession;
use App\Employee;
use App\Permission;
use App\ReportPermissions;
use App\ReportXml;
use App\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class UserController extends Controller
{
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

//        $credential = [$request->only('email','password');
        $credential = [
            'email' => $request->email,
            'password' => $request->password,
            'project_id' => 6
        ];

//"http://192.168.1.156/api/check_permissions"
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://173.82.115.171/smupms/api/check_permissions", //"http://upms.ceylonit.tech/api/check_permissions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($credential),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);



        if ($err) {
            echo "cURL Error #:" . $err;
        } else {

            if ($response != "0") {

                //check is available branch
                $val = $this->checkBranchExist(json_decode($response), $request->branch);
                if (isset($val) && $val != "") {

                    $this->setCurrentLogins(json_decode($response),$val);
                    return redirect('clients');

                } else {
                    return redirect()->back()->withInput($request->only('user_name', 'remember'))->withErrors([
                        'password_fail' => 'Sorry ! You Have No Permission to Login This Branch.',
                    ]);
                }

            } else {

                return redirect()->back()->withInput($request->only('user_name', 'remember'))->withErrors([
                    'password_fail' => 'Wrong username or password !',
                ]);
            }

        }

    }

    public function checkBranchExist($permissions, $select_branch)
    {
        $branch_up_id = (new Branch())->where('code', $select_branch)->first();
        $ret_value = "";
        if (isset($branch_up_id)) {
            foreach ($permissions->user->user_branches as $value) {

                if ($value->branch->id == $branch_up_id->up_id) {

                    $ret_value = $branch_up_id;

                }

            }

        }
        return $ret_value;

    }

    public function setCurrentLogins($permissions,$branch)
    {

        try {
            DB::beginTransaction();



            $user_login = (new UserLogin())->where('user_register_id', $permissions->user->id);
            $user_login->delete();
            $user_permissions = (new Permission())->
            where('user_register_id', $permissions->user->id)->
            where('company_id',$permissions->permission->company_id)->
            where('project_id',$permissions->permission->project_id);
            $user_permissions->delete();

            $user_permissions_report = (new ReportPermissions())->
            where('user_register_id', $permissions->user->id)->
            where('company_id',$permissions->permission->company_id)->
            where('project_id',$permissions->permission->project_id);
            $user_permissions_report->delete();

            $emp_user = (new Employee())->where('user_register_id', $permissions->user->id)->first();

            Session::put('user_register_id',$permissions->user->id);
            Session::put('company_id', $permissions->permission->company_id);
            Session::put('project_id', $permissions->permission->project_id);



           /* Session::put('user_id', $permissions->user->id);
            Session::put('user_name', $permissions->user->name);
            Session::put('user_email', $permissions->user->email);
            Session::put('company_id', $permissions->permission->company_id);
            Session::put('company', $permissions->permission->company);
            Session::put('project_id', $permissions->permission->project_id);*/

           if(isset($emp_user)){
               $cashier_session=(new CashierSession())
                   ->where('employee',$emp_user->code)
                   ->where('status','RUNING')->first();

               Session::put('cashier_session',isset($cashier_session)?$cashier_session->cashier_point:null);

               Session::put('user_id', $emp_user->code);
               Session::put('user_name', $emp_user->name);
           }


            Session::put('branch_id', $branch->code);
            Session::put('branch_name', $branch->name);
            Session::put('branch_address', $branch->address_line1.', '.$branch->address_line2.', '.$branch->address_line3.'.');
            Session::put('branch_tp', $branch->hotline);
            Session::put('working_date', $branch->working_date);



            $user_login = new UserLogin();
            $user_login->create([
                'user_register_id' => $permissions->user->id,
                'user_name' => $permissions->user->name,
                'user_email' => $permissions->user->email,
                'company_id' => $permissions->permission->company_id,
                'project_id' => $permissions->permission->project_id,
                'role_id' => $permissions->permission->role_id,
            ]);

            foreach ($permissions->permission->details as $detail) {
                $user_permissions = new Permission();
                $user_permissions->create([
                    'company_id' => $permissions->permission->company_id,
                    'project_id' => $permissions->permission->project_id,
                    'user_register_id' => $permissions->user->id,
                    'module_id' => $detail->module_id,
                    'module_order_no' => $detail->module->order_no,
                    'module_name' => $detail->module->des,
                    'module_path' => $detail->module->path,
                    'module_icon' => $detail->module->icon,
                    'module_category_id' => $detail->module->module_category_id,
                    'module_category' => $detail->module->module_category->des,
                    'module_category_icon' => $detail->module->icon,
                    'view' => $detail->view,
                    'add' => $detail->add,
                    'edit' => $detail->edit,
                    'delete' => $detail->delete,
                    'print' => $detail->print
                ]);
            }

            foreach ($permissions->report_permission->report_details as $report_detail) {


                $report_ = (new ReportXml())->where('name',$report_detail->module->path)->first();
               // dd($report_->code);
                $code_ =intval($report_['code']);

                $user_permissions_report = new ReportPermissions();

                $user_permissions_report->create([
                    'company_id' => $permissions->permission->company_id,
                    'project_id' => $permissions->permission->project_id,
                    'user_register_id' => $permissions->user->id,
                    'module_id' => $code_,
                    'module_order_no' => $report_detail->module->order_no,
                    'module_name' => $report_detail->module->des,
                    'module_path' => $report_detail->module->path,
                    'module_icon' => $report_detail->module->icon,
                    'module_category_id' => $report_detail->module->module_category_id,
                    'module_category' => $report_detail->module->report_category,
                    'module_category_icon' => $report_detail->module->icon,
                    'view' => $report_detail->view
                ]);

            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
        }
    }
    public function logout(Request $request)
    {

       // Auth::guard('admin')->logout();

        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('main_page');    //->guest(route( 'main_page' ));
    }

}
