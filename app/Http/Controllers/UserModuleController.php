<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserModuleController extends Controller
{

    public function loadPermission(Request $request){
        $user_auth=(new Permission())
            ->where('company_id',$request->session()->get('company_id'))
            ->where('project_id',$request->session()->get('project_id'))
            ->where('user_register_id',$request->session()->get('user_register_id'))
            ->orderby('module_order_no')->get();

        return compact('user_auth');
    }

}
