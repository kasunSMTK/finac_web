<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\BankAccount;
use App\Loan;
use App\Settlement;
use App\SettlementHistory;
use App\Transaction;
use App\TransactionHistory;
use App\Util\AccountInterface;
use App\Util\ApplicationVarible;
use App\Util\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use function Sodium\add;

class VoucherController extends Controller
{
    public function index(Request $request){
        $cashier = $request->session()->get('cashier_session');

        if ($cashier != null) {

            $transaction_type=ApplicationVarible::$VOUCHER_TRANSACTION_CODE;
            $loan = (new Loan())->
            where('status', ApplicationVarible::$APPLICATION_START)->
            where('available_voucher', '1')->
                whereHas('loanTypes',function ($query){
                $query->where('is_hp','0');
            })->

            orderBy('index_no')->
            get();

            $common = new Common();
            $referenceNo = $common->referenceNoGenerate($request, ApplicationVarible::$VOUCHER_PREFIX);

            $account = (new Account())->selectRaw('code,concat(name , code)as name')->
            where('account_group','BANK_ACCOUNT')->
            where('active',1)->orderBy('name')->pluck('name','code');

            $cheque = (new BankAccount())->selectRaw('code, account_number as name')->
            orderBy('name')->pluck('name','code');
            $cash =true;

            return view('transaction.voucher.index', compact('loan', 'referenceNo','account','cheque','cash','transaction_type'));
        } else {

            // alert error

        }

    }
    public function store(Request $request){
        $this->validate(Request(), [
            'agreement_no' => 'required',
            'payment_amount' => 'required',
            'payment_amount_confirmation' => ['required_with:payment_amount', 'same:payment_amount'],
        ]);
        try{

            DB::beginTransaction();
            //transaction
            $transaction = (new Transaction())->saveTransaction(
                $request->session()->get('working_date'),
                ApplicationVarible::$VOUCHER_TRANSACTION_CODE,
                $request->reference_no,
                $request->document_no,
                $request->loan_index,
                $request->session()->get('branch_id'),
                $request->client_code,
                ApplicationVarible::$ACTIVE
            );

            (new TransactionHistory())->saveTransactionHistory(
                $transaction->index_no,
                'NEW',
                $request->session()->get('user_id'),
                'Voucher Payment'
            );
            //settlement
            $settlement_ary = (new Settlement())->updateSettlementAmount(
                $request->loan_index,
                $request->settlement);

            //settlement history
            (new SettlementHistory())->saveSettlementHistories(
                $settlement_ary,
                $transaction->index_no,
                ApplicationVarible::$VOUCHER_TRANSACTION_CODE,
                $request->session()->get('working_date')
            );

            //call mix payment transaction\



            (new AccountTransaction())->MixPaymentAccountTransaction(
                $request,
                $transaction->index_no,
                ApplicationVarible::$VOUCHER_TRANSACTION_CODE,
                'Voucher Payment'.$request->agreement_no,
                AccountInterface::$VOUCHER_AMOUNT_DEBIT_CODE,
                $request->payment_amount,
                $request->payment_cash,
                $request->cheque_information,
                $this->getAccountPayment($request),
                    0);


            DB::commit();
            //  $this->reports($transaction->index_no,ApplicationVarible::$RECEIPT);
            return ["save", config('app.FLD')."loan_voucher",$transaction->index_no,ApplicationVarible::$VOUCHER_TRANSACTION_CODE];




        }catch(Exception $e){
            DB::rollback();
            return $e;
        }


    }
    private function getAccountPayment(Request $request){
        $account_payment = [];
        if(isset($request->payment_account) && floatval($request->payment_account)>0){
            $account_payment = ['account'=>$request->account,'payment_account'=>$request->payment_account];

        }

        return $account_payment;

    }



}
