<?php

namespace App\Http\Middleware;


use Closure;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\Routing\Annotation\Route;


class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       /* return $next(Auth::check());*/
    /*   dd($request->session()->has('branch_id'));*/




        if($request->session()->has('branch_id') &&
            $request->session()->has('working_date') &&
            $request->session()->has('user_id')){
            return $next($request);
        }else{

            return redirect('/');
        }
    }
}
