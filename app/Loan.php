<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Loan extends Model
{
    use Notifiable;

    public $table ='loan';

    protected $fillable=[
        'index_no' ,
        'application_reference_no' ,
        'application_document_no' ,
        'application_transaction_date' ,
        'loan_reference_no' ,
        'loan_document_no' ,
        'loan_transaction_date' ,
        'branch' ,
        'client' ,
        'supplier' ,
        'agreement_no' ,
        'loan_type' ,
        'sales_invoice' ,
        'loan_group' ,
        'loan_amount' ,
        'installment_amount' ,
        'installment_count' ,
        'interest_rate' ,
        'expected_loan_date',
        'loan_date' ,
        'payment_term' ,
        'panalty' ,
        'panalty_type' ,
        'panalty_rate' ,
        'panalty_amount' ,
        'grace_days' ,
        'reason' ,
        'approve_employee' ,
        'loan_officer',
        'recovery_officer' ,
        'note' ,
        'available_receipt' ,
        'available_voucher' ,
        'profile' ,
        'situation' ,
        'sp_note' ,
        'status' ,
        'print_do' ,
        'rebate_approve' ,
        'receipt_block' ,
        'is_npl'
    ];

    protected $primaryKey='index_no';
/*    public $incrementing=false;*/
    public $timestamps=false;

    public function clients(){

        return $this->belongsTo(Client::class,'client','code');
    }
    public function loanTypes(){

        return $this->belongsTo(LoanType::class,'loan_type','code');
    }
    public function loanGroup(){

        return $this->belongsTo(LoanGroup::class,'loan_group','code');
    }
    public function suppliers(){

        return $this->belongsTo(Client::class,'client','code')->where('supplier','1');
    }
    public function settlements(){

        return $this->hasMany(Settlement::class);
    }
    public function clientString(){

        return $this->belongsTo(Client::class,'client','code')->select('code','name');
    }
    public function loanTypeString(){

        return $this->belongsTo(LoanType::class,'loan_type','code')->select('code','name');
    }
    public function loanOfficers(){

        return $this->belongsTo(Employee::class,'loan_officer','code')->select('code','name');
    }
    public function recoveryOfficers(){

        return $this->belongsTo(Employee::class,'recovery_officer','code')->select('code','name');
    }

    public function updateAvailableReceipt($index){

        $loan = (new Loan())->find($index);
        $loan->available_receipt = "1";
        $loan->update();

    }





}
