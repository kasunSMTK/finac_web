<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanDocument extends Model
{
    protected $table='loan_document';
    public $primaryKey='loan';
    public $timestamps=false;
    protected $fillable=[
        'loan',
        'document'
    ];

}
