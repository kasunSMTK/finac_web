<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanGroup extends Model
{
    protected $table="loan_group";

    public $fillable=[
        'code',
        'name'
    ];

    public $primaryKey='code';
    public $timestamps=false;
    public $incrementing=false;
}
