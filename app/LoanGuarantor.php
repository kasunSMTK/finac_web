<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanGuarantor extends Model
{
    protected $primaryKey='loan';
    public $incrementing=false;
    public $timestamps=false;
    public $table='loan_guarantor';

    protected $fillable=[
        'loan' ,
        'guarantor'
    ];
}
