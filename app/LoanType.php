<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
    protected $table='loan_type';

    public $fillable=[
        'code' ,
        'prefix' ,
        'name' ,
        'minimum_allocation' ,
        'maximum_allocation' ,
        'minimum_interest_rate' ,
        'maximum_interest_rate',
        'installment_count' ,
        'payment_term' ,
        'grace_days' ,
        'interest_method',
        'available_group_loan' ,
        'panalty_available' ,
        'panalty_type',
        'panalty_rate' ,
        'panalty_amount' ,
        'is_hp' ,
        'active' ,
        'last_agrement_no' ,
        'agrement_no_width'
    ];
    public $primaryKey='code';
    public $timestamps=false;
    public $incrementing=false;
}
