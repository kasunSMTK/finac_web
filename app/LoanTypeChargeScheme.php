<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanTypeChargeScheme extends Model
{
    protected $table ="loan_type_charge_scheme";
/*    protected $primaryKey=["loan_type","charge_scheme"];*/

    public $fillable=[
        'loan_type',
        'charge_scheme'
    ];

    public function loan_types(){

        return $this->belongsTo(LoanType::class,'loan_type','loan_type');
    }
    public function chargeScheme(){
        return $this->belongsTo(ChargeScheme::class,'charge_scheme','code');
    }


}
