<?php

namespace App;

use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Payment extends Model
{
    public $table = 'payment';
    protected $primaryKey = 'index_no';
    public $fillable = [
        'index_no',
        'transaction_date',
        'branch',
        'client',
        'cashier_session',
        'amount',
        'transaction',
        'transaction_type',
        'status'
    ];
    // public $incrementing = false;
    public $timestamps = false;

    public function storePayment($branch, $client, $cashier_session, $cash, $cheque, $account, $bank, $transaction, $transaction_type)
    {

        $total_amount = floatval($cash) + floatval($cheque) + floatval($account) + floatval($bank);

        $payments = [
            "transaction_date" => Session::get('working_date'),
            "branch" => $branch,
            "client" => $client,
            "cashier_session" => $cashier_session,
            "amount" => $total_amount,
            "transaction" => $transaction,
            "transaction_type" => $transaction_type,
            "status" => "ACTIVE"
        ];
        $index_payment = $this->create($payments);


        $payment_object = $this->storePaymentInformation($index_payment->index_no, floatval($cash), floatval($cheque), floatval($account), floatval($bank), $transaction, $transaction_type);

        return $payment_object;
    }

    public function storePaymentInformation($index_payment, $cash, $cheque, $account, $bank, $transaction, $transaction_type)
    {


        if ($cash > 0) {
            $payments_infor = [
                "payment" => $index_payment,
                "payment_setting" => $this->getPaymentSettingCode($transaction_type,'CASH'),
                "amount" => $cash,
                "transaction" => $transaction,
                "transaction_type" => $transaction_type
            ];
            (new PaymentInformation())->create($payments_infor);

        }
        if ($account > 0) {


            $payments_infor = [
                "payment" => $index_payment,
                "payment_setting" => $this->getPaymentSettingCode($transaction_type,'ACCOUNT'),
                "amount" => $account,
                "transaction" => $transaction,
                "transaction_type" => $transaction_type
            ];

            (new PaymentInformation())->create($payments_infor);

        }
        if ($bank > 0) {
            $payments_infor = [
                "payment" => $index_payment,
                "payment_setting" => $this->getPaymentSettingCode($transaction_type,'BANK'),
                "amount" => $bank,
                "transaction" => $transaction,
                "transaction_type" => $transaction_type
            ];
            (new PaymentInformation())->create($payments_infor);

        }
        if ($cheque > 0) {
            if($transaction_type==ApplicationVarible::$TEMPORARY_RECEIPT_TRANSACTION_CODE){
                $payments_infor = [
                    "payment" => $index_payment,
                    "payment_setting" => $this->getPaymentSettingCode($transaction_type,'CHEQUE'),
                    "amount" => $cheque,
                    "transaction" => $transaction,
                    "transaction_type" => $transaction_type
                ];
                $payment_object = (new PaymentInformation())->create($payments_infor);

            }else{

                $payments_infor = [
                    "payment" => $index_payment,
                    "payment_setting" => $this->getPaymentSettingCode($transaction_type,'CHEQUE'),
                    "amount" => $cheque,
                    "transaction" => $transaction,
                    "transaction_type" => $transaction_type
                ];
                $payment_object = (new PaymentInformation())->create($payments_infor);
            }


            return $payment_object;
        }

    }

    private function getPaymentSettingCode($transaction_type,$type){

        $code = (new PaymentSetting())
            ->where('transaction_type',$transaction_type)
            ->where('type',$type)->where('active','1')->first();


        return $code->code;
    }
}
