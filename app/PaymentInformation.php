<?php

namespace App;

use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;

class PaymentInformation extends Model
{
    public $table = 'payment_information';
    protected $primaryKey='index_no';
    public $fillable=[
        'index_no' ,
        'payment' ,
        'payment_setting' ,
        'amount' ,
        'transaction',
        'transaction_type' ,
    ];
   // public $incrementing = false;
    public $timestamps = false;

    public function payment(){

        return $this->belongsTo(Payment::class,'payment');
    }
    public function cheque(){

        return $this->belongsTo(Cheque::class,'payment_object');
    }

    public function information($index){

        return $this->where('transaction',$index)->where('payment_setting',ApplicationVarible::$TEMP_RECEIPT_CHEQUE)->get();
    }

}
