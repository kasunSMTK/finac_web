<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model
{
    protected $table = 'payment_setting';
    protected $fillable = [
        'code',
        'name',
        'transaction_type',
        'account' ,
        'credit_or_debit' ,
        'sort_index' ,
        'type' ,
        'active'
    ];
    public $primaryKey = 'code';
    public $incrementing = false;
    public $timestamps = false;
}
