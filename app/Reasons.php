<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reasons extends Model
{
    protected $table = 'reason_setup';
    public $primaryKey = 'code';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'code',
        'description',
    ];

}
