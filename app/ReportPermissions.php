<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportPermissions extends Model
{
    protected $table = 'pro_report_permissions';
    protected $fillable = [
        'company_id',
        'project_id',
        'user_register_id',
        'module_id',
        'module_order_no',
        'module_name',
        'module_path',
        'module_icon',
        'module_category_id',
        'module_category',
        'module_category_icon',
        'view'
    ];
    public $timestamps = false;
}
