<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportXml extends Model
{
    protected $fillable=[
        'code',
        'name',
        'category' ,
        'reports'
    ];
    protected $primaryKey='code';
    public $table='report';
    public $incrementing=false;
    public $timestamps=false;
}
