<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    Protected  $filltable=[
        'code',
        'name'
    ];

    protected  $table='route';

    public $incrementing = false;
    protected $primaryKey = 'code';
    public $timestamps = false;

    public function clients(){
        $this->hasMany(Client::class,'route','code');
    }
   /* public function getRouteListAttribute()
    {
        return $this->Route->lists('code')->all();
    }*/
}
