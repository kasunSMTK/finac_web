<?php

namespace App;

use App\Util\ApplicationVarible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Settlement extends Model
{
    protected $table = 'settlement';
    public $primaryKey = 'index_no';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'index_no',
        'transaction_date',
        'branch',
        'client',
        'loan',
        'installment_no',
        'transaction',
        'transaction_type',
        'description',
        'amount',
        'balance_amount',
        'rebate_amount',
        'due_date',
        'settlement_type',
        'account',
        'status'
    ];

    public function loans()
    {

        return $this->belongsTo(Loan::class, 'loan', 'index_no');
    }

    public function settlementType()
    {

        return $this->belongsTo(SettlementType::class, 'settlement_type', 'code')
            ->orderBy('priority');
    }

    public function updateSettlementAmount($loan_index, $update_query)
    {


        $settlement_ary = [];
        $i = 0;
        if (isset($update_query) && isset($loan_index)) {
            foreach ($update_query as $key => $row) {
                foreach ($row as $kay1 => $row1) {
                    foreach ($row1 as $kay2 => $row2) {
                        foreach ($row2 as $kay3 => $row3) {
                            foreach ($row3 as $kay4 => $row5) {


                                if (isset($row5)) {
                                    $settlement_ary[$i] = [
                                        'index_no' => $kay1,
                                        'balance_amount' => $kay2,
                                        'settlement_amount' => $row5
                                    ];
                                    $i++;
                                    $balance = floatval($kay2) - floatval($row5);
                                    $this->where('loan', $loan_index)
                                        ->where('index_no', $kay1)
                                        ->update(["balance_amount" => $balance >= 0 ? $balance : 0,
                                            "status" => $balance <= 0 ? 'SETTLED' : 'PENDING',"rebate_amount"=>"0"]);
                                }
                            }
                        }
                    }
                }
            }

        }


        return ($settlement_ary);


    }
    public function updateRebateSettlementAmount($loan_index, $update_query)
    {

        $settlement_ary = [];
        $i = 0;
        if (isset($update_query) && isset($loan_index)) {
            foreach ($update_query as $key => $row) {
                foreach ($row as $kay1 => $row1) {
                    foreach ($row1 as $kay2 => $row2) {
                        foreach ($row2 as $kay3 => $row3) {
                            foreach ($row3 as $kay4 => $row5) {


                                if (isset($row5)) {
                                    $settlement_ary[$i] = [
                                        'index_no' => $kay1,
                                        'balance_amount' => $kay2,
                                        'settlement_amount' => $row5
                                    ];
                                    $i++;
                                    $balance =  floatval($row5);

                                    $this->where('loan', $loan_index)
                                        ->where('index_no', $kay1)
                                        ->update(["rebate_amount" => $balance >= 0 ? $balance : 0]);
                                }
                            }
                        }
                    }
                }
            }

        }

        return ($settlement_ary);


    }

    public function saveOverPayments(
        $branch_id,
        $client,
        $loan,
        $transaction,
        $transaction_type,
        $overAmount)
    {
        if ($overAmount > 0) {
        $settlement = [
            'transaction_date' => Session::get('working_date'),
            'branch' => $branch_id, // session variable
            'client' => $client,
            'loan' => $loan,
            'transaction' => $transaction,    // TRANSACTION
            'transaction_type' => $transaction_type,
            'installment_no' => '',
            'description' => 'Over payment Amount',
            'amount' => $overAmount,
            'balance_amount' => 0.00,
            'due_date' => Session::get('working_date'),
            'settlement_type' => ApplicationVarible::$OVER_PAY_RECEIPT,
            'status' => ApplicationVarible::$SETTLED,
            'account' => ''
        ];

        $this->create($settlement);

        }


    }
    public function saveSettlement(
        $branch_id,
        $client,
        $loan,
        $transaction,
        $transaction_type,
        $installment_no,
        $description,
        $amount,
        $settlement_type,
        $account,
        $status)
    {

            $settlement = [
                'transaction_date' => Session::get('working_date'),
                'branch' => $branch_id, // session variable
                'client' => $client,
                'loan' => $loan,
                'transaction' => $transaction,    // TRANSACTION
                'transaction_type' => $transaction_type,
                'installment_no' => $installment_no,
                'description' => $description,
                'amount' => $amount,
                'balance_amount' => $amount,
                'due_date' => Session::get('working_date'),
                'settlement_type' => $settlement_type,
                'status' => $status,
                'account' => $account
            ];



            $this->create($settlement);




    }



}
