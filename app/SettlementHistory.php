<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettlementHistory extends Model
{
    protected $fillable = [
        'index_no',
        'transaction',
        'transaction_type',
        'transaction_date',
        'settlement',
        'settlement_amount',
        'before_balance',
        'status'
    ];
    public $primaryKey = 'index_no';
    protected $table = 'settlement_history';
    public $incrementing = false;
    public $timestamps = false;


    public function settlements()
    {
        return $this->belongsTo(Settlement::class, 'settlement');
    }

    public function transactions()
    {
        return $this->belongsTo(Transaction::class, 'transaction');
    }

    public function saveSettlementHistories(
        $history,
        $transaction,
        $transaction_type,
        $transaction_date)
    {


        $index_no = 0;
        $settlement_amount = 0.0;
        $balance_amount = 0.0;
        $i = 0;

        if (isset($history)) {


            foreach ($history as $key => $row) {
                foreach ($row as $kay1 => $row1) {

                    if ($kay1 === 'index_no') {
                        $index_no = $row1;
                    }
                    if ($kay1 === 'settlement_amount') {
                        $settlement_amount = $row1;
                    }
                    if ($kay1 === 'balance_amount') {
                        $balance_amount = $row1;
                    }

                }
                if ($index_no != '' && $settlement_amount != '' && $balance_amount != '') {

                    $settlement_ary = [
                        'transaction' => $transaction,
                        'transaction_type' => $transaction_type,
                        'transaction_date' => $transaction_date,
                        'settlement' => $index_no,
                        'settlement_amount' => $settlement_amount,
                        'before_balance' => $balance_amount,
                        'status' => 'ACTIVE'
                    ];
                    $i++;
                    $this->create($settlement_ary);

                    $index_no = 0;
                    $settlement_amount = 0.0;
                    $balance_amount = 0.0;

                }
            }
        }
    }

}
