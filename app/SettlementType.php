<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettlementType extends Model
{
    protected $table='settlement_type';
    protected $fillable=[
        'code' ,
        'priority' ,
        'description' ,
        'credit_or_debit'
    ];
    public $primaryKey='code';
    public $timestamps=false;
    public $incrementing=false;
}
