<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemperaryReceipt extends Model
{
    public $fillable=[
        'index_no' ,
        'loan' ,
        'branch' ,
        'transaction' ,
        'amount' ,
        'status'
    ];
    public $table='temperary_receipt';
    public $incrementing=false;
    public $primaryKey = 'index_no';
    public $timestamps = false;

    public function loans(){

        return $this->belongsTo(Loan::class,'loan');
    }
    public function transactions(){

        return $this->belongsTo(Transaction::class,'transaction');
    }

    public function storeTempReceipt(
        $loan ,
        $branch ,
        $transaction ,
        $amount
    ){
       $fill_table=['loan'=>$loan,
        'branch'=>$branch,
        'transaction'=>$transaction,
        'amount'=>$amount,
        'status'=>'PENDING'];

       $this->create($fill_table);

    }
    public function updateStates($index_no,$status){

            $temp_receipt = (new TemperaryReceipt())->find($index_no);
            $temp_receipt->status = $status;
            $temp_receipt->update();

    }
    public function findAndUpdateStates($index_no,$status){

        $temp_receipt = (new TemperaryReceipt())->where('transaction',$index_no)->get();
        foreach ($temp_receipt as $list){
            $t_receipt = (new TemperaryReceipt())->find($list['index_no']);
            $t_receipt->status = $status;
            $t_receipt->update();
        }


    }
}
