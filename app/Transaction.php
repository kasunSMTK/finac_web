<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    Protected $table = 'transaction';
    public $primaryKey = 'index_no';
    public $timestamps = false;

    public $fillable = [
        'index_no',
        'transaction_date',
        'transaction_type',
        'reference_no',
        'document_no',
        'loan',
        'cashier_session',
        'branch',
        'client',
        'note',
        'status',
        'officer'
    ];

    public function saveTransaction(
        $working_date,
        $transaction_type,
        $reference_no,
        $document_no,
        $loan,
        $branch,
        $client,
        $status,
        $note = ""
    )
    {
        $transaction = [
            'transaction_date' => $working_date,
            'transaction_type' => $transaction_type,
            'reference_no' => $reference_no,
            'document_no' => $document_no == null || empty($document_no) ?
                '' : $document_no,
            'loan' => $loan,
            'branch' => $branch,
            'client' => $client,
            'status' => $status,
            'note' => $note

        ];


        $transactionSave = $this->create($transaction);


      return   $transactionSave;

    }
    public function loans(){

        return $this->belongsTo(Loan::class,'loan');

    }
    public function clients(){

        return $this->belongsTo(Client::class,'client');

    }
    public function amounts(){

        return $this->hasOne(Payment::class,'transaction');
    }
    public function paymentInformation(){

        return $this->hasMany(PaymentInformation::class,'transaction');

    }

}
