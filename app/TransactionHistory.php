<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $fillable = [
        'index_no',
        'transaction',
        'action',
        'employee',
        'note',
        'date_time'
    ];
    protected $table = 'transaction_history';
    public $timestamps = false;
    public $primaryKey = 'index_no';

    public function saveTransactionHistory(
        $transaction,
        $action,
        $employee,
        $note
    )
    {
        $transaction_history = [
            'transaction' => $transaction,
            'action' => $action,
            'employee' => $employee,
            'note' => $note

        ];


       $this->create($transaction_history);


    }

}
