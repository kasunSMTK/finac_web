<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    Protected $table = 'transaction_type';
    public $primaryKey = 'code';
    public $timestamps = false;

    public $fillable = [
        'code' ,
        'name' ,
        'reports' ,
        'print_report' ,
        'approve'
    ];

    public function reports(){

        return $this->belongsTo(ReportXml::class,'reports');
    }
}
