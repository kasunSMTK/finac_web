<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
    protected $table = 'pro_user_logins';
    protected $fillable = [
        'user_register_id',
        'user_name',
        'user_email',
        'company_id',
        'project_id',
        'role_id',
    ];
}
