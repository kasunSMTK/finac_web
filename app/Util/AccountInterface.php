<?php

namespace App\Util;

class AccountInterface
{

    public static $TYPE_AUTO = "AUTO";
    public static $CREDIT = "CREDIT";
    public static $DEBIT = "DEBIT";

    public static $LOAN_AMOUNT_DEBIT = "LOAN_AMOUNT_DEBIT";
    public static $LOAN_AMOUNT_CREDIT = "LOAN_AMOUNT_CREDIT";
    public static $LOAN_AMOUNT_DO_CREDIT = "LOAN_AMOUNT_DO_CREDIT";


    public static $DEFAULT_CHARGE_DEBIT = "DEFAULT_CHARG_DEBIT";
    public static $RECEIPT_AMOUNT_CREDIT = "RECEIPT_AMOUNT_CREDIT";
    public static $RECEIPT_PANELTY_CREDIT_CODE = "RECEIPT_PANELTY_CREDIT";
    public static $RECEIPT_INT_PNL_DEBIT_CODE = "RECEIPT_INT_PNL_DEBIT";
    public static $RECEIPT_ADVANCED_CREDIT_CODE = "RECEIPT_ADVANCED_CREDIT";

    public static $RECEIPT_AMOUNT_CRDIT_CODE = "BRECEIPT_AMOUNT_CRDIT";
    public static $BRECEIPT_PANELTY_CREDIT_CODE = "BRECEIPT_PANELTY_CREDIT";
    public static $BRECEIPT_INT_PNL_DEBIT_CODE = "BRECEIPT_INT_PNL_DEBIT";
    public static $BRECEIPT_ADVANCED_CREDIT_CODE = "BRECEIPT_ADVANCED_CREDIT";

    public static   $CHEQUE_RECEIPT_AMOUNT_CREDIT_CODE = "CHEQUE_RECEIPT_CREDIT";
    public static   $CHEQUE_RECEIPT_AMOUNT_DEBIT_CODE = "CHEQUE_RECEIPT_DEBIT";
    public static   $CHEQUE_RECEIPT_PANELTY_CREDIT_CODE = "CHEQUE_PANELTY_CREDIT";
    public static   $CHEQUE_RECEIPT_INT_PNL_DEBIT_CODE = "CHEQUE_INT_PNL_DEBIT";
    public static   $CHEQUE_RECEIPT_ADVANCED_CREDIT_CODE = "CHEQUE_ADVANCED_CREDIT";

    public static $TEMP_RECEIPT_AMOUNT_CREDIT_CODE = "TRECEIPT_AMOUNT_CREDIT";

    public static $DEPOSIT_AMOUNT_CREDIT_CODE = "DEPOSIT_AMOUNT_CREDIT";
    public static $RETURN_AMOUNT_DEBIT_CODE = "RETURN_AMOUNT_DEBIT";

    public static $VOUCHER_AMOUNT_DEBIT_CODE = "VOUCHER_AMOUNT_DEBIT";
    public static $OTHER_CHARGE_AMOUNT_DEBIT_CODE = "OTHER_CHARGE_DEBIT";

    public static $REBATE_AMOUNT_CREDIT_CODE = "REBIT_AMOUNT_CREDIT";
    public static $REBATE_PENALTY_AMOUNT_DEBIT_CODE = "REBIT_PANALTY_DEBIT";
    public static $REBATE_OTHER_CHARGE_AMOUNT_DEBIT_CODE = "REBIT_OTHRCHG_DEBIT";
    public static $REBATE_INTEREST_AMOUNT_DEBIT_CODE = "REBIT_INTEREST_DEBIT";
    public static $REBATE_INSURANCE_AMOUNT_DEBIT_CODE = "REBIT_INSURANCE_DEBIT";

//loan close...............
    public static $CREDIT_SETTLEMENT_CREDIT_CODE = "CREDIT_SETTLE_CREDIT";
    public static $CREDIT_SETTLEMENT_DEBIT_CODE = "CREDIT_SETTLE_DEBIT";

    public static $DEBIT_SETTLEMENT_CREDIT_CODE = "DEBIT_SETTLE_CREDIT";
    public static $DEBIT_SETTLEMENT_DEBIT_CODE = "DEBIT_SETTLE_DEBIT";

    public static $SETTLEMENT_DUE_DEBIT_CODE = "SETTLEMENT_DUE_DEBIT";
    public static $INTEREST_DUE_CREDIT_CODE = "INTEREST_DUE_CREDIT";
    public static $CAPITAL_DUE_CREDIT_CODE = "CAPITAL_DUE_CREDIT";
    public static $INSURANCE_DUE_CREDIT_CODE = "INSUARANCE_DUE_CREDIT";
//..........................

}