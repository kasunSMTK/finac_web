<?php

namespace App\Util;


use Illuminate\Http\Request;

class ApplicationVarible
{

    //TRANSACTION TYPES
    public static $LOAN_APPLICATION = 'LOAN_APPLICATION';
    public static $LOAN_START = 'LOAN';
    public static $RECEIPT = 'RECEIPT';
    public static $VOUCHER_TRANSACTION_CODE = "VOUCHER";
    public static $BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE = "BANK_RECEIPT";
    public static $TEMPORARY_RECEIPT_TRANSACTION_CODE = "TEMP_RECEIPT";
    public static $CHEQUE_RECEIPT_TRANSACTION_CODE = "CHEQUE_RECEIPT";
    public static $TRANSACTION_CANCEL_TRANSACTION_CODE = "TRANSACTION_CANCEL";
    public static $OTHER_CHARGE_TRANSACTION_CODE = "OTHER_CHARGE";
    public static $REBATE_TRANSACTION_CODE = "REBIT";
    public static $REBATE_APPROVE_TRANSACTION_CODE = "REBIT_APPROVE";
    public static $LOAN_CLOSE_TRANSACTION_CODE = "LOAN_CLOSE";

    //TRANSACTION PREFIX  REFERENCE GENERATE
    public static $RECEIPT_PREFIX = 'REC';
    public static $LOAN_APPLICATION_PREFIX = 'LON';
    public static $CHEQUE_DEPOSIT_PREFIX = "CHD";
    public static $CHEQUE_REALIZE_PREFIX = "CHR";
    public static $CHEQUE_RETURN_PREFIX = "CQR";
    public static $VOUCHER_PREFIX = "VOU";
    public static $OTHER_CHARGE = "OTC";
    public static $REBATE_PREFIX = 'REB';



    //LOAN STATUS
    public static $APPLICATION_PENDING = 'APPLICATION_PENDING';
    public static $APPLICATION_ACCEPT = "APPLICATION_ACCEPT";
    public static $APPLICATION_REJECT = "APPLICATION_REJECT";
    public static $APPLICATION_CANCEL = "CANCEL";
    public static $APPLICATION_START = "LOAN_START";

    public static $COLLECTED = "COLLECTED";
    public static $SUSPEND = "SUSPEND";
    //SETTLEMENT TYPE

    public static $APPLICATION_CHARGE = 'APPLICATION_CHARGE';
    public static $LOAN_INTEREST = 'LOAN_INTEREST';
    public static $LOAN_CAPITAL = 'LOAN_CAPITAL';
    public static $LOAN_AMOUNT = 'LOAN_AMOUNT';
    public static $OVER_PAY_RECEIPT = 'OVER_PAY_RECEIPT';
    public static $OTHER_CHARGES = "OTHER_CHARGE";
    public static $INSURANCE = "INSURANCE";
    public static $LOAN_PENALTY = "LOAN_PANALTY";

    //TRANSACTION STATUS
    public static $PENDING = 'PENDING';
    public static $SETTLED = 'SETTLED';
    public static $ACTIVE = 'ACTIVE';
    public static $CANCEL = 'CANCEL';
    public static $REALIZE = "REALIZE";

    //PAYMENT SETTINGS

    public static $RECEIPT_CASH = 'RECEIPT_CASH';
    public static $RECEIPT_BANK = 'RECEIPT_BANK';
    public static $RECEIPT_CHEQUE = 'RECEIPT_CHEQUE';
    public static $TEMP_RECEIPT_CHEQUE = 'TEMP_RECEIPT_CHEQUE';


    //cheque type
    public static $CLIENT = "CLIENT";
    public static $COMPANY = "COMPANY";

    //cheque status
    public static $CHQ_PENDING = "PENDING";
    public static $CHQ_DEPOSIT = "DEPOSIT";
    public static $CHQ_REALIZE = "REALIZE";
    public static $CHQ_RETURN = "RETURN";
    public static $CHQ_ISSUE = "ISSUE";


    //accounts
    public static $CHEQUE_DEPOSIT_TRANSACTION_CODE = "CHEQUE_DEPOSIT";
    public static $CHEQUE_REALIZE_TRANSACTION_CODE = "CHEQUE_REALIZE";
    public static $CHEQUE_RETURN_TRANSACTION_CODE = "CHEQUE_RETURN";


}