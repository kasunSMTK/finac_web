<?php

namespace App\Util;


use App\CompanyLeaveDay;
use App\Employee;
use App\Http\Controllers\CompanyLeaveDayController;
use App\ReportXml;
use App\TransactionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;


class Common
{


    public function referenceNoGenerate(Request $request, $types_px)
    {

        $user = $request->session()->get('user_id');
        $prefix = (new Employee())->select('prefix')->where('code', $user)->first();
        /* $WORKING_DATE = $request->session()->get('WORKING_DATE');*/

        $ldate = substr(date('YmdHis'), 2);
        /*        $user = substr($user, 0,3);*/


        $reference = $types_px . $prefix->prefix . $ldate;

        return $reference;

    }

    public function calculatePaymentSchedule(
        $interestMethod,
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date)
    {

        $scheduleObjects = [];
        switch ($interestMethod) {
            case InterestMethod::$FLAT:
                $scheduleObjects = $this->getFlatPaymentSchedule(
                    $paymentTerm,
                    $installmentCount,
                    $annualInterestRate,
                    $loanAmount,
                    $loan_date);
                break;
            case InterestMethod::$REDUCING:
                $scheduleObjects = $this->getRedusingPaymentSchedule(
                    $paymentTerm,
                    $installmentCount,
                    $annualInterestRate,
                    $loanAmount,
                    $loan_date
                );
                break;
            case InterestMethod::$FLAT_INTERST_FIRST:
                $scheduleObjects = $this->getFirstInterstPaymentSchedule(
                    $paymentTerm,
                    $installmentCount,
                    $annualInterestRate,
                    $loanAmount,
                    $loan_date
                );
                break;
            case InterestMethod::$REDUCING_100:
                $scheduleObjects = $this->getFlatResuding100PaymentSchedule();
                break;
            case InterestMethod::$FLAT_QUARTER:
                $scheduleObjects = $this->getFlatQuarterPaymentSchedule(
                    $paymentTerm,
                    $installmentCount,
                    $annualInterestRate,
                    $loanAmount,
                    $loan_date
                );
                break;
            case InterestMethod::$FLAT_INTERST_INSTALLMENT_FIRST:
                $scheduleObjects = $this->getFlatInterstInstallmentFirstPaymentSchedule(
                    $paymentTerm,
                    $installmentCount,
                    $annualInterestRate,
                    $loanAmount,
                    $loan_date
                );
                break;


        }
        return $scheduleObjects;


    }

    public function getEndDate($vLoanDate, $installmentCount, $paymentTerm)
    {
        $endDate = $vLoanDate;

        switch ($paymentTerm) {
            case "DAILY":
                $endDate = str_replace('-', '/', $endDate);
                $endDate = date('Y-m-d', strtotime($endDate . "+" . ($installmentCount + 31) . " days"));
                break;
            case "WEEKLY":
                $endDate = str_replace('-', '/', $endDate);
                $endDate = date('Y-m-d', strtotime($endDate . "+" . ($installmentCount) . " weeks"));
                break;
            case "MONTHLY":
                $endDate = str_replace('-', '/', $endDate);
                $endDate = date('Y-m-d', strtotime($endDate . "+" . ($installmentCount) . " months"));
                break;
            case "QUARTER":
                $endDate = str_replace('-', '/', $endDate);
                $endDate = date('Y-m-d', strtotime($endDate . "+" . ($installmentCount * 6) . " months"));
                break;
            default:

        }
        return $endDate;
    }

    function getAvailableDay($result, $date_set)
    {

        for ($i = 0; $i < sizeof($result); $i++) {

            if ($result[$i]->leave_date === $date_set) {

                $date_set = str_replace('-', '/', $date_set);
                $date_set = date('Y-m-d', strtotime($date_set . "+1 days"));


                return $this->getAvailableDay($result, $date_set);
            }
        }
        return $date_set;
    }

    public function getFlatPaymentSchedule(
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date)
    {
        $payment_schedule = [];
        try {

            $capitalAmount = 0;
            $interestAmount = 0;
            $totalAmount = 0;
            $paymentDate = 0;
            $currentInterestRate = 0.0;

            $last_date = $this->getEndDate($loan_date, $installmentCount, $paymentTerm);

            $result = (new CompanyLeaveDay())->
            getLeaveDays($loan_date, $last_date);


            for ($i = 0; $i < $installmentCount; $i++) {

                switch ($paymentTerm) {
                    case "DAILY":
                        $currentInterestRate = $annualInterestRate / 300;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 days"));

                        }
                        $loan_date = $this->getAvailableDay($result, $loan_date);

                        break;
                    case "WEEKLY":
                        $currentInterestRate = $annualInterestRate / 52;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+7 days"));
                        }
                        break;
                    case "MONTHLY":
                        $currentInterestRate = $annualInterestRate / 12;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 months"));
                        }
                        break;
                    case "QUARTER":
                        $currentInterestRate = $annualInterestRate / 2;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+6 months"));
                        }
                        break;
                    default:

                }
                $capitalAmount = $loanAmount / $installmentCount;
                $interestAmount = $loanAmount * $currentInterestRate / 100;
                $totalAmount = $capitalAmount + $interestAmount;
                $paymentDate = date("Y-m-d", strtotime($loan_date));

                $payment_schedule[$i] = array(
                    'Count' => $i + 1,
                    'Interest' => $interestAmount,
                    'Capital' => $capitalAmount,
                    'Amount' => $totalAmount,
                    'Pay_Date' => $paymentDate);
            }

        } catch (Exception $e) {

        }
        return $payment_schedule;

    }

    public function getRedusingPaymentSchedule(
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date
    )
    {
        $payment_schedule = [];
        try {

            $capitalAmount = 0;
            $interestAmount = 0;
            $totalAmount = 0;
            $paymentDate = 0;
            $currentInterestRate = 0.0;


            $last_date = $this->getEndDate($loan_date, $installmentCount, $paymentTerm);

            $result = (new CompanyLeaveDay())->
            getLeaveDays($loan_date, $last_date);

            $currentInterestRate = $annualInterestRate / 12 / 100;
            $installmentAmount = ($loanAmount * $currentInterestRate) / (1 - pow(1 + $currentInterestRate, $installmentCount * -1));

            $loanRunningBalance = $loanAmount;
            $totalAmount = $installmentAmount;

            for ($i = 0; $i < $installmentCount; $i++) {

                switch ($paymentTerm) {
                    case "DAILY":
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 days"));

                        }
                        $loan_date = $this->getAvailableDay($result, $loan_date);
                        break;
                    case "WEEKLY":
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+7 days"));
                        }
                        break;
                    case "MONTHLY":
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 months"));
                        }
                        break;

                    default:

                }


                $interestAmount = $currentInterestRate * $loanRunningBalance;
                $capitalAmount = $totalAmount - $interestAmount;
                $loanRunningBalance = $loanRunningBalance - $capitalAmount;
                $paymentDate = date("Y-m-d", strtotime($loan_date));


                $payment_schedule[$i] = array(
                    'Count' => $i + 1,
                    'Interest' => $interestAmount,
                    'Capital' => $capitalAmount,
                    'Amount' => $totalAmount,
                    'Pay_Date' => $paymentDate);
            }
            //  dd($payment_schedule);


        } catch (Exception $e) {

        }
        return $payment_schedule;
    }

    public function getFirstInterstPaymentSchedule(
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date
    )
    {
        $payment_schedule = [];
        try {

            $capitalAmount = 0;
            $interestAmount = 0;
            $totalAmount = 0;
            $paymentDate = 0;
            $currentInterestRate = 0.0;

            $last_date = $this->getEndDate($loan_date, $installmentCount, $paymentTerm);

            $result = (new CompanyLeaveDay())->
            getLeaveDays($loan_date, $last_date);
            $_count_last = 0;

            for ($i = 0; $i < $installmentCount; $i++) {

                switch ($paymentTerm) {
                    case "DAILY":
                        $currentInterestRate = $annualInterestRate / 300;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 days"));

                        }
                        $loan_date = $this->getAvailableDay($result, $loan_date);

                        break;
                    case "WEEKLY":
                        $currentInterestRate = $annualInterestRate / 52;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+7 days"));
                        }
                        break;
                    case "MONTHLY":
                        $currentInterestRate = $annualInterestRate / 12;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 months"));
                        }
                        break;
                    case "QUARTER":
                        $currentInterestRate = $annualInterestRate / 2;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+6 months"));
                        }
                        break;
                    default:

                }
                $capitalAmount = $loanAmount / $installmentCount;
                $interestAmount = $loanAmount * $currentInterestRate / 100;
                $totalAmount = $interestAmount;
                $paymentDate = date("Y-m-d", strtotime($loan_date));


                $_count_last++;
                if ($_count_last == $installmentCount) {
                    $_total_inerest = (($loanAmount) + ($interestAmount));
                    $payment_schedule[$i] = array(
                        'Count' => $i + 1,
                        'Interest' => $interestAmount,
                        'Capital' => $loanAmount,
                        'Amount' => $_total_inerest,
                        'Pay_Date' => $paymentDate);

                } else {
                    $payment_schedule[$i] = array(
                        'Count' => $i + 1,
                        'Interest' => $interestAmount,
                        'Capital' => '0.0',
                        'Amount' => $totalAmount,
                        'Pay_Date' => $paymentDate);

                }
            }


        } catch (Exception $e) {

        }
        return $payment_schedule;
    }

    public function getFlatResuding100PaymentSchedule()
    {
        return null;
    }

    public function getFlatQuarterPaymentSchedule(
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date)
    {
        $payment_schedule = [];
        try {

            $capitalAmount = 0;
            $interestAmount = 0;
            $totalAmount = 0;
            $paymentDate = 0;
            $currentInterestRate = 0.0;

            $last_date = $this->getEndDate($loan_date, $installmentCount, $paymentTerm);

            $result = (new CompanyLeaveDay())->
            getLeaveDays($loan_date, $last_date);


            for ($i = 0; $i < $installmentCount; $i++) {

                switch ($paymentTerm) {
                    case "DAILY":
                        $currentInterestRate = $annualInterestRate / 300;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 days"));

                        }
                        $loan_date = $this->getAvailableDay($result, $loan_date);

                        break;
                    case "WEEKLY":
                        $currentInterestRate = $annualInterestRate / 52;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+7 days"));
                        }
                        break;
                    case "MONTHLY":
                        $currentInterestRate = $annualInterestRate / 12;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 months"));
                        }
                        break;
                    case "QUARTER":
                        $currentInterestRate = $annualInterestRate / 2;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+6 months"));
                        }
                        break;
                    default:

                }
                $capitalAmount = $loanAmount / $installmentCount;
                $interestAmount = $loanAmount * $currentInterestRate / 100;
                $totalAmount = $capitalAmount + $interestAmount;
                $paymentDate = date("Y-m-d", strtotime($loan_date));

                $payment_schedule[$i] = array(
                    'Count' => $i + 1,
                    'Interest' => $interestAmount,
                    'Capital' => $capitalAmount,
                    'Amount' => $totalAmount,
                    'Pay_Date' => $paymentDate);
            }

        } catch (Exception $e) {

        }
        return $payment_schedule;

    }

    public function getFlatInterstInstallmentFirstPaymentSchedule(
        $paymentTerm,
        $installmentCount,
        $annualInterestRate,
        $loanAmount,
        $loan_date)
    {
        $payment_schedule = [];
        try {

            $capitalAmount = 0;
            $interestAmount = 0;
            $totalAmount = 0;
            $paymentDate = 0;
            $currentInterestRate = 0.0;

            $payInterst = 0;
            $payCapital = 0;

            $last_date = $this->getEndDate($loan_date, $installmentCount, $paymentTerm);

            $result = (new CompanyLeaveDay())->
            getLeaveDays($loan_date, $last_date);

            $interestAmount = ($loanAmount * $annualInterestRate) / 100;

            $yers = $installmentCount / 12.0;

            $installmentAmount = ($loanAmount + ($interestAmount * $yers)) / $installmentCount;
            $interestAmount = $interestAmount * $yers;


            for ($i = 1; $i < $installmentCount; $i++) {

                switch ($paymentTerm) {
                    case "DAILY":
                        $currentInterestRate = $annualInterestRate / 300;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 days"));

                        }
                        $loan_date = $this->getAvailableDay($result, $loan_date);

                        break;
                    case "WEEKLY":
                        $currentInterestRate = $annualInterestRate / 52;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+7 days"));
                        }
                        break;
                    case "MONTHLY":
                        $currentInterestRate = $annualInterestRate / 12;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+1 months"));
                        }
                        break;
                    case "QUARTER":
                        $currentInterestRate = $annualInterestRate / 2;
                        if ($i !== 0) {
                            $date1 = str_replace('-', '/', $loan_date);
                            $loan_date = date('Y-m-d', strtotime($date1 . "+6 months"));
                        }
                        break;
                    default:

                }
                $installmentNumber = $i - 1;
                $paymentDate = date("Y-m-d", strtotime($loan_date));
                $_total_inerest = 0.0;

                if ($interestAmount !== 0) {
                    if (($interestAmount - $installmentAmount) > 0) {

                        $payCapital = 0;
                        $payInterst = $installmentAmount;
                        $_total_inerest = ($payInterst + $payCapital);
                        $payment_schedule[$installmentNumber] = array(
                            'Count' => $installmentNumber,
                            'Interest' => $payInterst,
                            'Capital' => '0.0',
                            'Amount' => $_total_inerest,
                            'Pay_Date' => $paymentDate);

                        $interestAmount = $interestAmount - $installmentAmount;
                    } else {

                        $payInterst = $interestAmount;
                        $payCapital = $installmentAmount - $payInterst;
                        $_total_inerest = ($payInterst + $payCapital);

                        $payment_schedule[$installmentNumber] = array(
                            'Count' => $installmentNumber,
                            'Interest' => $payInterst,
                            'Capital' => $payCapital,
                            'Amount' => $_total_inerest,
                            'Pay_Date' => $paymentDate);


                        $loanAmount = $loanAmount - $payCapital;
                        $interestAmount = 0;
                    }
                } else {

                    $payInterst = 0.0;
                    $payCapital = $installmentAmount;
                    $_total_inerest = ($payInterst + $payCapital);


                    $payment_schedule[$installmentNumber] = array(
                        'Count' => $installmentNumber,
                        'Interest' => '0.0',
                        'Capital' => $payCapital,
                        'Amount' => $_total_inerest,
                        'Pay_Date' => $paymentDate);

                    $loanAmount = $loanAmount - $payCapital;


                }
            }
        } catch (Exception $e) {

        }

        return $payment_schedule;
    }
  //<a href="{{url('cash_payment/reports/ ' . 13239 .'/'.'RECEIPT')}}" name="ss" target="_blank" >reports</a>
    public function reportGenerate($transaction_no, $transaction_type)
{

    try{
        $PHPJasperXML = new PHPJasperXML();
        $PHPJasperXML->debugsql = false;


        /* $row= (new ReportXml())->select('report')->
                 where('code','30')->first();*/

        $row = (new TransactionType())->
        where('code', $transaction_type)->
        where('print_report', 1)->first();


        if (isset($row)) {
            $PHPJasperXML->arrayParameter = array(
                ReportParameter::$H_COMPANY_NAME => Session::get('branch_name'),
                ReportParameter::$H_COMPANY_ADDRESS => Session::get('branch_address'),
                ReportParameter::$H_COMPANY_TELEPHONE => Session::get('branch_tp'),
                ReportParameter::$H_COMPANY_FAX => Session::get('branch_tp'),
                ReportParameter::$H_COMPANY_EMAIL => Session::get('branch_tp'),
                ReportParameter::$TRANSACTION_NO => $transaction_no);


            $PHPJasperXML->load_xml_string($row->reports->report);

            // $PHPJasperXML->xml_dismantle($xml);
            $PHPJasperXML->transferDBtoArray();
            $PHPJasperXML->outpage("I");

        }

    }catch(Exception $exception){
        dd($exception);
    }

}

    public function reportGeneratePdf(Request $report)
    {

        try{
            $PHPJasperXML = new PHPJasperXML();
            $PHPJasperXML->debugsql = false;


            /* $row= (new ReportXml())->select('report')->
                     where('code','30')->first();*/

            $row = (new ReportXml())->
            where('code', $report->report_id)->first();


            if (isset($row)) {
                $PHPJasperAr = array(
                    ReportParameter::$H_COMPANY_NAME => Session::get('branch_name'),
                    ReportParameter::$H_COMPANY_ADDRESS => Session::get('branch_address'),
                    ReportParameter::$H_COMPANY_TELEPHONE => Session::get('branch_tp'),
                    ReportParameter::$H_COMPANY_FAX => Session::get('branch_tp'),
                    ReportParameter::$H_COMPANY_EMAIL => Session::get('branch_tp'),
                );



                if(isset($report->agreement_no)){
                    $ary = array(ReportParameter::$AGREEMENT_NO => $report->agreement_no);
                    $PHPJasperAr= array_merge($PHPJasperAr,$ary);
                }




                $PHPJasperXML->arrayParameter = $PHPJasperAr;
                $PHPJasperXML->load_xml_string($row->report);
                // $PHPJasperXML->xml_dismantle($xml);
                $PHPJasperXML->transferDBtoArray();
                $PHPJasperXML->outpage("I");

            }

        }catch(Exception $exception){
            dd($exception);
        }

    }


}

