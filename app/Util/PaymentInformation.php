<?php

namespace App\Util;

class PaymentInformation
{

    public static  $VOUCHER_ACCOUNT = "VOUCHER_ACCOUNT";
    public static  $VOUCHER_CASH = "VOUCHER_CASH";
    public static  $VOUCHER_CHEQUE ="VOUCHER_CHEQUE";

}