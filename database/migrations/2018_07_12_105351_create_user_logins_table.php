<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pro_user_logins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_register_id');
            $table->string('user_name',100);
            $table->string('user_email',100);
            $table->integer('company_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('role_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pro_user_logins');
    }
}
