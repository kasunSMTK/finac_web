<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pro_user_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('user_register_id');
            $table->unsignedInteger('module_id');
            $table->integer('module_order_no')->default(0);
            $table->string('module_name',50);
            $table->string('module_path',100);
            $table->string('module_icon',100);
            $table->integer('module_category_id');
            $table->string('module_category',50);
            $table->string('module_category_icon',100)->default('');
            $table->boolean('view');
            $table->boolean('add');
            $table->boolean('edit');
            $table->boolean('delete');
            $table->boolean('print');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pro_user_permissions');
    }
}
