$(document).ready(function () {

    $('#cheque_information_model').modal('show');

    $('#add_').click(function () {
        load_data();
        $('#cheque_information_model').modal('hide');

    });


    function load_data() {


        var token = (document.getElementsByName('_token'));
        var transaction_type = $('#transaction_type').val();
        var bank_account = $('#bank_branch').val();

        $('#table_top tbody').empty();
        $.ajax({
            type: 'post',
            url: 'cheque_transaction/table_data',
            data: {
                "_token": token[0].value,
                transaction_type: transaction_type,
                bank_account: bank_account,
            },
            success: function (response) {

                for (var i = 0; i < response.data_table.length; i++) {

                    $('#table_top > tbody:last-child')
                        .append('<tr>' +
                            '<td class="text-sm-center">' + response.data_table[i].index_no + '</td>' +
                            '<td class="text-sm-center">' + response.data_table[i].account_no + '</td>' +
                            '<td class="text-sm-center">' + response.data_table[i].cheque_no + '</td>' +
                            '<td class="text-sm-right">' + (response.data_table[i].amount).toFixed(2) + '</td>' +
                            '<td class="text-sm-center">' + response.data_table[i].cheque_date + '</td>' +
                            '<td class="text-sm-left">' + response.data_table[i].clients.name + ' [' + response.data_table[i].clients.code + ']' + '</td>' +
                            '<td class="text-sm-center"><a type="button"  class="btn btn-success btn-circle btn-sm text-gray-100"><i class="fas fa-arrow-down "></i></a></td>' +
                            '</tr>');

                }
            }
        });

    }


    $('#table_top tbody').on('click', 'tr', function () {
        /*var table = $('#table_top').DataTable();*/
        var table = $(this).closest("tr");
        //  var row_ary= table.row( this ).data() ;

        var row_ary = [];
        $(this).each(function (rowIndex, r) {
            var cols = [];
            $(this).find('th,td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            row_ary.push(cols);
        });

        $('#table_down > tbody:last-child')
            .append('<tr>' +
                '<td class="text-sm-center">' + row_ary[0][0] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][1] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][2] + '</td>' +
                '<td class="text-sm-right">' + row_ary[0][3] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][4] + '</td>' +
                '<td class="text-sm-left">' + row_ary[0][5] + '</td>' +
                '<td class="text-sm-center"><i class="fas fa-trash text-danger"></i></td>' +
                '</tr>');

        $(this).closest('tr').remove();

    });


    $('#table_down tbody').on('click', 'tr', function () {

        var table = $(this).closest("tr");


        var row_ary = [];
        $(this).each(function (rowIndex, r) {
            var cols = [];
            $(this).find('th,td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            row_ary.push(cols);
        });

        $('#table_top > tbody:last-child')
            .append('<tr>' +
                '<td class="text-sm-center">' + row_ary[0][0] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][1] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][2] + '</td>' +
                '<td class="text-sm-right">' + row_ary[0][3] + '</td>' +
                '<td class="text-sm-center">' + row_ary[0][4] + '</td>' +
                '<td class="text-sm-left">' + row_ary[0][5] + '</td>' +
                '<td class="text-sm-center"><a type="button"  class="btn btn-success btn-circle btn-sm text-gray-100"><i class="fas fa-arrow-down "></i></a></td>' +
                '</tr>');

        $(this).closest('tr').remove();

    });

    $('#btnSave').click(function () {


        var token = (document.getElementsByName('_token'));
        var transaction_type = $('#transaction_type').val();
        var table_data = getTableData($('#table_down'));
        var document_no = $('#document_no').val();
        var reference_no = $('#reference_no').val();
        var bank_branch = $('#bank_branch').val();
        var transaction_date = $('#transaction_date').val();
        var note = $('#note').val();


        $.ajax({
            type: 'post',
            url: 'cheque_transaction/save_data',
            data: {
                "_token": token[0].value,
                transaction_type: transaction_type,
                table_data: table_data,
                document_no: document_no,
                reference_no: reference_no,
                bank_branch: bank_branch,
                transaction_date: transaction_date,
                note: note,
            },
            success: function (response) {


                if (response[0] === "save") {
                    alert("Save Success ! ");

                    location.href = "/" + response[1];

                } else {
                    alert("Save Fail ! " + response);
                }

            }, error: function (xhr, status, error) {


                errorDisplay(xhr.responseText);//calling common js
            }


        });
    });


    //  get all table  data
    function getTableData(table) {
        var data = [];
        table.find('tr').each(function (rowIndex, r) {
            var cols = [];
            $(this).find('th,td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            data.push(cols);
        });
        return data;
    }


});