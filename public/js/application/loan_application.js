$(document).ready(function () {
    var result = [];
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();

    $(window).keydown(function (event) {

        if ((event.keyCode == 13) && ($(event.target)[0] != $("textarea")[0])) {

            event.preventDefault();

            return false;

        }

    });


    $('#loan_type').change(function () {
        fetch_select($(this).val());
        fetch_charge_scheme($(this).val());


    });

    $('#chk_Penalty').change(function () {

        penalty_ctatus($('#chk_Penalty').prop("checked"));
    });

    $('#panalty_type').change(function () {

        penalty_type_select($(this).val());
    });


    function fetch_select(val) {
        var token = (document.getElementsByName('_token'));

        $.ajax({
            type: 'post',
            url: 'loan_types/get_loan_type',
            data: {
                "_token": token[0].value,
                loan_type_code: val
            },
            success: function (response) {
                // console.log(response);
                $('#interest_rate').val(response.loan_type.minimum_interest_rate);
                $('#loan_amount').val(response.loan_type.maximum_allocation);
                $('#payment_term').val(response.loan_type.payment_term);
                $('#installment_count').val(response.loan_type.installment_count);
                $('#installment_amount').val('00.0');
                $('#grace_days').val(response.loan_type.grace_days);

                var chk = (response.loan_type.panalty_available == 1 ? true : false);
                var isHp = (response.loan_type.is_hp == 1 ? true : false);


                penalty_ctatus(chk);
                penalty_type_select(response.loan_type.panalty_type);
                $('#panalty_rate').val(response.loan_type.panalty_rate);
                $('#panalty_amount').val(response.loan_type.panalty_amount);
                hpLoanSelect(isHp);

                $('#interest_method').val(response.loan_type.interest_method);


                /*$('#is_panalty').prop('checked', true);*/

            }
        });
    }

    function hpLoanSelect(val) {

        if (val) {


            $('#loan_group').prop('disabled', false);
            $('#supplier').prop('disabled', false);
        } else {

            $('#loan_group').val('');
            $('#supplier').val('');

            $('#loan_group').prop('disabled', true);
            $('#supplier').prop('disabled', true);

        }


    }

    function penalty_ctatus(val) {

        $('#chk_Penalty').prop('checked', val);

        if (val) {
            $('#panalty_rate').prop('disabled', false);
            $('#panalty_amount').prop('disabled', false);

            $('#panalty_type').prop('disabled', false);

        } else {

            $('#panalty_rate').val(0);
            $('#panalty_amount').val(0);

            $('#panalty_type').val("NONE");

            $('#panalty_rate').prop('disabled', true);
            $('#panalty_amount').prop('disabled', true);

            $('#panalty_type').prop('disabled', true);
        }

    }

    function penalty_type_select(val) {

        $('#panalty_type').val(val);
        switch (val) {
            case "NONE":
                $('#panalty_rate').prop('disabled', true);
                $('#panalty_amount').prop('disabled', true);
                $('#panalty_rate').val(0);
                $('#panalty_amount').val(0);
                break;
            case "RATE":
                $('#panalty_rate').prop('disabled', false);
                $('#panalty_amount').prop('disabled', true);
                $('#panalty_amount').val(0);
                break;
            case "AMOUNT":
                $('#panalty_rate').prop('disabled', true);
                $('#panalty_amount').prop('disabled', false);
                $('#panalty_rate').val(0);
                break;

        }


    }

    $('#interest_rate').change(function () {
        Loan_Amount_calculation();
    });
    var inputInterestRate = document.getElementById('interest_rate');

    inputInterestRate.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            Loan_Amount_calculation();

        }
    });

    $('#loan_amount').change(function () {
        Loan_Amount_calculation();
    });
    var inputLoanAmount = document.getElementById('loan_amount');

    inputLoanAmount.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            Loan_Amount_calculation();
        }
    });

    $('#installment_count').change(function () {
        Loan_Amount_calculation();
    });


    $('.tab_3').click(function () {


        var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");
        var paymentTerm = $('#payment_term').val();
        var installmentCount = $('#installment_count').val();


        getLeaveDays(calanderdate, installmentCount, paymentTerm);

    });


    var inputInstallmentCount = document.getElementById('installment_count');

    inputInstallmentCount.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            Loan_Amount_calculation();
        }
    });

    $('#installment_amount').change(function () {
        $('#interest_rate').val(calculateInterest());
    });

    var inputInstallmentAmount = document.getElementById('installment_amount');

    inputInstallmentAmount.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            $('#interest_rate').val(calculateInterest());
        }
    });


    function Loan_Amount_calculation() {

        var interestMethod = $('#interest_method').val();

        if (interestMethod !== null || interestMethod !== "") {
            switch (interestMethod) {
                case "FLAT":
                    getFlatLoanInformation();
                    break;
                case "REDUCING":
                    getRedusingLoanInformation();
                    break;
                case "FLAT_INTERST_FIRST":
                    getFlatInterstFirstLoanInformation();
                    break;
                case "REDUCING_100":
                    getFlatRedusing100LoanInformation();
                    break;
                case "FLAT_QUARTER":
                    getFlatQuarterLoanInformation();
                    break;
                case "FLAT_INTERST_INSTALLMENT_FIRST":
                    getFlatInterstInstallmentFirstLoaninformation();
                    break;

            }

        }


    }


    function getFlatLoanInformation() {

        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);
        try {


            var paymentTerm = $('#payment_term').val();

            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();

            var currentInterestRate = 0.0;
            var installmentAmount = 0.0;
            var paymentCapital = loanAmount / installmentCount;

            switch (paymentTerm) {
                case "DAILY":
                    currentInterestRate = annualInterestRate / 300;
                    break;
                case "WEEKLY":
                    currentInterestRate = annualInterestRate / 52;
                    break;
                case "MONTHLY":
                    currentInterestRate = annualInterestRate / 12;
                    break;
                case "QUARTER":
                    currentInterestRate = annualInterestRate / 2;
                    break;
            }
            if (paymentTerm == "QUARTER") {
                installmentAmount = (paymentCapital + (loanAmount * currentInterestRate / 100)) * 6;
            } else {
                installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
            }
            $('#installment_amount').val(installmentAmount);
        } catch (err) {
            alert(err);
        }

    }

    function getRedusingLoanInformation() {
        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);

        try {

            var paymentTerm = $('#payment_term').val();

            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();

            var currentInterestRate = 0.0;


            switch (paymentTerm) {
                case "DAILY":
                    currentInterestRate = annualInterestRate / 12 / 100;
                    break;
                case "WEEKLY":
                    currentInterestRate = annualInterestRate / 12 / 100;
                    break;
                case "MONTHLY":
                    currentInterestRate = annualInterestRate / 12 / 100;
                    break;

            }

            var installmentAmount = (loanAmount * currentInterestRate) / (1 - Math.pow(1 + currentInterestRate, installmentCount * -1));

            $('#installment_amount').val(installmentAmount);
        } catch (err) {
            alert(err);
        }
    }

    function getFlatInterstFirstLoanInformation() {
        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);

        try {


            var paymentTerm = $('#payment_term').val();

            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();

            var currentInterestRate = 0.0;
            var installmentAmount = 0.0;


            switch (paymentTerm) {
                case "DAILY":
                    currentInterestRate = annualInterestRate / 300;
                    break;
                case "WEEKLY":
                    currentInterestRate = annualInterestRate / 52;
                    break;
                case "MONTHLY":
                    currentInterestRate = annualInterestRate / 12;
                    break;
                case "QUARTER":
                    currentInterestRate = annualInterestRate / 2;
                    break;
            }

            installmentAmount = loanAmount * currentInterestRate / 100;

            $('#installment_amount').val(installmentAmount);
        } catch (err) {
            alert(err);
        }
    }

    function getFlatRedusing100LoanInformation() {
        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);


        $('#installment_amount').val(0.00);


    }

    function getFlatQuarterLoanInformation() {
        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);

        try {


            var paymentTerm = $('#payment_term').val();

            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();

            var currentInterestRate = 0.0;
            var installmentAmount = 0.0;
            var paymentCapital = loanAmount / installmentCount;


            switch (paymentTerm) {
                case "DAILY":
                    currentInterestRate = annualInterestRate / 300;
                    break;
                case "WEEKLY":
                    currentInterestRate = annualInterestRate / 52;
                    break;
                case "MONTHLY":
                    currentInterestRate = annualInterestRate / 12;
                    break;
                case "QUARTER":
                    currentInterestRate = annualInterestRate / 2;
                    break;
            }

            installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);

            $('#installment_amount').val(installmentAmount);
        } catch (err) {
            alert(err);
        }


    }

    function getFlatInterstInstallmentFirstLoaninformation() {
        utilityHandlingNumber(['#interest_rate', '#installment_count', '#loan_amount']);
        try {

            var paymentTerm = $('#payment_term').val();

            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();

            var currentInterestRate = 0.0;
            var installmentAmount = 0.0;
            var paymentCapital = loanAmount / installmentCount;

            switch (paymentTerm) {
                case "DAILY":
                    currentInterestRate = annualInterestRate / 300;
                    break;
                case "WEEKLY":
                    currentInterestRate = annualInterestRate / 52;
                    break;
                case "MONTHLY":
                    currentInterestRate = annualInterestRate / 12;
                    break;
                case "QUARTER":
                    currentInterestRate = annualInterestRate / 2;
                    break;
            }
            if (paymentTerm == "QUARTER") {
                installmentAmount = (paymentCapital + (loanAmount * currentInterestRate / 100)) * 6;
            } else {
                installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
            }
            $('#installment_amount').val(installmentAmount);
        } catch (err) {
            alert(err);
        }
    }

    function calculateInterest() {
        try {
            var loanAmount = $('#loan_amount').val();
            var installmentCount = $('#installment_count').val();
            var installAmount = $('#installment_amount').val();
            var paymentCapital = loanAmount / installmentCount;
            var paymentTerm = $('#payment_term').val();

            var monthInters;
            var interestMethod = $('#interest_method').val();

            console.log(interestMethod);
            if (interestMethod === "FLAT_INTERST_FIRST") {

                monthInters = ((installAmount) * 100) / loanAmount;
            } else {

                monthInters = ((installAmount - paymentCapital) * 100) / loanAmount;
            }


            var interstRate = 0.0;

            switch (paymentTerm) {
                case "MONTHLY":
                    interstRate = monthInters * 12;
                    break;
                case "WEEKLY":
                    interstRate = monthInters * 52;
                    break;
                case "DAILY":
                    interstRate = monthInters * 300;
                    break;
                case "QUARTER":
                    interstRate = monthInters * 2;
                    break;
                default:

            }

        } catch (err) {
            alert(err);
            console.log(err);
        }
        return interstRate;

    }

    function utilityHandlingNumber(vartxt) {

        var txtName;
        for (var i = 0; i < vartxt.length; i++) {
            txtName = $(vartxt[i]).attr('name');

            if ($(vartxt[i]).val() == null || $(vartxt[i]).val() == '') {

                alert('Cannot Empty Value ' + txtName);
                break;
            } else if ($(vartxt[i]).val() <= 0) {

                alert('Check Amount ' + txtName);
                break;
            }

        }

    }

    function fetch_charge_scheme(val) {
        $('#table_schemes tbody').empty();
        var token = (document.getElementsByName('_token'));

        $.ajax({
            type: 'post',
            url: 'charge_scheme/get_loan_scheme',
            data: {
                "_token": token[0].value,
                loan_type_code: val
            },
            success: function (response) {
                for (var i = 0; i < response.loanChargeScheme.length; i++) {

                    /*     console.log(response.loanChargeScheme[i].charge_scheme.name);*/

                    $('#table_schemes > tbody:last-child')
                        .append('<tr><td>' + response.loanChargeScheme[i].charge_scheme.name + '</td>' +
                            '<td><input class="form-control form-control-sm" name="tbl_scheme[' + i + '][' + response.loanChargeScheme[i].charge_scheme.account + '][' + response.loanChargeScheme[i].charge_scheme.name + ']" ' +
                            'id="' + response.loanChargeScheme[i].charge_scheme.code + '" value="' + response.loanChargeScheme[i].charge_scheme.amount + '"/></td></tr>');

                }


            }
        });


    }

    $('#btnSchedule').click(function () {
        fetchPaymentSchedule();


    });


    function fetchPaymentSchedule() {


        var sh_array = calculatePaymentSchedule();
        var tot_interest = 0.0;
        var tot_capital = 0.0;
        var tot_amount = 0.0;

        $('#table_schedule tbody').empty();
        $('#table_schedule_footer tbody').empty();

        for (var i = 0; i < sh_array.length; i++) {

            tot_interest += sh_array[i].Interest;
            tot_capital += sh_array[i].Capital;
            tot_amount += sh_array[i].Amount;

/*            console.log("d d  "+i);
            console.log(sh_array[i].Capital);*/


            /*console.log(sh_array[i].Pay_Date);*/
            var countInst = Number(sh_array[i].Count) + 1;

            $('#table_schedule > tbody:last-child')
                .append('<tr>' +
                    '<td class="text-sm-left">' + sh_array[i].Pay_Date + '</td>' +
                    '<td class="text-sm-center">' + countInst + '</td>' +
                    '<td class="text-sm-right">' + (sh_array[i].Capital).toFixed(2) + '</td>' +
                    '<td class="text-sm-right">' + (sh_array[i].Interest).toFixed(2) + '</td>' +
                    '<td class="text-sm-right">' + (sh_array[i].Amount).toFixed(2) + '</td>' +

                    '</tr>');
        }


        $('#table_schedule_footer > tbody:last-child')
            .append('<tr>' +
                '<td class="text-md-left text-gray-800"> Total </tdclass>' +
                '<td> </td>' +
                '<td class="text-md-right text-gray-800">' + (tot_capital).toFixed(2) + '</td>' +
                '<td class="text-md-right text-gray-800">' + (tot_interest).toFixed(2) + '</td>' +
                '<td class="text-md-right text-gray-800">' + (tot_amount).toFixed(2) + '</td>' +
                '</tr>');


    }

    function calculatePaymentSchedule() {
        var interestMethod = $('#interest_method').val();
        var sheduleList = [];

        if (interestMethod !== null || interestMethod !== "") {
            switch (interestMethod) {
                case "FLAT":
                    sheduleList = getFlatPaymentSchedule();
                    break;
                case "REDUCING":
                    sheduleList = getRedusingPaymentSchedule();
                    break;
                case "FLAT_INTERST_FIRST":
                    sheduleList = getFirstInterstPaymentSchedule();
                    break;
                case "REDUCING_100":
                    sheduleList = getFlatResuding100PaymentSchedule();
                    break;
                case "FLAT_QUARTER":
                    sheduleList = getFlatQuarterPaymentSchedule();
                    break;
                case "FLAT_INTERST_INSTALLMENT_FIRST":
                    sheduleList = getFlatInterstInstallmentFirstPaymentSchedule();
                    break;

            }

        }
        return sheduleList;
    }


    function getFlatPaymentSchedule() {

        try {

            var capitalAmount;
            var interestAmount;
            var totalAmount;
            var paymentDate;
            var currentInterestRate = 0.0;

            var paymentTerm = $('#payment_term').val();
            var installmentCount = $('#installment_count').val();
            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();


            var payment_schedule = new Array();

            var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");


            for (var i = 0; i < installmentCount; i++) {

                switch (paymentTerm) {
                    case "DAILY":
                        currentInterestRate = annualInterestRate / 300;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'days');
                        }
                        calanderdate = getAvailableDay(result, calanderdate);
                        break;
                    case "WEEKLY":
                        currentInterestRate = annualInterestRate / 52;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(7, 'days');
                        }
                        break;
                    case "MONTHLY":
                        currentInterestRate = annualInterestRate / 12;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'months');
                        }
                        break;
                    case "QUARTER":
                        currentInterestRate = annualInterestRate / 2;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(6, 'months');
                        }
                        break;
                    default:

                }
                capitalAmount = loanAmount / installmentCount;
                interestAmount = loanAmount * currentInterestRate / 100;
                totalAmount = capitalAmount + interestAmount;
                paymentDate = calanderdate.format("YYYY-MM-DD");

                payment_schedule [i] =
                    {
                        'Count': i,
                        'Interest': interestAmount,
                        'Capital': capitalAmount,
                        'Amount': totalAmount,
                        'Pay_Date': paymentDate
                    };
            }

        } catch (err) {

            console.log(err);
        }


        return payment_schedule;

    }

    function getRedusingPaymentSchedule() {

        try {

            var capitalAmount;
            var interestAmount;
            var totalAmount;
            var paymentDate;
            var loanRunningBalance;
            var currentInterestRate = 0.0;
            var installmentAmount = 0.0;

            var paymentTerm = $('#payment_term').val();
            var installmentCount = $('#installment_count').val();
            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();


            var payment_schedule = new Array();

            var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");

            currentInterestRate = annualInterestRate / 12 / 100;
            installmentAmount = (loanAmount * currentInterestRate) / (1 - Math.pow(1 + currentInterestRate, installmentCount * -1));

            loanRunningBalance = loanAmount;
            totalAmount = installmentAmount;


            for (var i = 0; i < installmentCount; i++) {


                switch (paymentTerm) {
                    case "DAILY":

                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'days');
                        }
                        calanderdate = getAvailableDay(result, calanderdate);
                        break;
                    case "WEEKLY":

                        if (i !== 0) {
                            calanderdate = calanderdate.add(7, 'days');
                        }
                        break;
                    case "MONTHLY":

                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'months');
                        }
                        break;
                    case "QUARTER":

                        if (i !== 0) {
                            calanderdate = calanderdate.add(6, 'months');
                        }
                        break;
                    default:

                }

                currentInterestRate = annualInterestRate / 12 / 100;
                interestAmount = currentInterestRate * loanRunningBalance;
                capitalAmount = totalAmount - interestAmount;
                loanRunningBalance = loanRunningBalance - capitalAmount;
                paymentDate = calanderdate.format("YYYY-MM-DD");


                payment_schedule [i] =
                    {
                        'Count': i,
                        'Interest': interestAmount,
                        'Capital': capitalAmount,
                        'Amount': totalAmount,
                        'Pay_Date': paymentDate
                    };
            }

        } catch (err) {

            console.log(err);
        }


        return payment_schedule;

    }

    function getFirstInterstPaymentSchedule() {

        try {

            var capitalAmount;
            var interestAmount;
            var totalAmount;
            var paymentDate;
            var currentInterestRate = 0.0;

            var paymentTerm = $('#payment_term').val();
            var installmentCount = $('#installment_count').val();
            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();


            var payment_schedule = new Array();

            var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");

            var _count_last = 0;
            for (var i = 0; i < installmentCount; i++) {

                switch (paymentTerm) {
                    case "DAILY":
                        currentInterestRate = annualInterestRate / 300;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'days');
                        }
                        calanderdate = getAvailableDay(result, calanderdate);
                        break;
                    case "WEEKLY":
                        currentInterestRate = annualInterestRate / 52;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(7, 'days');
                        }
                        break;
                    case "MONTHLY":
                        currentInterestRate = annualInterestRate / 12;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'months');
                        }
                        break;
                    case "QUARTER":
                        currentInterestRate = annualInterestRate / 2;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(6, 'months');
                        }
                        break;
                    default:

                }
                capitalAmount = loanAmount / installmentCount;
                interestAmount = loanAmount * currentInterestRate / 100;
                totalAmount = interestAmount;
                paymentDate = calanderdate.format("YYYY-MM-DD");

                _count_last++;

                if (parseInt(_count_last,10) === parseInt(installmentCount,10)) {
                    var _total_inerest=(parseFloat(loanAmount) + parseFloat(interestAmount));
                    capitalAmount=parseFloat(loanAmount);

                    payment_schedule [i] =
                        {
                            'Count': i,
                            'Interest': interestAmount,
                            'Capital': capitalAmount,
                            'Amount': _total_inerest,
                            'Pay_Date': paymentDate
                        };
                } else {
                    capitalAmount=0.0;
                    payment_schedule [i] =
                        {
                            'Count': i,
                            'Interest': interestAmount,
                            'Capital': capitalAmount,
                            'Amount': totalAmount,
                            'Pay_Date': paymentDate
                        };
                }
            }

        } catch (err) {

            console.log(err);
        }


        return payment_schedule;

    }

    function getFlatResuding100PaymentSchedule() {

    }

    function getFlatQuarterPaymentSchedule() {
        try {

            var capitalAmount;
            var interestAmount;
            var totalAmount;
            var paymentDate;
            var currentInterestRate = 0.0;

            var paymentTerm = $('#payment_term').val();
            var installmentCount = $('#installment_count').val();
            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();


            var payment_schedule = new Array();

            var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");


            for (var i = 0; i < installmentCount; i++) {

                switch (paymentTerm) {
                    case "DAILY":
                        currentInterestRate = annualInterestRate / 300;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'days');
                        }
                        calanderdate = getAvailableDay(result, calanderdate);
                        break;
                    case "WEEKLY":
                        currentInterestRate = annualInterestRate / 52;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(7, 'days');
                        }
                        break;
                    case "MONTHLY":
                        currentInterestRate = annualInterestRate / 12;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'months');
                        }
                        break;
                    case "QUARTER":
                        currentInterestRate = annualInterestRate / 2;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(6, 'months');
                        }
                        break;
                    default:

                }
                capitalAmount = loanAmount / installmentCount;
                interestAmount = loanAmount * currentInterestRate / 100;
                totalAmount = capitalAmount + interestAmount;
                paymentDate = calanderdate.format("YYYY-MM-DD");

                payment_schedule [i] =
                    {
                        'Count': i,
                        'Interest': interestAmount,
                        'Capital': capitalAmount,
                        'Amount': totalAmount,
                        'Pay_Date': paymentDate
                    };
            }

        } catch (err) {

            console.log(err);
        }


        return payment_schedule;


    }

    function getFlatInterstInstallmentFirstPaymentSchedule() {
        try {

            var capitalAmount;
            var interestAmount;
            var totalAmount;
            var paymentDate;
            var currentInterestRate = 0.0;

            var paymentTerm = $('#payment_term').val();
            var installmentCount = $('#installment_count').val();
            var annualInterestRate = $('#interest_rate').val();
            var loanAmount = $('#loan_amount').val();

            var payInterst = 0;
            var payCapital = 0;


            var payment_schedule = new Array();

            var calanderdate = moment($('#expected_loan_date').val(), "YYYY-MM-DD");

            interestAmount = (loanAmount * annualInterestRate) / 100;

            var yers=installmentCount/12.0;

            var installmentAmount = (parseFloat(loanAmount) + parseFloat( interestAmount * yers) ) / installmentCount;
            interestAmount= interestAmount * yers;


            for (var i = 1; i < parseInt(installmentCount)+1; i++) {

                switch (paymentTerm) {
                    case "DAILY":
                        currentInterestRate = annualInterestRate / 300;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'days');
                        }
                        calanderdate = getAvailableDay(result, calanderdate);
                        break;
                    case "WEEKLY":
                        currentInterestRate = annualInterestRate / 52;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(7, 'days');
                        }
                        break;
                    case "MONTHLY":
                        currentInterestRate = annualInterestRate / 12;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(1, 'months');
                        }
                        break;
                    case "QUARTER":
                        currentInterestRate = annualInterestRate / 2;
                        if (i !== 0) {
                            calanderdate = calanderdate.add(6, 'months');
                        }
                        break;
                    default:

                }
                var installmentNumber = i - 1;
                paymentDate = calanderdate.format("YYYY-MM-DD");
                var _total_inerest=0.0;

                ///////////////////////////////////////////////////////////////////


                if (parseFloat(interestAmount) !== 0) {
                    if ((parseFloat(interestAmount) - parseFloat(installmentAmount)) > 0) {

                        payCapital = 0;
                        payInterst = installmentAmount;
                         _total_inerest=(parseFloat(payInterst) + parseFloat(payCapital));
                        payment_schedule [installmentNumber] =
                            {
                                'Count': installmentNumber,
                                'Interest': payInterst,
                                'Capital': payCapital,
                                'Amount': _total_inerest,
                                'Pay_Date': paymentDate
                            };
                        interestAmount = interestAmount - installmentAmount;
                    } else {

                        payInterst = interestAmount;
                        payCapital = installmentAmount - payInterst;
                         _total_inerest=(parseFloat(payInterst) + parseFloat(payCapital));

                        payment_schedule [installmentNumber] =
                            {
                                'Count': installmentNumber,
                                'Interest': payInterst,
                                'Capital': payCapital,
                                'Amount': _total_inerest,
                                'Pay_Date': paymentDate
                            };


                        loanAmount = loanAmount - payCapital;
                        interestAmount = 0;
                    }
                } else {

                    payInterst = 0;
                    payCapital = installmentAmount;
                     _total_inerest=(parseFloat(payInterst) + parseFloat(payCapital));


                    payment_schedule [installmentNumber] =
                        {
                            'Count': installmentNumber,
                            'Interest': payInterst,
                            'Capital': payCapital,
                            'Amount': _total_inerest,
                            'Pay_Date': paymentDate
                        };

                    loanAmount = loanAmount - payCapital;
                }


            }


        } catch (err) {

            console.log(err);
        }


        return payment_schedule;


    }

    function getLeaveDays(loanDate, installmentCount, paymentTerm) {

        var token = document.getElementsByName('_token');

        var end_date = getEndDate(loanDate, installmentCount, paymentTerm);


        $.ajax({
            'type': 'post',
            'url': 'leave_day/getLeaveDays',
            'data': {
                '_token': token[0].value,
                loanDate: loanDate.format("YYYY-MM-DD"),
                endDate: end_date.format("YYYY-MM-DD")
            },
            success: function (response) {


                for (var i = 0; i < response.leave_days.length; i++) {

                    result[i] = response.leave_days[i].leave_date;

                    /*console.log(response.leave_days[i].leave_date);*/
                }
            }

        });

    }

    function getEndDate(vLoanDate, installmentCount, paymentTerm) {

        var endDate = moment(vLoanDate, "YYYY-MM-DD");


        switch (paymentTerm) {
            case "DAILY":
                endDate.add((Number(installmentCount) + 31), 'days').format("YYYY-MM-DD");
                break;
            case "WEEKLY":
                endDate.add(installmentCount, 'weeks').format("YYYY-MM-DD");
                break;
            case "MONTHLY":
                endDate.add(installmentCount, 'months').format("YYYY-MM-DD");
                break;
            case "QUARTER":
                endDate.add(Number(installmentCount) * 6, 'months').format("YYYY-MM-DD");
                break;
            default:

        }
        return endDate;
    }


    function getAvailableDay(response, date_set) {

        for (var i = 0; i < response.length; i++) {

            if (moment(response[i], "YYYY-MM-DD").isSame(moment(date_set), "YYYY-MM-DD")) {

                date_set.add(1, 'days');

                return getAvailableDay(response, date_set);
            }
        }
        return date_set;
    }

    $('#btnSave').click(function () {

        var form = $('#loanApp');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {

                if (response[0] == "save") {
                    alert("Save Success !");
                    //location.href = response[1];
                    window.location.replace(response[1]);
                } else {
                    alert("Save Fail ! " + response[0]);
                }

            },
            error: function (xhr, status, error) {
                errorDisplay(xhr.responseText);//calling common js
            }
        });
    });


    $('#addGuarantor').click(function () {
        var tmp = $('#guarantor').val();
        var token = document.getElementsByName('_token');

        $.ajax({
            'type': 'POST',
            'url': 'clients/getClients',
            data: {
                '_token': token[0].value,
                code: tmp
            },
            success: function (response) {

                $('#tbl_guarantor > tbody:last-child')
                    .append('<tr><td>' + response.guarantors.code + '</td>' +
                        '<td><input class="form-control form-control-sm" border="0" readonly name="tbl_guarantor[' + response.guarantors.code + ']" ' +
                        'value="' + response.guarantors.name + '"/></td>' +
                        '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                        '<i class="fas fa-trash"></i></td></tr>');
            }
        });
    });


    $('#tbl_guarantor tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });

    $('#addDocument').click(function () {
        var description = $('#description').val();
        /* var reference_link = $('#select_file').val();*/
        var reference_link = $("#select_file")[0].files[0].name;

        if (description != null) {

            $('#tblAddDocument > tbody:last-child')
                .append('<tr><td>' + description + '</td>' +
                    '<td><input class="form-control form-control-sm" border="0" readonly name="tbl_documents[' + description + ']" ' +
                    'value="' + reference_link + '"/></td>' +
                    '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                    '<i class="fas fa-trash"></i></td></tr>');
        }
        $('#description').val("");
        $('.custom-file-input').siblings(".custom-file-label").addClass("selected").html("");

    });

    $('#tblAddDocument tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();

    });

    $(".custom-file-input").on("change", function () {//file/upload

        var fileName = document.getElementById("select_file").files[0].name;
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);


        /*     console.log(document.getElementById("customFile").value);
         var fileName = document.getElementById("customFile").files[0].name;
         console.log(fileName);


         $(this).siblings(".custom-file-label").addClass("selected").html(fileName);*/


        /*     var fileName = $(this).val().split("\\").pop();
         $(this).siblings(".custom-file-label").addClass("selected").html(fileName);*/
    });


    function getChargesScheme() {

        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]);
        var token = document.getElementsByName('_token');
        console.log(formData);

        $.ajax({
            'type': 'POST',
            'url': 'file/upload',
            'dataType': 'JSON',

            data: {
                '_token': token[0].value,
                select_file: formData
            },
            success: function (response) {


            }
        });
    }


});

