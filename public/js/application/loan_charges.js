$(document).ready(function () {


    $("#dataTable").on("click", "tr", function (row, $el, field) {

        var cur_row = $(this).closest('tr');
        var col2 = cur_row.find('td:eq(2)').text();
         var col1 = cur_row.find('td:eq(1)').text();
         var col5 = cur_row.find('td:eq(5)').text();

        $('#agreement_no').val(col2);

        var span = document.getElementById("agreement_details");
        span.textContent = col1+" - "+col5;
        // $('#client').val(col1);
        // $('#address_line').val(col5);

        $('#search_model').modal('hide');
        addRow();

    });



    function addRow(){
        var x = (document.getElementById("table_settlement").rows.length)-1;
        $('#table_settlement > tbody:last-child')
            .append('<tr>' +
                '<td><input class="form-control form-control-sm text-primary account_evt" type="text" id="account_'+x+'" name="account_'+x+'"/> </td>' +
                '<td><input class="form-control form-control-sm text-primary" type="text" id="description_'+x+'" name="description_'+x+'"/></td>' +
                '<td><input class="form-control form-control-sm text-primary text-lg-right"  type="number" id="amount_'+x+'" name="amount_'+x+'"/></td>' +
                '<input type="hidden" id="code_'+x+'" name="code_'+x+'">'+
                '</tr>');

    }

    $("#table_settlement").on("click", " tr td ", function () {

        var cur_row = $(this).closest('td');
        if(cur_row[0].cellIndex === 0){
            $('#com_search_model').modal('show');
            var cur_row_q = $(this).closest('tr');
            console.log(cur_row_q[0].rowIndex);

            $('#hid_id').val((cur_row_q[0].rowIndex)-1);
        }



    });
    $('#btnAddRaw').click(function () {

        addRow();
    });

    $('table.dataTable1').DataTable();

    $("table.dataTable1").on("click", "tr", function () {

        var cur_row = $(this).closest('tr');
        var col1 = cur_row.find('td:eq(0)').text();
        var col2 = cur_row.find('td:eq(1)').text();

        var concat_string = (col1+' '+col2);

        var index = $('#hid_id').val();

        console.log(index);
        $('#account_'+index).val(concat_string);
        $('#code_'+index).val(col1);


        $('#com_search_model').modal('hide');
    });
    //payment/data_save
    $('#btnSave').click(function () {

        $('#save_confirm').modal('show');

    });
    //save
    $('#btnSaveConfirm').click(function () {
        $('#save_confirm').modal('hide');

        var token = (document.getElementsByName('_token'));
        var form = $('#loan_charge');
        //  var form = $("#tbl2 :input").serialize();

        sendToArray();

            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    if (response[0] == "save") {
                        //   alert("Save Success !");

                      //  location.href = response[1];

                        window.location.replace(response[1]);

                    } else {
                        alert("Save Fail ! " + response);
                    }
                },
                error: function (xhr, status, error) {
                    alert(error+" Contact System Administrator");


                    errorDisplay(xhr.responseText);//calling common js
                }
            });



    });

    $('#cancelConfirm').click(function () {
        $('#save_confirm').modal('hide');

    });

    function sendToArray(){

        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

        $settlement = [];
        for (var b = 0; b < rows-1; b++) {

            //console.log($('#account_'+b).val());

            $settlement[b]=[$('#code_'+b).val(),$('#account_'+b).val(),$('#description_'+b).val(),$('#amount_'+b).val()]

        }

        $('#settlement').val(JSON.stringify($settlement));

    }

    function calculateTotal(){
        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

       var total =0.0;
        for (var b = 0; b < rows-1; b++) {

            total += parseFloat($('#amount_'+b).val());

        }
        $('#total_amount').val(total);

        console.log("asdas "+total);
    }
    $("#table_settlement").on("change", "tr", function () {

        calculateTotal();
    });



})
