/**
 * Created by kasun on 4/30/2020.
 */
$(document).ready(function(){
    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();

    var ary_date = new Array();
    var installment_count = 0;
    $("#dataTable").on("click", "tr", function (row, $el, field) {

        var cur_row = $(this).closest('tr');
        var col2 = cur_row.find('td:eq(2)').text();
        var col1 = cur_row.find('td:eq(1)').text();
        var col5 = cur_row.find('td:eq(5)').text();

        $('#agreement_no').val(col2);
        $('#client').val(col1);
        $('#address_line').val(col5);
        dataSet(col2);
        $('#search_model').modal('hide');
    });

    $('#agreement_no').click(function () {
        clear()

    });

    function dataSet(val) {

        var balance_amount = 0.00;
        var penalty_balance = 0.00;
        var capital_balance = 0.00;
        var interest_balance = 0.00;
        var token = (document.getElementsByName('_token'));
        var transaction_type=$('#transaction_type').val();

        $('#table_settlement tbody').empty();
        $.ajax({
            type: 'post',
            url: 'loan_close/data_set',
            data: {
                "_token": token[0].value,
                agreement_no: val,
                transaction_type:transaction_type
            },
            success: function (response) {

                ary_date = new Array();
                var result = response.settlement.sort(function (a, b) {
                    var keyA = new Date(a.due_date),
                        keyB = new Date(b.due_date);

                    //  ((keyA < keyB) ? -1 : ((keyA > keyB) ? 1 : 0))

                    return a.installment_no - b.installment_no || parseFloat(a.settlement_type.priority) - parseFloat(b.settlement_type.priority);
                });

                // console.log(result);

                $('#loan_amount').val(response.data_list.loan_amount);
                $('#loan_index').val(response.data_list.index_no);
                $('#client_code').val(response.data_list.client);
                $('#installment_amount').val(parseFloat(response.data_list.installment_amount).toFixed(2));
                $('#reference_no').val(response.data_list.loan_reference_no);

                installment_count = response.data_list.installment_count;

                for (var i = 0; i < response.settlement.length; i++) {

                    balance_amount += parseFloat(response.settlement[i].balance_amount);

                    if (response.settlement[i].settlement_type.code === "LOAN_PANALTY") {
                        penalty_balance += parseFloat(response.settlement[i].balance_amount);
                    }
                    if (response.settlement[i].settlement_type.code === "LOAN_INTEREST") {
                        interest_balance += parseFloat(response.settlement[i].balance_amount);
                    }
                    if (response.settlement[i].settlement_type.code === "LOAN_CAPITAL") {
                        capital_balance += parseFloat(response.settlement[i].balance_amount);
                    }
                    ary_date[i] =
                        {
                            "date": (response.settlement[i].due_date).toString(),
                            "amount": (response.settlement[i].amount).toString(),
                            "balance": (response.settlement[i].balance_amount).toString()
                        };

                    $('#table_settlement > tbody:last-child')
                        .append('<tr>' +
                            '<td class="text-sm-left">' + response.settlement[i].due_date + '</td>' +
                            '<td class="text-sm-center">' + response.settlement[i].installment_no + '</td>' +
                            '<td class="text-sm-center">' + (response.settlement[i].settlement_type.code === "LOAN_PANALTY" ? "Loan Penalty" : response.settlement[i].description) + '</td>' +
                            '<td class="text-sm-right">' + parseFloat(response.settlement[i].amount).toFixed(2) + '</td>' +
                            '<td class="text-sm-right">' + parseFloat(response.settlement[i].balance_amount).toFixed(2) + '</td>' +
                            '<td class="text-sm-right">' + (response.settlement[i].status) + '</td>' +
                            '</tr>');
                }

                $('#loan_balance').val(parseFloat(balance_amount).toFixed(2));
                $('#penalty_balance').val(parseFloat(penalty_balance).toFixed(2));
                $('#cap_bal').val(parseFloat(capital_balance).toFixed(2));
                $('#int_bal').val(parseFloat(interest_balance).toFixed(2));

            }
        });
    }
    function clear() {

        $('#agreement_no').val("");
        $('#client').val("");
        $('#address_line').val("");
        $('#loan_amount').val(0.0);
        $('#installment_amount').val(0.0);
        $('#loan_balance').val(0.0);
        $('#penalty_balance').val(0.0);
        $('#cap_bal').val(0.0);
        $('#int_bal').val(0.0);
        $('#table_settlement tbody').empty();


    }
    $('#note').prop('disabled',true);
    $('#settlement_close').click(function () {
        if(document.getElementById('settlement_close').checked){
            $('#note').prop('disabled',true);
            $('#span_message').text("Warning !  Loan will be close as settlement,  Check loan balance and settlement payment should be include.");
        }
    })
    $('#suspend_close').click(function () {
        if(document.getElementById('suspend_close').checked){
            $('#note').prop('disabled',false);
            $('#span_message').text("Warning !  Loan suspend , agreement is temporally disable ex:(Payments,other transaction etc..)");
        }
    })
    $('#error_close').click(function () {
        if(document.getElementById('error_close').checked){
            $('#note').prop('disabled',true);
            $('#span_message').text("Warning !  Loan error close ,agreement is permanently remove loan details and payment.  ");
        }
    })
    $('#reschedule_close').click(function () {
        if(document.getElementById('reschedule_close').checked){
            $('#note').prop('disabled',true);
            $('#span_message').text("not implemented yet");
        }
    })
})