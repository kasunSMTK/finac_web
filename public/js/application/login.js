$(document).ready(function () {


    $('#btnSave').click(function () {
        var form = $('#loginApp');
        console.log(form);
        $.ajax({
            type: "POST",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {

                if (response === "save") {
                    alert("Save Success !");
                    location.href = "/clients";
                } else {
                    alert("Save Fail ! " + response);
                }

            },
            error: function (xhr, status, error) {

            }
        });
    });
});