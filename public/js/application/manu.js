$(document).ready(function () {

    var token = document.getElementsByName('_token');


    var getUrl = window.location;
    var baseUrl = getUrl.pathname.split('/')[1];


    console.log(baseUrl);

    $.ajax({
        'type': 'post',
        'url': 'main_page/login/permission',
        'data': {
            '_token': token[0].value
        },
        success: function (response) {

            var link_reg = [];
            var link_tranc = [];
            var link_payment = [];
            var link_cheque_transaction = [];
            var link_voucher = [];
            var link_approve = [];
            var link_report = [];

            var HEAD_URL = "";// this is using localhost
         //   var HEAD_URL = "/"+baseUrl;// this is using hosting


            for (var i = 0; i < response.user_auth.length; i++) {

                if (response.user_auth[i].module_category_id == 1) {
                    var link = $("<a>");

                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_reg[i] = link;
                } else if (response.user_auth[i].module_category_id == 2) {
                    var link = $("<a>");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_tranc[i] = link;
                } else if (response.user_auth[i].module_category_id == 6) {
                    var link = $("<a>");

                    link.addClass(response.user_auth[i].module_category_icon);
                    link.addClass("collapse-item ");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link_payment[i] = link;


                } else if (response.user_auth[i].module_category_id == 7) {
                    var link = $("<a>");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_cheque_transaction[i] = link;
                } else if (response.user_auth[i].module_category_id == 8) {
                    var link = $("<a>");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_voucher[i] = link;
                }
                else if (response.user_auth[i].module_category_id == 23) {
                    var link = $("<a>");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_approve[i] = link;
                }else if (response.user_auth[i].module_category_id == 13) {
                    var link = $("<a>");
                    link.attr("href", HEAD_URL+response.user_auth[i].module_path);
                    link.attr("title", "some Description");
                    link.text(response.user_auth[i].module_name);
                    link.addClass("collapse-item");
                    link_report[i] = link;


                }

/*  <i class="fas fa-fw text-danger">SS</i> */
            }
            $(".registration_client").html(link_reg);
            $(".transaction_loan").html(link_tranc);
            $(".transaction_payments").html(link_payment);
            $(".cheque_transaction").html(link_cheque_transaction);
            $(".loan_voucher").html(link_voucher);
            $(".approval").html(link_approve);
            $(".reports").html(link_report);


        }

    });


});
