$(document).ready(function () {

    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();

    $('#rebate_amount').prop('disabled', true);
    var ary_date = new Array();
    var installment_count = 0;
    // $("#dataTable").on("click", "tr", function (row, $el, field) {
    //
    //     var cur_row = $(this).closest('tr');
    //     var col2 = cur_row.find('td:eq(2)').text();
    //     var col1 = cur_row.find('td:eq(1)').text();
    //     var col5 = cur_row.find('td:eq(5)').text();
    //
    //     $('#agreement_no').val(col2);
    //     $('#client').val(col1);
    //     $('#address_line').val(col5);
    //     dataSet(col2);
    //     $('#search_model').modal('hide');
    //
    //
    // });

    // $('#agreement_no').change(function () {
    //     $('#agreement_no').prop('disabled', true);
    //     $('#rebate_amount').prop('disabled', false);
    //     clear();
    //     dataSet( $('#agreement_no').val());
    // });
    clear();
    dataSet($('#agreement_no').val());
    $('#agreement_no').prop('disabled', true);
    $('#rebate_amount').prop('disabled', false);

    function clear() {

        //$('#agreement_no').val("");
        $('#client').val("");
        $('#address_line').val("");
        $('#loan_amount').val(0.0);
        $('#installment_amount').val(0.0);
        $('#loan_balance').val(0.0);
        $('#penalty_balance').val(0.0);
        $('#after_balance').val(0.0);
        $('#areas_amount').val(0.0);
        $('#areas_vol').val(0.0);
        $('#rebate_amount').val("");
        $('#rebate_percentage').val("");
        $('#table_settlement tbody').empty();


    }

    function dataSet(val) {


        var index = ($('#loan_index').val())
        var balance_amount = 0.00;
        var payment_amount = 0.00;
        var penalty_balance = 0.00;
        var token = (document.getElementsByName('_token'));
        var transaction_type=$('#transaction_type').val();

        $('#table_settlement tbody').empty();
        $.ajax({
            type: 'post',
            url: 'edit/dataSet',
            data: {
                "_token": token[0].value,
                agreement_no: val,
                transaction_type:transaction_type
            },
            success: function (response) {



                ary_date = new Array();
                var result = response.settlement.sort(function (a, b) {
                    var keyA = new Date(a.due_date),
                        keyB = new Date(b.due_date);

                    //  ((keyA < keyB) ? -1 : ((keyA > keyB) ? 1 : 0))

                    return a.installment_no - b.installment_no || parseFloat(a.settlement_type.priority) - parseFloat(b.settlement_type.priority);
                });



                $('#loan_amount').val(response.data_list.loan_amount);
                $('#loan_index').val(response.data_list.index_no);
                $('#client_code').val(response.data_list.client);
                $('#installment_amount').val(parseFloat(response.data_list.installment_amount).toFixed(2));
                    $('#client').val(response.data_list.clients.name);
                    $('#address_line').val(response.data_list.clients.address_line1+' '+response.data_list.clients.address_line2+' '+response.data_list.clients.address_line3);

                installment_count = response.data_list.installment_count;


                for (var i = 0; i < response.settlement.length; i++) {

                    balance_amount += parseFloat(response.settlement[i].balance_amount);
                    payment_amount += parseFloat(response.settlement[i].rebate_amount);

                    if (response.settlement[i].settlement_type.code == "LOAN_PANALTY") {
                        penalty_balance += response.settlement[i].balance_amount;
                    }
                    ary_date[i] =
                        {
                            "date": (response.settlement[i].due_date).toString(),
                            "amount": (response.settlement[i].amount).toString(),
                            "balance": (response.settlement[i].balance_amount).toString()
                        };


                    $('#table_settlement > tbody:last-child')
                        .append('<tr>' +
                            '<td class="text-sm-left">' + response.settlement[i].due_date + '</td>' +
                            '<td class="text-sm-center">' + response.settlement[i].installment_no + '</td>' +
                            '<td class="text-sm-center">' + (response.settlement[i].settlement_type.code == "LOAN_PANALTY" ? "Loan Penalty" : response.settlement[i].description) + '</td>' +
                            '<td class="text-sm-right">' + parseFloat(response.settlement[i].amount).toFixed(2) + '</td>' +
                            '<td class="text-sm-right">' + parseFloat(response.settlement[i].balance_amount).toFixed(2) + '</td>' +
                            '<td><input value="'+response.settlement[i].rebate_amount+'"  id="settlement_' + i + '" class="form-control form-control-sm text-sm-right text-success no-border" name="settlement[' + i + '][' + response.settlement[i].index_no + '][' + response.settlement[i].balance_amount + '][' + response.settlement[i].due_date + '][' + response.settlement[i].settlement_type.code + ']" type="number" min="0"  step="any"></td>' +
                            '<input type="hidden" name="temp_bal" id="temp_id_' + i + '" value="' + (response.settlement[i].balance_amount) + '" min="0"  /> ' +
                            '</tr>');


                }

                $('#loan_balance').val(parseFloat(balance_amount).toFixed(2));
                $('#penalty_balance').val(parseFloat(penalty_balance).toFixed(2));
                $('#rebate_amount').val(parseFloat(payment_amount).toFixed(2));
                var per_ = ((parseFloat(payment_amount) / parseFloat(balance_amount) )* 100).toFixed(2);
                $('#rebate_percentage').val(per_);




                afterBalance(balance_amount,0.0);

            }
        });
    }

    function afterBalance(balance_amount,rebate_amount) {

        var pay_amount = $('#rebate_amount').val();
        var pay_per = $('#rebate_percentage').val();
        if ((pay_amount == '' || pay_amount.length == 0)) {
            pay_amount = 0.0;
        }if ((pay_per != '' || pay_per.length != 0)) {
            pay_amount = rebate_amount;
        }
        if ((balance_amount != '' || balance_amount.length !== 0)) {
            var calculate_after_bal =0.0;
            if(parseFloat(pay_amount) > parseFloat(balance_amount)){
                calculate_after_bal =0.0;
                alert("Wrong Amount!");
                $('#rebate_amount').val(0.0);
                $('#rebate_percentage').val(0.0);
            }else{
                calculate_after_bal = parseFloat(balance_amount) - parseFloat(pay_amount);
            }

            $('#after_balance').val(calculate_after_bal.toFixed(2));
        }

        advancedAmount();

    }

    function advancedAmount() {

        /*console.log(ary_date);*/

        var trance_date = moment($('#transaction_date').val(), "YYYY-MM-DD")
        var total = 0.0;
        var arease_total = 0.0;
        for (var i = 0; i < ary_date.length; i++) {

            var due_date = moment(ary_date[i].date, "YYYY-MM-DD")


            if (due_date.diff(trance_date) <= 0) {
                arease_total += parseFloat(ary_date[i].balance);
            }
        }


        $('#areas_amount').val(arease_total.toFixed(2));

        $('#areas_vol').val(
            $('#installment_amount').val() != '' || $('#installment_amount').val().length !== 0
                ? (arease_total / parseFloat($('#installment_amount').val())).toFixed(2)
                : 0);


    }
    $('#rebate_amount').change(function () {
        $('#rebate_percentage').val(0);
        rebate_amount_change();

    });
    $('#rebate_percentage').change(function () {
        $('#rebate_amount').val(0);
        rebate_percentage_change();

    });
    function rebate_amount_change() {


        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

        for (var b = 0; b < rows; b++) {

            $('#settlement_' + b).val("");
        }

        if (parseFloat($('#rebate_amount').val()) > 0) {
            var i = 0;
            var bal_amount = $('#temp_id_0').val();
            var temp = $('#rebate_amount').val();

            var settlement = 0.0;

            while (parseFloat(temp) > 0) {
                bal_amount = $('#temp_id_' + i).val();
                settlement = Math.min(bal_amount, temp);

                temp = temp - settlement;
                $('#settlement_' + i).val(settlement);
                i++;
                console.log(temp)
            }



        }
        afterBalance($('#loan_balance').val(),0.0);
    }
    function rebate_percentage_change() {


        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

        for (var b = 0; b < rows; b++) {

            $('#settlement_' + b).val("");
        }
        var rebate_amount =0.0;
        var balance_amount = parseFloat($('#loan_balance').val());

        if (parseFloat($('#rebate_percentage').val()) > 0 && parseFloat(balance_amount) > 0) {

            var per = $('#rebate_percentage').val();
             rebate_amount = (parseFloat(balance_amount) * per)/100;


            var i = 0;
            var bal_amount = $('#temp_id_0').val();
            var temp = rebate_amount;

            var settlement = 0.0;
            var overAmount = 0.0;

            while (parseFloat(temp) > 0) {
                bal_amount = $('#temp_id_' + i).val();
                settlement = Math.min(bal_amount, temp);

                temp = temp - settlement;
                $('#settlement_' + i).val(settlement);
                i++;
            }



        }
        afterBalance($('#loan_balance').val(),rebate_amount);
    }



    $('#table_settlement').on('change', 'input', function () {
        /* var tableData = $(this).closest("tr").find("td input").map(function () {
         return $(this).val();
         }).get();*/
        var total_amount = 0.0;
        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

        for (var b = 0; b < rows - 1; b++) {

            if ($('#settlement_' + b).val() != '' || $('#settlement_' + b).val().length !== 0) {
                total_amount += parseFloat($('#settlement_' + b).val());
            }
        }


        $('#rebate_amount').val(total_amount);
        afterBalance($('#loan_balance').val());

    });
    //payment/data_save
    $('#btnSave').click(function () {

            $('#save_confirm').modal('show');

    });

    $('#btnSaveConfirm').click(function () {
        $('#save_confirm').modal('hide');
        var token = (document.getElementsByName('_token'));
        var form = $('#f_payment');
        //  var form = $("#tbl2 :input").serialize();

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                if (response[0] === "save") {
                    //   alert("Save Success !");

                    window.location.replace(response[1]);
                } else {
                    alert("Save Fail ! " + response);
                }
            },
            error: function (xhr, status, error) {
                alert(error+" Contact System Administrator");

               errorDisplay(xhr.responseText);//calling common js
            }
        });
    });

    $('#cancelConfirm').click(function () {
        $('#save_confirm').modal('hide');

    });

    $('#add_to_table').click(function () {

        var bank_branch = $('#bank_branch').val();
        var account_no = $('#account_no').val();
        var cheque_no = $('#cheque_no').val();
        var cheque_date = $('#cheque_date').val();
        var amount = $('#amount').val();

        if (bank_branch == "" || bank_branch == null,
            account_no == "" || account_no == null,
            cheque_no == "" || cheque_no == null,
            amount == "" || amount == null) {

        } else {

            var rows = document.getElementById('tbl_cheque').getElementsByTagName("tr").length;

            var cheque_information = [];


            $('#tbl_cheque > tbody:last-child')
                .append('<tr>' +
                    '<td>' + account_no + '</td>' +
                    '<td>' + cheque_no + '</td>' +
                    '<td><input id="amount_' + (parseInt(rows) + 1) + '" value="' + amount + '" ' +
                    'class="form-control form-control-sm text-sm-right text-success no-border" readonly type="number" min="0"  step="any"></td>' +
                    '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                    '<i class="fas fa-trash"></i></td></tr>' +
                    '<input type="hidden" value="' + bank_branch + '" name="cheque_information[' + (parseInt(rows) - 1) + '][bank_branch]">' +
                    '<input type="hidden" value="' + account_no + '" name="cheque_information[' + (parseInt(rows) - 1) + '][account_no]">' +
                    '<input type="hidden" value="' + cheque_no + '" name="cheque_information[' + (parseInt(rows) - 1) + '][cheque_no]">' +
                    '<input type="hidden" value="' + cheque_date + '" name="cheque_information[' + (parseInt(rows) - 1) + '][cheque_date]">' +
                    '<input type="hidden" value="' + amount + '" name="cheque_information[' + (parseInt(rows) - 1) + '][amount]">');

            $('#cheque_model').modal('hide');
            addTotalAmount();
        }
    });

    $('#tbl_cheque tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();
        addTotalAmount();
    });

    function addTotalAmount() {
        var total_amount = 0.0;
        var rows = document.getElementById('tbl_cheque').getElementsByTagName("tr").length;

        for (var b = 2; b <= rows; b++) {

            if ($('#amount_' + b).val() != '' || $('#amount_' + b).val().length !== 0) {
                total_amount += parseFloat($('#amount_' + b).val());
            }
        }

        $('#rebate_amount_confirmation').val(total_amount);

    }
    //payment/data_save
    $('#btnReject').click(function () {

        $('#reject_confirm').modal('show');

    });
    $('#rejectCancelConfirm').click(function () {
        $('#reject_confirm').modal('hide');

    });
    $('#btnRejectConfirm').click(function () {
        var token = (document.getElementsByName('_token'));
        var form = $('#f_payment');
        //  var form = $("#tbl2 :input").serialize();

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                if (response[0] === "save") {
                    //   alert("Save Success !");

                    window.location.replace(response[1]);
                } else {
                    alert("Something wrong ! " + response);
                }
            },
            error: function (xhr, status, error) {
                alert(error+" Contact System Administrator");

                errorDisplay(xhr.responseText);//calling common js
            }
        });

    });

});



