$(document).ready(function(){


    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();
    // $(".rdo_report").on('click',function(event){
    //     console.log("dddd")
    //     $('#report_model').modal('show');
    // });

    $("input[name='options']").click(function(event){

        console.log(event.target)


        generateReport(event.target)
    });


    function generateReport(name){


        $('#report_id').val("")
        $('#d_from_date').hide();
        $('#d_to_date').hide();
        $('#d_agreement_no').hide();
        console.log(name.title)
        switch(name.title){

            case "customer_full_details" :

                $('#d_from_date').hide();
                $('#d_to_date').hide();
                $('#d_agreement_no').show();

                break;


        }

        $('#report_id').val(name.id)
        $('#report_model').modal('show');
    }



    $('#dataTable1').dataTable( {
        "columns": [
            {
                "data": ""
            }
        ]
    } );




    $('#agreement_no').click(function () {
        $('#temp_').val('agreement_no')
        agreementClick();
    });

    function agreementClick(){

        tableDestroy();

        var token = (document.getElementsByName('_token'));

        $.ajax({
            type: 'post',
            url: '/report_view/loan',
            data: {
                "_token": token[0].value,
            },
            success: function (response) {


                $('#dataTable1').DataTable( {
                    data: response.data,
                    columns: response.head

                });

                document.getElementById("title").innerHTML = response.title;
                $('#search_model').modal('show');

            }
        });
    }


    function tableDestroy() {

        var table = $('#dataTable1').DataTable();

        table.clear().destroy();
        $('#dataTable1').find('td,th').remove();


    }



    $("#dataTable1").on("click", "tr", function (row, $el, field) {
            var cur_row = $(this).closest('tr');

            var col1 = cur_row.find('td:eq(0)').text();


        var id = $('#temp_').val()

        $('#'+id).val(col1);


        $('#search_model').modal('hide');


    });

    $("#print_report").click(function () {
//reports/print
      //  location.href =  "_blank";
        $('#report_model').modal('hide');
    });

    // $(':checked').prop('title', function(k) {
    //     tit[k]= $(this).attr('title');
    //     console.log("sasss")
    // });



});
