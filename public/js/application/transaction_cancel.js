$(document).ready(function () {
    var result = [];
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();

    load_data();
    $('#transaction_date').change(function(){

        load_data();

    })

    function load_data(){
        var var_date = $('#transaction_date').val();
        var transaction_type = $('#transaction_type').val();
        var token = (document.getElementsByName('_token'));
        $('#cancel_table tbody').empty();
        $.ajax({
            type:'POST',
            url:'payment_cancel/'+transaction_type,
            data:{
                "_token":token[0].value,
                transaction_date:var_date

            },success :function(response) {

                if(response.transaction.length > 0)
                    for (var i = 0; i < response.transaction.length; i++) {

                        if(response.transaction[i].amounts != null)
                            $('#cancel_table > tbody:last-child')
                                .append('<tr>' +
                                    '<td class="col-md-1 p-1">' + response.transaction[i].transaction_date + '</td>' +
                                    '<td class="col-md-1 p-1">' + response.transaction[i].reference_no + '</td>' +
                                    '<td class="col-md-1 p-1">' + response.transaction[i].document_no + '</td>' +
                                    '<td class="col-md-1 p-1 text-nowrap">' + response.transaction[i].clients.code+' - '+response.transaction[i].clients.name  + '</td>' +
                                    '<td class="col-md-1 p-1">' + response.transaction[i].loans.agreement_no + '</td>' +
                                    '<td class="col-md-1 p-1 text-md-right">' + (response.transaction[i].amounts['amount']).toFixed(2) + '</td>' +
                                    '<td class="col-md-1 p-1">' +
                                    '<a type="button"  href="payment_cancel/'+transaction_type+'/edit/'+transaction_type+'/'+response.transaction[i].index_no+'/'+response.transaction[i].reference_no+'/'+response.transaction[i].loans.index_no+'/'+response.transaction[i].clients.code+'" class="btn btn-sm btn-danger -edit">' +
                                    '<i class="fa fa-edit"></i></a></td>' +
                                    '</tr>');

                    }
            }
        })
    }






});