/**
 * Created by KASUN on 9/2/2019.
 */
$(document).ready(function () {

    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
        /* startDate: '-3d'*/
    });
    $('.selectpicker').selectpicker();

    $('#payment_amount').prop('disabled', true);
    var ary_date = new Array();
    var installment_count = 0;
    $("#dataTable").on("click", "tr", function (row, $el, field) {

        var cur_row = $(this).closest('tr');
        var col2 = cur_row.find('td:eq(2)').text();
        var col1 = cur_row.find('td:eq(1)').text();
        var col5 = cur_row.find('td:eq(5)').text();

        $('#agreement_no').val(col2);
        $('#client').val(col1);
        $('#address_line').val(col5);
        dataSet(col2);
        $('#search_model').modal('hide');


    });

    $('#agreement_no').click(function () {
        $('#payment_amount').prop('disabled', false);
        clear();
    });
    $('#payment_amount').change(function () {

        payment_amount_change();

    });

    function clear() {

        $('#agreement_no').val("");
        $('#client').val("");
        $('#address_line').val("");
        $('#loan_amount').val(0.0);
        $('#installment_amount').val(0.0);
        $('#loan_balance').val(0.0);
        $('#penalty_balance').val(0.0);
        $('#advance_amount').val(0.0);
        $('#after_balance').val(0.0);
        $('#areas_amount').val(0.0);
        $('#areas_vol').val(0.0);
        $('#payment_amount').val("");
        $('#table_settlement tbody').empty();


    }

    function dataSet(val) {

        var balance_amount = 0.00;
        var penalty_balance = 0.00;
        var token = (document.getElementsByName('_token'));
        var transaction_type = $('#transaction_type').val();

        $('#table_settlement tbody').empty();
        $.ajax({
            type: 'post',
            url: 'loan_voucher/data_voucher',
            data: {
                "_token": token[0].value,
                agreement_no: val,
                transaction_type: transaction_type
            },
            success: function (response) {


                ary_date = new Array();
                var result = response.settlement.sort(function (a, b) {
                    var keyA = new Date(a.due_date),
                        keyB = new Date(b.due_date);

                    //  ((keyA < keyB) ? -1 : ((keyA > keyB) ? 1 : 0))

                    return a.installment_no - b.installment_no || parseFloat(a.settlement_type.priority) - parseFloat(b.settlement_type.priority);
                });

                // console.log(result);

                $('#loan_amount').val(response.data_list.loan_amount);
                $('#loan_index').val(response.data_list.index_no);
                $('#client_code').val(response.data_list.client);
                $('#installment_amount').val(response.data_list.installment_amount.toFixed(2));

                installment_count = response.data_list.installment_count;

                for (var i = 0; i < response.settlement.length; i++) {

                    balance_amount += response.settlement[i].balance_amount;

                    if (response.settlement[i].settlement_type.code === "LOAN_PANALTY") {
                        penalty_balance += response.settlement[i].balance_amount;
                    }
                    ary_date[i] =
                        {
                            "date": (response.settlement[i].due_date).toString(),
                            "amount": (response.settlement[i].amount).toString(),
                            "balance": (response.settlement[i].balance_amount).toString()
                        };

                    if (response.type_editble_text == true) {
                        $('#table_settlement > tbody:last-child')
                            .append('<tr>' +
                                '<td class="text-sm-left">' + response.settlement[i].due_date + '</td>' +
                                '<td class="text-sm-center">' + response.settlement[i].installment_no + '</td>' +
                                '<td class="text-sm-center">' + (response.settlement[i].settlement_type.code === "LOAN_PANALTY" ? "Loan Penalty" : response.settlement[i].description) + '</td>' +
                                '<td class="text-sm-right">' + (response.settlement[i].amount).toFixed(2) + '</td>' +
                                '<td class="text-sm-right">' + (response.settlement[i].balance_amount).toFixed(2) + '</td>' +
                                '<td><input readonly id="settlement_' + i + '" class="form-control form-control-sm text-sm-right text-success no-border" name="settlement[' + i + '][' + response.settlement[i].index_no + '][' + response.settlement[i].balance_amount + '][' + response.settlement[i].due_date + '][' + response.settlement[i].settlement_type.code + ']" type="number" min="0"  step="any"></td>' +
                                '<input type="hidden" name="temp_bal" id="temp_id_' + i + '" value="' + (response.settlement[i].balance_amount) + '" min="0"  /> ' +
                                '</tr>');
                        $('#payment_amount').prop('disabled', true);
                    } else {
                        $('#table_settlement > tbody:last-child')
                            .append('<tr>' +
                                '<td class="text-sm-left">' + response.settlement[i].due_date + '</td>' +
                                '<td class="text-sm-center">' + response.settlement[i].installment_no + '</td>' +
                                '<td class="text-sm-center">' + (response.settlement[i].settlement_type.code === "LOAN_PANALTY" ? "Loan Penalty" : response.settlement[i].description) + '</td>' +
                                '<td class="text-sm-right">' + (response.settlement[i].amount).toFixed(2) + '</td>' +
                                '<td class="text-sm-right">' + (response.settlement[i].balance_amount).toFixed(2) + '</td>' +
                                '<td><input  id="settlement_' + i + '" class="form-control form-control-sm text-sm-right text-success no-border" name="settlement[' + i + '][' + response.settlement[i].index_no + '][' + response.settlement[i].balance_amount + '][' + response.settlement[i].due_date + '][' + response.settlement[i].settlement_type.code + ']" type="number" min="0"  step="any"></td>' +
                                '<input type="hidden" name="temp_bal" id="temp_id_' + i + '" value="' + (response.settlement[i].balance_amount) + '" min="0"  /> ' +
                                '</tr>');
                        $('#payment_amount').prop('disabled', false);
                    }
                }

                $('#loan_balance').val(balance_amount.toFixed(2));
                $('#penalty_balance').val(penalty_balance.toFixed(2));


                // if (response.temporary_receipt.length != 0) {
                //
                //     $('#payment_amount').val(parseFloat(response.temporary_receipt.amount));
                //     $('#chq_payment_amount').val(parseFloat(response.temporary_receipt.amount));
                //     payment_amount_change();
                // } else {

                    afterBalance(balance_amount);
                //}
            }
        });
    }

    function afterBalance(balance_amount) {

        var pay_amount = $('#payment_amount').val();
        if ((pay_amount == '' || pay_amount.length === 0)) {
            pay_amount = 0.0;
        }
        if ((balance_amount != '' || balance_amount.length !== 0)) {

            var calculate_after_bal = parseFloat(balance_amount) - parseFloat(pay_amount);

            $('#after_balance').val(calculate_after_bal.toFixed(2));
        }

    }
    function payment_amount_change() {


        var rows = document.getElementById('table_settlement').getElementsByTagName("tr").length;

        for (var b = 0; b < rows; b++) {

            $('#settlement_' + b).val("");
        }

        if (parseFloat($('#payment_amount').val()) > 0) {
            var i = 0;
            var bal_amount = $('#temp_id_0').val();
            var temp = $('#payment_amount').val();

            var settlement = 0.0;
            var overAmount = 0.0;

            while (parseFloat(temp) > 0) {
                bal_amount = $('#temp_id_' + i).val();
                settlement = Math.min(bal_amount, temp);

                temp = temp - settlement;
                $('#settlement_' + i).val(settlement);
                i++;
            }
            if (temp > 0) {
                //OVER PAYMENT
                overAmount = temp;
            } else {
                overAmount = 0.0;
            }

            $('#overAmount').val(overAmount);
        }
        afterBalance($('#loan_balance').val());
    }
    //payment/data_save
    $('#btnSave').click(function () {



            $('#payment_amount_confirmation').val('');
            $('#span_payment_amount_confirmation').val('');
            $('#payment_model').modal('show');
            $('#payment_amount_confirmation').prop('disabled',false);

    });

    $('#payment_cash').change(function(){

       // alert($('#payment_cash').val());

        paymentAmountMatch();
    });
    $('#payment_account').change(function(){

        paymentAmountMatch();
    });

    $('#add_to_table').click(function () {


        var account_no = $('#account_no').val();
        var cheque_no = $('#cheque_no').val();
        var cheque_date = $('#cheque_date').val();
        var amount = $('#amount').val();

        if (
            account_no == "" || account_no == null,
            cheque_no == "" || cheque_no == null,
            amount == "" || amount == null) {

        } else {

            var rows = document.getElementById('tbl_cheque').getElementsByTagName("tr").length;

            var cheque_information = [];


            $('#tbl_cheque > tbody:last-child')
                .append('<tr>' +
                    '<td>' + account_no + '</td>' +
                    '<td>' + cheque_no + '</td>' +
                    '<td><input id="amount_' + (parseInt(rows) + 1) + '" value="' + amount + '" ' +
                    'class="form-control form-control-sm text-sm-right text-success no-border" readonly type="number" min="0"  step="any"></td>' +
                    '<td> <a type="button"  class="btn btn-danger btn-circle btn-sm text-gray-100">' +
                    '<i class="fas fa-trash"></i></td></tr>' +

                    '<input type="hidden" value="' + account_no + '" name="cheque_information[' + (parseInt(rows) - 1) + '][account_no]">' +
                    '<input type="hidden" value="' + cheque_no + '" name="cheque_information[' + (parseInt(rows) - 1) + '][cheque_no]">' +
                    '<input type="hidden" value="' + cheque_date + '" name="cheque_information[' + (parseInt(rows) - 1) + '][cheque_date]">' +
                    '<input type="hidden" value="' + amount + '" name="cheque_information[' + (parseInt(rows) - 1) + '][amount]">');

            $('#cheque_model').modal('hide');
            paymentAmountMatch();
        }
    });



    function paymentAmountMatch(){

        var cash = $('#payment_cash').val();
        var bank = $('#payment_account').val();
        var cheque = addTotalAmount();
        if(cash==""){
            cash =0 ;
        }if(bank==""){
            bank=0;
        }if(cheque==""){
            cheque=0;
        }

        console.log(cash);
        console.log(bank);
        console.log(cheque);


        var total = parseFloat(cash)+parseFloat(bank)+parseFloat(cheque);

        $('#payment_amount_confirmation').val(total);



    }
    function addTotalAmount() {
        var total_amount = 0.0;
        var rows = document.getElementById('tbl_cheque').getElementsByTagName("tr").length;

        for (var b = 2; b <= rows; b++) {

            if ($('#amount_' + b).val() != '' || $('#amount_' + b).val().length !== 0) {
                total_amount += parseFloat($('#amount_' + b).val());
            }
        }

       return (total_amount);

    }

    $("#print_payment").click(function () {

        location.href = "/" + $('#new_page').val();

    });
    $("#cancel_print").click(function () {

        location.href = "/" + $('#new_page').val();

    });

    $('#tbl_cheque tbody').on('click', '.btn', function () {
        $(this).closest('tr').remove();
        paymentAmountMatch();
    });

    $('#accept_payment').click(function () {

        var token = (document.getElementsByName('_token'));
        var form = $('#f_payment');
        //  var form = $("#tbl2 :input").serialize();
        if(validation()){
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    if (response[0] === "save") {
                        //   alert("Save Success !");
                        $('#payment_model').modal('hide');
                        $('#print_option').modal('show');

                        $("#print_payment").attr("href", "payments/reports/print/" + response[2] + "/" + response[3]);
                        $('#new_page').val(response[1]);

                        //location.href = "/" + response[1];

                    } else {
                        alert("Save Fail ! " + response);
                    }
                },
                error: function (xhr, status, error) {
                    alert(error+" Contact System Administrator");

                    errorDisplay(xhr.responseText);//calling common js
                }
            });

        }else{

        }
    });
    function validation(){
        var cm_total = $('#payment_amount_confirmation').val();
        var type_total = $('#payment_amount').val();

        if(parseFloat(cm_total) != parseFloat(type_total)){  alert("Wrong amount try again !" ); return false}

        return true;

    }




})