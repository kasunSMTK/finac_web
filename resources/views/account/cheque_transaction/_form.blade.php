{{ csrf_field() }}
<div class="row mb-0">
    <div class="col-sm-12">
        <div class="card shadow mb-4">
            <div class="table-responsive account-check-custom-scrollbar">
                <table name="table_top" id="table_top" class="table-sm table-bordered table-hover "
                       width="100%"
                       cellspacing="0">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th class="text-md-center">Account No</th>
                        <th>Cheque No</th>
                        <th class="text-md-right">Amount</th>
                        <th>Cheque date</th>
                        <th>Client</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <span id="span_table_data" class="alert-danger"> {{ $errors->first('table_data') }}</span>
        <div class="card shadow mb-4">
            <div class="table-responsive account-check-custom-scrollbar">
                <table name="table_down" id="table_down" class="table-sm table-bordered table-hover"
                       width="100%"
                       cellspacing="0">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th class="text-md-center">Account No</th>
                        <th>Cheque No</th>
                        <th class="text-md-right">Amount</th>
                        <th>Cheque date</th>
                        <th>Client</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="float-right">

            <button type="button" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i> Save
            </button>
            <button type="button" class="btn  btn-primary"><i class="fa fa-refresh"></i>Reset</button>
        </div>

    </div>
</div>
@include('commonForm.ChequeInformation')
