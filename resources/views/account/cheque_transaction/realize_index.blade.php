@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">Cheque Realized</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::open( ['url' => ['cheque_realize/save_data'] ,'id'=>'f_cheque']) !!}
                <input type="hidden" id="transaction_type" value="{{\App\Util\ApplicationVarible::$CHEQUE_REALIZE_TRANSACTION_CODE}}"/>
                @include('account.cheque_transaction._form')
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="/js/application/searchModel.js" type="text/javascript"></script>
    <script src="/js/application/cheque_transaction.js" type="text/javascript"></script>

@stop

