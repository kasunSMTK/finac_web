{{ csrf_field() }}
<div class="modal" id="cheque_model">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cheque Details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('bank_branch','Bank Branch',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::select('bank_branch',$cheque, null,['class'=>'form-control form-control-sm ','data-live-search'=>'true','id'=>'bank_branch']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('account_no','Account No Ex: 234-100-xxx',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('account_no','',['class'=>'form-control form-control-sm ','id'=>'account_no']) !!}
                        </div>
                        <span id="span_account_no" class="alert-danger"> {{ $errors->first('account_no') }}</span>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('cheque_no','Cheque No Ex: 15241xx',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('cheque_no','',['class'=>'form-control form-control-sm ','id'=>'cheque_no']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('cheque_date','Cheque Date',['class'=>'label-info mb-sm-0']) !!}
                            <div class="input-group date" data-provide="datepicker">
                                {!! Form::text('cheque_date', '',['class'=>'form-control form-control-sm','id'=>'cheque_date']) !!}
                                <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                            <span id="span_cheque_date" class="alert-danger expected_loan_date"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('amount','Amount',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::number('amount','',['class'=>'form-control form-control-sm text-primary text-lg-right border border-success','step'=>'any','min'=>'0','id'=>'amount']) !!}
                        </div>
                    </div>

                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="add_to_table" class="btn btn-primary">Add
                </button> {{--data-dismiss="modal"--}}
            </div>

        </div>
    </div>

</div>


