{{ csrf_field() }}
<div class="modal" id="cheque_information_model">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cheque Account</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('bank_branch','Bank Branch',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::select('bank_branch',$cheque, null,['class'=>'form-control form-control-sm ','data-live-search'=>'true','id'=>'bank_branch']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('reference_no','Reference No',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('reference_no',$referenceNo,['class'=>'form-control form-control-sm ','readonly']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('document_no','Document No',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('document_no','',['class'=>'form-control form-control-sm ']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('transaction_date','Transaction Date',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('transaction_date',Session::get('working_date'),['class'=>'form-control form-control-sm ','readonly']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('note','Note',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('note','',['class'=>'form-control form-control-sm text-primary ']) !!}
                        </div>
                    </div>

                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="add_" class="btn btn-primary">Add
                </button> {{--data-dismiss="modal"--}}
            </div>

        </div>
    </div>

</div>


