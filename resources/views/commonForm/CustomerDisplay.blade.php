<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Customer Panel</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('guarantor','Customer',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::select('guarantor',$clients, $loan->client,['class'=>'form-control form-control-sm ','data-live-search'=>'true']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="addGuarantor" class="btn btn-success" >Add</button> {{--data-dismiss="modal"--}}
            </div>

        </div>
    </div>
</div>