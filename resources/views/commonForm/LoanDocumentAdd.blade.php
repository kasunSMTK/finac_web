<div class="modal" id="myModal2">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Document Select</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('description','Name',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('description','',['class'=>'form-control form-control-sm ','placeholder'=>'Ex: Nic']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('reference_link','Reference Link',['class'=>'label-info mb-sm-0']) !!}
                            <div class="custom-file">
                                <input type="file" name="select_file" id="select_file" class="custom-file-input" >
                                <label name="reference_link" class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            {{--{!! Form::text('reference_link','',['class'=>'form-control form-control-sm ']) !!}--}}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="addDocument" class="btn btn-success upload-image" >Add</button> {{--data-dismiss="modal"--}}
            </div>

        </div>
    </div>
</div>