{{ csrf_field() }}
<div class="modal" id="payment_model">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Payment Option</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    @if(isset($account))
                        <div class="col-sm-12 pb-0">
                            <div class="form-group mb-sm-1">
                                {!! Form::label('account','Bank Account',['class'=>'label-info mb-sm-0']) !!}
                                {!! Form::select('account',$account, null,['class'=>'form-control form-control-sm ','data-live-search'=>'true']) !!}
                            </div>
                        </div>
                    @endif
                    @if(isset($cheque))
                        <div class="col-sm-12 pb-3">
                            <div class="float-right">

                                <a id="btnAdd" class="btn btn-sm btn-info text-gray-100" data-target="#cheque_model" data-toggle="modal">
                                    <i class="fa fa-plus"></i> Add </a>
                            </div>
                            </div>
                            <div class="col-sm-12 pb-2">
                            <div class="table-responsive">
                                <table id="tbl_cheque" class="table-sm table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Account</th>
                                        <th>Cheque</th>
                                        <th>Amount</th>
                                        <th>AC</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                {{--  <div>{{$Clients->links()}}</div>--}}
                            </div>
                        </div>
                         @include('commonForm.ChequeDetails')
                    @endif
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('payment_amount_check','Re Enter Payment Amount',['class'=>'label-info mb-sm-0']) !!}
                            <span class="icon-case text-success"><i class="fa fa-coins"></i></span>
                            {!! Form::number('payment_amount_confirmation','',['class'=>'form-control form-control-sm text-primary text-lg-right border border-success','step'=>'any','min'=>'0','id'=>'payment_amount_confirmation',isset($cheque)?'readonly':'']) !!}
                        </div>
                        <span id="span_payment_amount_confirmation"
                              class="alert-danger"> {{ $errors->first('payment_amount_check') }}</span>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="accept_payment" class="btn btn-primary">Accept
                </button> {{--data-dismiss="modal"--}}
            </div>

        </div>
    </div>

</div>
@include('commonForm.PrintOption')

