{{ csrf_field() }}
<div class="modal" id="reject_confirm">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-body">
             <p>Confirmation</p>
                <p>Do you want to Reject</p>
            </div>

            <input type="hidden" id="new_page"/>
            <!-- Modal footer -->
            <div class="modal-footer">
                <a  id="btnRejectConfirm" class="btn  btn-success text-gray-100" target="_blank" >Reject</a>
                <button type="button" id="rejectCancelConfirm"  class="btn btn-danger">Cancel
                </button>
            </div>

        </div>
    </div>

</div>