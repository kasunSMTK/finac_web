{{ csrf_field() }}
<div class="modal" id="report_model">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{$name}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

        {!! Form::open( ['url' => ['reports/print'] ,'id'=>'f_payment', 'target'=>'_blank']) !!}

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">


                    <div id="d_from_date" class="col-sm-12 pb-0">
                            <div class="form-group mb-sm-1">
                                {!! Form::label('from_date','From Date',['class'=>'label-info mb-sm-0']) !!}
                                <div class="input-group date" data-provide="datepicker">
                                    {!! Form::text('from_date', null,['class'=>'form-control form-control-sm','id'=>'from_date']) !!}
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div id="d_to_date" class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('to_date','To Date',['class'=>'label-info mb-sm-0']) !!}
                            <div class="input-group date" data-provide="datepicker">
                                {!! Form::text('to_date', null,['class'=>'form-control form-control-sm','id'=>'to_date']) !!}
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="d_agreement_no"  class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('agreement_no','Agreement No',['class'=>'label-info mb-sm-0']) !!}
                            {!! Form::text('agreement_no','',['class'=>'form-control form-control-sm ','id'=>'agreement_no']) !!}
                        </div>

                    </div>
                </div>
            </div>
            <input id="temp_" name="temp_" type="hidden"/>
            <input id="report_id" name="report_id" type="hidden"/>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" id="print_report" class="btn  btn-success "><i class="fa fa-file-pdf"></i> View
                </button>
                {{--<a  id="print_payment" class="btn  btn-success text-gray-100" target="_blank" ><i class="fa fa-file-pdf text-gray-100"></i> View</a>--}}
                {{--<button type="button" class="btn  btn-primary"><i class="fa fa-refresh"></i>Print</button>--}}
            </div>
            {!! Form::close() !!}
        </div>

    </div>

</div>


