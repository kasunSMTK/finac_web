{{ csrf_field() }}
<div class="modal" id="com_search_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{$transaction_type}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {{-- <div class="row">
                     <div class="col-sm-12 pb-0">--}}
                <div class="table-responsive">
                    <table class="table-sm dataTable1 table-bordered table-condensed table-striped table-hover" id="dataTable1"
                           width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($head as $column)
                            <th>{{$column}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $raw)
                                <tr>

                                    @foreach($head as $key=>$column)
                                    <td>{{$raw[$key]}}</td>
                                    @endforeach
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <input type="hidden" id="hid_id" name="hid_id" />


                {{--   </div>
               </div>--}}
            </div>

        </div>
    </div>

</div>

