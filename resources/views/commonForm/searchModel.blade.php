{{ csrf_field() }}
<div class="modal" id="search_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{$transaction_type}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {{-- <div class="row">
                     <div class="col-sm-12 pb-0">--}}
                <div class="table-responsive">
                    <table class="table-sm  table-bordered table-condensed table-striped table-hover" id="dataTable"
                           width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>code</th>
                            <th>Name</th>
                            <th>Agreement</th>
                            <th>Doc No</th>
                            <th>Tp</th>
                            <th>Address</th>
                            <th data-visible="false">Loan Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($transaction_type=='CHEQUE_RECEIPT')

                            @foreach($loan as $agreement)
                                <tr>
                                    <td>{{$agreement->loans->clients->code}}</td>
                                    <td>{{$agreement->loans->clients->name}}</td>
                                    <td>{{$agreement->loans->agreement_no}}</td>
                                    <td>{{$agreement->loan_document_no}}</td>
                                    <td>{{$agreement->loans->clients->mobile.'  '.$agreement->loans->clients->telephone1}}</td>
                                    <td>{{$agreement->loans->clients->address_line1.
                                    ', '.$agreement->loans->clients->address_line2.
                                    ', '.$agreement->loans->clients->address_line3}}</td>
                                    <td data-field="hd_loan_name">{{$agreement->loans->clients->long_name}}</td>
                                </tr>
                            @endforeach

                        @else
                            @foreach($loan as $agreement)
                                <tr>
                                    <td>{{$agreement->clients->code}}</td>
                                    <td>{{$agreement->clients->name}}</td>
                                    <td>{{$agreement->agreement_no}}</td>
                                    <td>{{$agreement->loan_document_no}}</td>
                                    <td>{{$agreement->clients->mobile.'  '.$agreement->clients->telephone1}}</td>
                                    <td>{{$agreement->clients->address_line1.
                                    ', '.$agreement->clients->address_line2.
                                    ', '.$agreement->clients->address_line3}}</td>
                                    <td data-field="hd_loan_name">{{$agreement->clients->long_name}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>


                {{--   </div>
               </div>--}}
            </div>

        </div>
    </div>

</div>

