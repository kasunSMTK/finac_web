{{ csrf_field() }}
<div class="modal" id="search_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 id="title" class="modal-title">{{$title}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {{-- <div class="row">
                     <div class="col-sm-12 pb-0">--}}
                <div class="table-responsive">
                    <table class="table-sm dataTable1 table-bordered table-condensed table-striped table-hover" id="dataTable1"
                           width="100%" cellspacing="0">
                        <thead>

                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>


                {{--   </div>
               </div>--}}
            </div>

        </div>
    </div>

</div>
