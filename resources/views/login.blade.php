<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Finac V9 Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">


</head>

<body class="bg-gray-100">

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">


            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    
                    {!! Form::open(['url' => 'main_page/login', 'method' => 'POST']) !!}
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block ">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="image/5.jpg" alt="1 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/1.jpg" alt="2 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/2.jpg" alt="3 slide">
                                    </div>
                                    <div class="carousel-item ">
                                        <img class="d-block w-100" src="image/4.jpg" alt="4 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/3.jpg" alt="5 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/6.jpg" alt="6 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/7.jpg" alt="7 slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="image/8.jpg" alt="8 slide">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                        </div>
                        <div class="col-lg-6 ">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    {!! Form::text('email','',['class'=>'form-control form-control-user','placeholder'=>'Enter User Name']) !!}
                                </div>
                                @if($errors->has('email'))
                                    <span class="alert-danger"> {{ $errors->first('email') }}</span>
                                @endif
                                <div class="form-group">
                                    {!! Form::password('password',['class'=>'form-control form-control-user','placeholder'=>'Password']) !!}
                                </div>
                                @if($errors->has('password'))
                                    <span class="alert-danger"> {{ $errors->first('password') }}</span>
                                @endif
                                <div class="form-group">
                                    {!! Form::select('branch',$branches,null,['class'=>'form-control form-control-user']) !!}
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                        <input type="checkbox" class="custom-control-input" id="customCheck">
                                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                                    </div>
                                </div>

                                <span class="alert-warning"> {{ $errors->first('password_fail') }}</span>

                                <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>


                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('/js/sb-admin-2.min.js')}}"></script>
<script src="{{asset('/js/application/login.js')}}"></script>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);

    }
</script>

</body>

</html>
