<!-- Sidebar -->
{{csrf_field()}}
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">

        </div>
        <div class="sidebar-brand-text mx-3">FINAC V6.0</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Loan Transaction
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
           aria-controls="collapseTwo">
            <i class="fas fa-fw fa-folder"></i>
            <span>Registration</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Registration Components</h6>
                <div class="collapse-divider registration_client">
                    <h6 class="collapse-header">Client Group</h6>

                    {{--<a class="collapse-item" href="{{url('clients')}}">Client</a>
                    <a class="collapse-item" href="#">Client Approve</a>
                    <a class="collapse-item" href="#">Client Information</a>--}}
                </div>
                <div class="collapse-divider">
                    <h6 class="collapse-header">Loan Group</h6>
                    <a class="collapse-item" href="#">Loan Type</a>
                    <a class="collapse-item" href="#">Charge Schema</a>
                </div>
                <a class="collapse-item" href="#">Employee</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Transaction Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-folder"></i>
            <span>Transaction</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">

                <h6 class="collapse-header">Loan Group</h6>
                <div class="collapse-divider transaction_loan">

                </div>
                <h6 class="collapse-header">Loan Charges</h6>
                <div class="collapse-divider transaction_charge">



                </div>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_approve"
           aria-expanded="true" aria-controls="collapse_approve">
            <i class="fas fa-fw fa-file"></i>
            <span>Approval</span>
        </a>
        <div id="collapse_approve" class="collapse" aria-labelledby="headingApprove" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider approval">

                </div>

            </div>
        </div>
    </li>
    <div class="sidebar-heading">
        Payments
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_payment"
           aria-expanded="true" aria-controls="collapse_payment">
            <i class="fas fa-fw fa-coins"></i>
            <span>Payment</span>
        </a>
        <div id="collapse_payment" class="collapse" aria-labelledby="headingPayment" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider transaction_payments">

                </div>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_voucher"
           aria-expanded="true" aria-controls="collapse_voucher">
            <i class="fas fa-fw fa-address-book"></i>
            <span>Loan Voucher</span>
        </a>
        <div id="collapse_voucher" class="collapse" aria-labelledby="headingPayment" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider loan_voucher">

                </div>

            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Account Transaction
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_cheque" aria-expanded="true"
           aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Cheque Transaction</span>
        </a>
        <div id="collapse_cheque" class="collapse" aria-labelledby="headingCheque" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider cheque_transaction  ">
                </div>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
           aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>General Transaction</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">


            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_report"
           aria-expanded="true" aria-controls="collapse_voucher">
            <i class="fas fa-fw fa-book-open"></i>
            <span>Report</span>
        </a>
        <div id="collapse_report" class="collapse" aria-labelledby="headingPayment" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <div class="collapse-divider reports">

                </div>

            </div>
        </div>
    </li>


    <!-- Nav Item - Charts -->


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    <li class="nav-item">
        <h7 class="collapse-header text-gray-300 text-sm-center">Working Date :</h7>
        <h8 class="collapse-header text-gray-300 text-sm-center">{{ Session::has('working_date') ?  Session::get('working_date')  : '' }}</h8>
    </li>


</ul>


<!-- End of Sidebar -->
