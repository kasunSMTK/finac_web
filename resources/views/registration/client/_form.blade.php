{{ csrf_field() }}
<div class="row">
    <div class="col-sm-6 ">
        <div class="form-group  mb-sm-0">
            {!! Form::label('code','Code ',['class'=>'label-info mb-sm-0']) !!}
            {{--<input type="text" class="form-control form-control-sm" name="code" placeholder="Code" >--}}
            @if($button_caption=='Update')
                {!! Form::text('code', $client->code,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
            @else
                {!! Form::text('code', $client->code,['class'=>'form-control form-control-sm']) !!}
            @endif
            @if($errors->has('code'))
                <span class="alert-danger"> {{ $errors->first('code') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('nic_no','NIC',['class'=>'label-info mb-sm-0']) !!}
            {{--<input type="text" class="form-control form-control-sm" name="nic_no" placeholder="Nic">--}}
            {!! Form::text('nic_no', $client->nic_no,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('name','Name With Initials',['class'=>'label-info mb-sm-0']) !!}
        {{--<input type="text" class="form-control form-control-sm" name="name" placeholder="Name With Initials">--}}
        {!! Form::text('name', $client->name,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('name'))
            <span class="alert-danger"> {{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('long_name','Full Name',['class'=>'label-info mb-sm-0']) !!}
        {{--  <input type="text" class="form-control form-control-sm" name="fullname" placeholder="Full Name">--}}
        {!! Form::text('long_name', $client->long_name,['class'=>'form-control form-control-sm']) !!}
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('address_line1','Address Line 1',['class'=>'label-info mb-sm-0']) !!}
            {{--<input type="text" class="form-control form-control-sm" name="address1" placeholder="Address1">--}}
            {!! Form::text('address_line1', $client->address_line1,['class'=>'form-control form-control-sm',]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('address_line2','Address Line 2',['class'=>'label-info mb-sm-0']) !!}
            {{--  <input type="text" class="form-control form-control-sm" name="address2" placeholder="Address2">--}}
            {!! Form::text('address_line2', $client->address_line2,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('address_line3','Address Line 3',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('address_line3', $client->address_line3,['class'=>'form-control form-control-sm']) !!}
    </div>
</div>
<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('route','Route',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('route', $route,$client->route,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        {{--'selected'=>( $route == $client->route ) ? $client->route  :'' ]) !!}--}}
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('mobile','Mobile',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('mobile', $client->mobile,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('telephone1','Telephone Home',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('telephone1', $client->telephone1,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('telephone2','Telephone Office',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('telephone2', $client->telephone2,['class'=>'form-control form-control-sm']) !!}
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('fax','Fax',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('fax', $client->fax,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('email','Email',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('email', $client->email,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('note','Note',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::text('note', $client->note,['class'=>'form-control form-control-sm']) !!}
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group">
        {!! Form::label('maximum_allocation','Maximum Allocation',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::number('maximum_allocation',isset($client->maximum_allocation)?$client->maximum_allocation:0,['class'=>'form-control form-control-sm']) !!}
        @if($errors->has('maximum_allocation'))
            <span class="alert-danger"> {{ $errors->first('maximum_allocation') }}</span>
        @endif
    </div>
</div>


<div class="float-right mb-2">

    <button type="submit"  class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-info"><i class="fa fa-refresh"></i>Reset</button>

</div>


