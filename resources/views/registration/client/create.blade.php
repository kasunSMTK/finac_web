 @extends('layouts.main')
@section('content')
    {{-- <div class="container">--}}
    <div class="container-fluid">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($client, ['url' => ['clients/save'],'enctype'=>"multipart/form-data"]) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-3">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create a Customer</h1>
                            </div>
                            {{-- <form class="user" action="{{url('clients')}}" method="POST">--}}

                            @include('registration.client._form',['button_caption'=>'Save'])


                            {{-- </form>--}}

                            {{--<div class="alert-danger">--}}
                            {{--@include('partials.alert')--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="p-5">
                            <a id="hlink" target="_blank" href="" data-toggle="lightbox">

                                <img id="image" src="{{ URL("user/profile_picture.png")}}"
                                     class="img-fluid rounded-circle img-thumbnail"
                                     style="width:200px;height:200px">
                            </a>
                            <input name="p_image" class="filestyle " type="file">


                        </div>
                        @if($errors->has('p_image'))
                            <span class="alert-danger"> {{ $errors->first('p_image') }}</span>
                        @endif

                    </div>


                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop
@section('application_js')
    <script src="{{asset('/js/application/client.js')}}" type="text/javascript"></script>

@stop

