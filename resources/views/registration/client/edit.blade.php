@extends('layouts.main')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-1">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                {!! Form::model($client,['url'=>'clients/edit/'.$client->code,'enctype'=>"multipart/form-data", 'method' => 'put']) !!}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit Customer</h1>
                            </div>


                            @include('registration.client._form',['button_caption'=>'Update'])



                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="p-5">
                            <a id="hlink" target="_blank" href="{{
                                URL("user/".isset($client->code)?
                                "user/image/".$client->code.".png": "")}}" data-toggle="lightbox">


                                <img id="image" src="{{
                                URL("user/".isset($client->code)?
                                "user/image/".$client->code.".png": URL("user/profile_picture.png"))}}"
                                     class="img-fluid rounded-circle img-thumbnail"
                                     style="width:200px;height:200px">
                            </a>
                            <input name="p_image" class="filestyle " type="file">


                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="{{asset('/js/application/client.js')}}" type="text/javascript"></script>

@stop

