@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        {{--editable Page--}}
{{--
        <div class="card-body">

        </div>--}}

        <!-- Page Heading -->
        <h6 class="h4 mb-2 text-gray-800">Client Manager</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">

                    <a id="btnAddNew" class="btn btn-sm btn-info add-new" href="{{url('clients/create')}}">
                        <i class="fa fa-plus"></i> Add New</a>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>NIC</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Route</th>
                            <th>Telephone</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Code</th>
                            <th>NIC</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Route</th>
                            <th>Telephone</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($Clients as $cllist)
                            <tr>
                                <td class="col-md-1">{{$cllist->code}}</td>
                                <td class="col-md-1">{{$cllist->nic_no}}</td>
                                <td class="col-md-2">{{$cllist->name}}</td>
                                <td class="col-md-3">{{$cllist->address_line1.' '.$cllist->address_line2.' '.$cllist->address_line3}}</td>
                                <td class="col-md-2">{{$cllist->router->name}}</td>
                                <td class="col-md-1">{{$cllist->mobile}}</td>
                                <td class="col-md-1">
                                    <a type="button" href="{{route('clients.edit',$cllist->code)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
