@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">{{$name}}</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {{--{!! Form::open( ['url' => [$type.'/store'] ,'id'=>'report']) !!}--}}
                <div class="row mb-0">

                    <div class="col-sm-1 col-sm-12">

                            <div class="table-responsive receipt-custom-scrollbar">
                                    <div class="row">

                                    @foreach($reports as $list)
                                        {{--<div class="col-sm-4 p-2 btn-group radio-group-toggle" data-toggle="buttons">--}}
                                            {{--<label class="btn bg-gray-600 text-gray-100 active">--}}
                                                {{--<div class="form-check float-left">--}}
                                                {{--<input class="form-check-input rdo_report" type="radio" name="options" title="{{$list->module_path}}" id="{{$list->module_id}}" autocomplete="off" > {{$list->module_name}}--}}
                                                {{--</div>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                                <div class="custom-radios col-sm-3 p-1 ml-1 btn-group radio-group-toggle" >
                                                <div>
                                                    <input class="ss" type="radio" title="{{$list->module_path}}" id="{{$list->module_id}}" name="options" value="{{$list->module_id}}" >
                                                    <label class="btn text-gray-600 active" for="{{$list->module_id}}">
                                                      <span class="ss" >
                                                      </span>
                                                        {{$list->module_name}}
                                                    </label>

                                                </div>
                                                </div>
                                    @endforeach
                                        </div>

                        </div>

                        @include('commonForm.ReportParameter')
                        @include('commonForm.searchModelReport')
                    </div>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/report.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>


@stop

