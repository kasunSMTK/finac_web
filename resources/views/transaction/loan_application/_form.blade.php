{{ csrf_field() }}

<div class="row pb-0">
    <div class="col-sm-6 pb-0 border border-bottom-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('application_reference_no','Reference No ',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('application_reference_no', $button_caption=='Save'?$referenceNo:$loan->application_reference_no,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
        </div>
    </div>
    <div class="col-sm-6 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('application_document_no','Document NO',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('application_document_no', $loan->application_document_no,['class'=>'form-control form-control-sm']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 border border-top-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('application_transaction_date','Transaction Date',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('application_transaction_date',Session::has('working_date')? Session::get('working_date') :'',['class'=>'form-control form-control-sm','readonly'=>'true','id'=>'transaction_date']) !!}
        </div>

            <span id="span_application_transaction_date" class="alert-danger"> {{ $errors->first('application_transaction_date') }}</span>

    </div>
    <div class="col-sm-6 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('expected_loan_date','Expected Loan Date',['class'=>'label-info mb-sm-0']) !!}
            <div class="input-group date" data-provide="datepicker">
                {!! Form::text('expected_loan_date', $loan->expected_loan_date,['class'=>'form-control form-control-sm','id'=>'expected_loan_date']) !!}
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
        </div>
                <span id="span_expected_loan_date" class="alert-danger expected_loan_date"></span>
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('client','Customer',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('client',$clients, $loan->client,['class'=>'form-control form-control-sm ','data-live-search'=>'true']) !!}
    </div>

        <span id="span_client" class="alert-danger"> </span>

</div>
<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('loan_type','Loan Type',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::select('loan_type',$loanTypes, $loan->loan_type,['class'=>'form-control form-control-sm','data-live-search'=>'true','id'=>'loan_type']) !!}
    </div>
    <input type="hidden" id="interest_method"/>

        <span id="span_loan_type" class="alert-danger"> </span>

</div>

<div class="row">
    <div class="col-sm-6 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('loan_group','Loan Group',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::select('loan_group',$loanGroups , $loan->loan_group,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        </div>
    </div>
    <div class="col-sm-6 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('supplier','Supplier',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::select('supplier',$suppliers, $loan->supplier,['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1 ">
            {!! Form::label('loan_amount','Loan Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('loan_amount',$loan->loan_amount,['class'=>'form-control form-control-sm text-md-right loan_amount','step'=>'any']) !!}
        </div>
        {{--@if($errors->has('loan_amount'))
            <span class="alert-danger"> {{ $errors->first('loan_amount') }}</span>
        @endif--}}
        <span id="span_loan_amount" class="alert-danger"> </span>

    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('payment_term','Payment Term',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('payment_term', $loan->payment_term,['class'=>'form-control form-control-sm','readonly'=>'true']) !!}
        </div>

            <span id="span_payment_term" class="alert-danger"></span>

    </div>
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('interest_rate','Interest Rate',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('interest_rate', $loan->interest_rate,['class'=>'form-control form-control-sm text-md-right','step'=>'any']) !!}
        </div>
            <span id="span_interest_rate" class="alert-danger"> </span>
    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('installment_count','Installment Count',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('installment_count', $loan->installment_count,['class'=>'form-control form-control-sm text-md-right']) !!}
        </div>
            <span id="span_installment_count" class="alert-danger"> </span>

    </div>
</div>
<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('installment_amount','Installment Amount',['class'=>'label-info mb-sm-0']) !!}
        {!! Form::number('installment_amount', $loan->installment_amount,['class'=>'form-control form-control-sm text-md-right','step'=>'any']) !!}
    </div>

        <span id ="span_installment_amount" class="alert-danger"> </span>

</div>
<div class="row border">
    <div class="col-sm-5">
        <div class="form-group mb-sm-1 form-inline">
            {!! Form::checkbox('chk_Penalty',$loan->panalty,($loan->panalty)==1?true:false,['id'=>'chk_Penalty']) !!}
            {!! Form::label('panalty','Penalty',['class'=>'label-info mb-sm-0']) !!}
            <div class="col-sm-5">
                {!! Form::select('panalty_type',['NONE'=>'NONE','RATE'=>'RATE','AMOUNT'=>'AMOUNT'],$loan->panalty_type,['class'=>'form-control form-control-sm','data-live-search'=>'true','id'=>'panalty_type']) !!}
            </div>
        </div>
    </div>

    <div class="col-sm-7">
        <div class="form-group mb-sm-1 form-check-inline">
            {{--{!! Form::radio('is_panalty','Rate',false,['class'=>'label-info mb-sm-0','id'=>'is_panalty']) !!}--}}
            {!! Form::label('is_panalty','Rate',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('panalty_rate', $loan->panalty_rate,['class'=>'form-control form-control-sm text-md-right','id'=>'panalty_rate','step'=>'any']) !!}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group mb-sm-1 form-inline float-right">
            {{--{!! Form::radio('is_panalty','Amount',false,['class'=>'label-info mb-sm-0']) !!}--}}
            {!! Form::label('is_panalty','Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('panalty_amount', $loan->panalty_amount,['class'=>'form-control form-control-sm text-md-right','id'=>'panalty_amount','step'=>'any']) !!}
        </div>
    </div>

</div>
<div class="row pt-1">
    <div class="col-sm-4 pb-0 ">
        <div class="form-group mb-sm-1">
            {!! Form::label('grace_days','Grace Days',['class'=>'label-info mb-sm-0 ']) !!}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group mb-sm-1">
            {!! Form::number('grace_days', $loan->grace_days,['class'=>'form-control form-control-sm text-md-right']) !!}
        </div>
    </div>
</div>
<div class="row pt-1">
    <div class="col-sm-4 pb-0 ">
        <div class="form-group mb-sm-1">
            {!! Form::label('note','Other Reference',['class'=>'label-info mb-sm-0']) !!}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group mb-sm-1">
            {!! Form::text('note', $loan->note,['class'=>'form-control form-control-sm','id'=>'note']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 pb-1 ">
        <div class="form-group mb-sm-1">
            {!! Form::label('reason','Reasons',['class'=>'label-info mb-sm-0']) !!}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group mb-sm-1">
            {!! Form::textarea('reason', $loan->reason,['class'=>'form-control form-control-sm rounded-0 ']) !!}
        </div>

    </div>
</div>







