@extends('layouts.main')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::model($loan, ['url' => ['loan_application/store'],'id'=>'loanApp']) !!}
                <div class="row mb-0">
                    <div class="col-sm-6">

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_1" role="tab"
                                   aria-selected="true">Loan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_2" role="tab">Charges</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link tab_3" data-toggle="tab" href="#tab_3" role="tab">Schedule</a>
                            </li>
                        </ul>

                        <!-- Tab panes {Fade}  -->
                        <div class="tab-content">
                            <div class="tab-pane fade show  active" id="tab_1" name="tab_1" role="tabpanel">
                                @include('transaction.loan_application.tab_Pane.tab_1')
                            </div>
                            <div class="tab-pane fade" id="tab_2" name="tab_2" role="tabpanel">
                                @include('transaction.loan_application.tab_Pane.tab_2')
                            </div>
                            <div class="tab-pane fade" id="tab_3" name="tab_3" role="tabpanel">
                                @include('transaction.loan_application.tab_Pane.tab_3')
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-6">

                        <div class="card shadow mb-4">

                            @include('transaction.loan_application.other_form.f_rightTop')

                        </div>

                        <div class="card shadow mb-4">

                            @include('transaction.loan_application.other_form.f_rightDown')

                        </div>

                        <div class="card  mb-4">

                            @include('transaction.loan_application.other_form.f_buttonDown',['button_caption'=>'Save'])

                        </div>


                    </div>
                    <input type="hidden" id="invisible_id"/>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/loan_application.js')}}" type="text/javascript"></script>

@stop

