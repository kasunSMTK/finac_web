@extends('layouts.main')
@section('content')
    <div class="container-fluid">

    {{--editable Page--}}
    {{--
            <div class="card-body">

            </div>--}}

    <!-- Page Heading -->
        <h6 class="h6 mb-2 text-gray-800">Loan Application Manager</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="float-right">

                    <a id="btnAddNew" class="btn btn-sm btn-info add-new" href="{{url('loan_application/create')}}"><i
                                class="fa fa-plus"></i> Add New</a>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($loanAppDetails as $loanList)
                            <tr>
                                <td class="col-md-1">{{$loanList->application_reference_no}}</td>
                                <td class="col-md-1">{{$loanList->application_document_no}}</td>
                                <td class="col-md-1">{{$loanList->clients->name}}</td>
                                <td class="col-md-1">{{$loanList->loan_type}}</td>
                                <td class="col-md-1 text-md-right">{{$loanList->loan_amount}}</td>
                                <td class="col-md-1 text-md-center">{{$loanList->installment_count}}</td>
                                <td class="col-md-1 text-md-right">{{round($loanList->installment_amount,2)}}</td>
                                @if($loanList->status==="APPLICATION_PENDING")
                                    <td class="col-md-1 text-md-center text-success font-weight-bold">P</td>
                                @else
                                    <td class="col-md-1 text-md-center text-warning font-weight-bold">R</td>
                                @endif
                                <td class="col-md-1">
                                    <a type="button" href=""
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                    {{--<a type="button" href="{{url('loan_application/loan/remove',$loanList->index_no)}}"
                                       class="btn btn-sm btn-danger"><i class="fa fa-brush"></i></a>--}}
                                    <a type="button" data-target="#confirmation" data-toggle="modal"
                                       class="btn btn-sm btn-danger text-gray-100"><i class="fa fa-brush"></i></a>
                                    @include('partials.confirmation',$loanList)
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
