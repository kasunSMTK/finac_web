@extends('layouts.main')
@section('content')
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                <div class="row mb-0">
                    <div class="col-sm-6">

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_1" role="tab" aria-selected="true">Loan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_2" role="tab">Charges</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link tab_3" data-toggle="tab" href="#tab_3" role="tab" >Document</a>
                            </li>
                        </ul>

                        <!-- Tab panes {Fade}  -->
                        <div class="tab-content">
                            <div class="tab-pane fade show  active" id="tab_1" name="tab_1" role="tabpanel">
                                @include('transaction.loan_application.loan_approval.tab_pane.tab_1')
                            </div>
                            <div class="tab-pane fade" id="tab_2" name="tab_2" role="tabpanel">
                                @include('transaction.loan_application.loan_approval.tab_pane.tab_2')
                            </div>
                            <div class="tab-pane fade" id="tab_3" name="tab_3" role="tabpanel">
                                @include('transaction.loan_application.loan_approval.tab_pane.tab_3')
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">

                            <div class="p-5">
                                <a id="hlink" target="_blank" href="{{
                                URL("user/".isset($loanList->clientString->code)?
                                "user/image/".$loanList->clientString->code.".png": "")}}" data-toggle="lightbox">


                                    <img id="image" src="{{
                                URL("user/".isset($loanList->clientString->code)?
                                "user/image/".$loanList->clientString->code.".png": URL("user/profile_picture.png"))}}"
                                         class="img-fluid rounded-circle img-thumbnail"
                                         style="width:200px;height:200px">
                                </a>
                                <br>
                                <a id="user_link" target="_blank" href="{{route('clients.edit',$loanList->clientString->code)}}" >
                                    {{$loanList->clientString->name}}</a>
                            </div>

                        </div>


                </div>
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('js/application/loan_application_approval.js')}}" type="text/javascript"></script>

@stop

