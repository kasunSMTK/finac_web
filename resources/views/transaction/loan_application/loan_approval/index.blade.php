@extends('layouts.main')
@section('content')
    <div class="container-fluid">

    <!-- Page Heading -->
        <h6 class="h6 mb-2 text-gray-800">Loan Application Approval</h6>

        <!-- DataTales Example -->
{{--        {!! Form::model([url('loan_application_approval/store')]) !!}--}}
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($approval_list as $loanList)
                            <tr>
                                <td class="col-sm-1">{{$loanList->application_reference_no}}</td>
                                <td class="col-md-1">{{$loanList->application_document_no}}</td>
                                <td class="col-md-1">{{$loanList->clientString->name}}</td>
                                <td class="col-md-1">{{$loanList->loan_type}}</td>
                                <td class="col-md-1 text-md-right">{{$loanList->loan_amount}}</td>
                                <td class="col-md-1 text-md-center">{{$loanList->installment_count}}</td>
                                <td class="col-md-1 text-md-right">{{round($loanList->installment_amount,2)}}</td>
                                <td class="col-md-1">
                                    <input type="hidden" id="index_no" value="{{$loanList->index_no}}"/>
                                   {{-- <input type="hidden" id="mod_client_des" value="{{$loanList->clientString->code.' - '.$loanList->clientString->name}}"/>
                                    <input type="hidden" id="mod_application_reference_no" value="{{$loanList->application_reference_no}}"/>--}}{{--
                                    --}}
                                    <a   href="{{url('loan_application_approval/edit',$loanList->index_no)}}"
                                       class="btn btn-sm btn-info text-gray-100"><i class="fa fa-info"></i></a>

                                    <a type="button"  data-target="{{'#loan_approval_'.$loanList->index_no}}" data-toggle="modal"
                                       class="btn btn-sm btn-success text-gray-100"><i class="fa fa-save"></i></a>
                                    @include('transaction.loan_application.loan_approval.loanApproveAction',$loanList)
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        {{--{!! Form::close() !!}--}}

    </div>

@stop
