{{ csrf_field() }}
<div class="modal" id="{{'loan_approval_'.$loanList->index_no}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Loan Approval</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 pb-0">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('client','Customer : '.$loanList->clientString->name,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('application_transaction_date','Transaction : '.$loanList->application_transaction_date,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('application_reference_no','Ref No : '.$loanList->application_reference_no,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('loan_type','Loan Product : '.$loanList->loanTypeString->name,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('loan_amount','Loan Amount : '.$loanList->loan_amount,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('interest_rate','Interest Rate : '.$loanList->interest_rate,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                        <div class="form-group mb-sm-1">
                            {!! Form::label('installment_count','Installment : '.$loanList->installment_count.' * '.$loanList->installment_amount,['class'=>'label-info mb-sm-0']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <a id="btnReject" href="{{url('loan_application_approval/reject',$loanList->index_no)}}" class="btn btn-danger text-gray-100" >Reject</a> {{--data-dismiss="modal"--}}
                <a id="btnApprove" href="{{url('loan_application_approval/approve',$loanList->index_no)}}" class="btn btn-success text-gray-100" >Approve</a> {{--data-dismiss="modal"--}}
            </div>


        </div>
    </div>

</div>
@section('alert')

@stop
@section('application_js')
    <script src="{{asset('/js/application/loan_application_approval.js')}}" type="text/javascript"></script>

@stop
