<div class="row">
    <div class="col-sm-12 p-sm-4 ">
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('client','Customer : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('client',$loanList->clientString->name,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('client','Full Name : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('client',$loanList->clientString->long_name,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('application_transaction_date','Transaction : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('application_transaction_date',$loanList->application_transaction_date,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1 ">
                {!! Form::label('application_reference_no','Ref No : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('application_reference_no',$loanList->application_reference_no,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('loan_type','Loan Product : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('loan_type',$loanList->loanTypeString->name,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1 ">
                {!! Form::label('loan_amount','Loan Amount : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('loan_amount',$loanList->loan_amount,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('interest_rate','Interest Rate : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('interest_rate',$loanList->interest_rate,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('installment_count','Installment : ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('installment_count',$loanList->installment_count.' * '.$loanList->installment_amount,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('panalty_rate','Penalty (Rate/Amount): ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('panalty_rate',$loanList->panalty_rate.'%  , '.$loanList->panalty_amount,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>
        <div class="col-sm-12 border-bottom ">
            <div class="form-group mb-sm-1">
                {!! Form::label('note','Note: ',['class'=>'label-info mb-sm-0']) !!}
                {!! Form::label('note',$loanList->note,['class'=>'label-info mb-sm-0 text-success font-weight-bold']) !!}
            </div>

        </div>

    </div>
</div>
