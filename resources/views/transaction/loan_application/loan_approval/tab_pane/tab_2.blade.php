<div class="p-0">
    <div class="text-center">
        <h9 class="h9 text-gray-800 mb-4">Loan Charges Details</h9>
    </div>

    <div class="table-sm">
        <table id="table_schemes" class="table-sm table-bordered" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th class="attrName">Date</th>
                <th class="attrName">Description</th>
                <th class="attrName">Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($settlements as $settlementList )

                <tr>
                    <td>{{$settlementList->due_date}}</td>
                    <td>{{$settlementList->description}}</td>
                    <td>{{$settlementList->amount}}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

</div>