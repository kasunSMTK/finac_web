<div class="p-0">
    <div class="text-center">
        <h9 class="h9 text-gray-800 mb-4">Loan Document Details</h9>
    </div>

    <div class="table-sm">
        <table id="table_schemes" class="table-sm table-bordered" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th class="attrName">Description</th>
                <th class="attrName">Link</th>
            </tr>
            </thead>
            <tbody>
            @foreach($doc_details as $documentsList )

                <tr>
                    <td>{{$documentsList->description}}</td>
                    <td class="text-md-center">
                        <a target="_blank" href="{{ URL("user/document/".$loanList->application_reference_no.'/'.$documentsList->code.'.png')}}" data-toggle="lightbox"
                           data-title="Document Name : ".{{$documentsList->name}}
                           data-footer="Finac Image Viewer">
                            <img src="{{ URL("user/document/".$loanList->application_reference_no.'/'.$documentsList->code.'.png')}}" class="img-fluid"
                                 style="width:100px;max-width:100px">
                        </a>
                    </td>
                </tr>

            @endforeach


            </tbody>
        </table>
    </div>

</div>