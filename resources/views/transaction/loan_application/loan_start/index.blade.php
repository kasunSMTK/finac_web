@extends('layouts.main')
@section('content')
    <div class="container-fluid">

    {{--editable Page--}}
    {{--
            <div class="card-body">

            </div>--}}

    <!-- Page Heading -->
        <h6 class="h6 mb-2 text-gray-800">Loan Start</h6>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Reference</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($loan_start_list as $loanList)
                            <tr>
                                <td class="col-md-1">{{$loanList->application_reference_no}}</td>
                                <td class="col-md-1">{{$loanList->application_document_no}}</td>
                                <td class="col-md-1">{{$loanList->clients->code.'  '.$loanList->clients->name}}</td>
                                <td class="col-md-1">{{$loanList->loan_type}}</td>
                                <td class="col-md-1 text-md-right">{{$loanList->loan_amount}}</td>
                                <td class="col-md-1 text-md-center">{{$loanList->installment_count}}</td>
                                <td class="col-md-1 text-md-right">{{round($loanList->installment_amount,2)}}</td>
                                <td class="col-md-1 text-md-center text-success font-weight-bold">Approved</td>

                                <td class="col-md-1">
                                    <a id="open_dialog" type="button" data-target="{{'#loan_start_confirmation_'.$loanList->index_no}}" data-toggle="modal"
                                       class="btn btn-sm btn-success text-gray-100"><i class="fa fa-save"></i></a>
                                    @include('partials.LoanStartAlert',$loanList)
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@stop
@section('application_js')
    <script src="{{asset('/js/application/loan_start.js')}}" type="text/javascript"></script>

@stop

