{{--//guarantor--}}
<div class="text-left">
    <h8 class="h8 text-gray-600 mb-4">Add Guarantor</h8>
</div>
<div class="card-header py-3">
    <div class="float-right">
        <a id="btnAddGuarantor" class="btn btn-sm btn-info add-new text-gray-100"  data-target="#myModal" data-toggle="modal"><i
                    class="fa fa-plus "></i> Add Guarantor</a>
    </div>
</div>
<div class="card-body">
    <div class="table-sm">
        <table id="tbl_guarantor" class="table-sm table-bordered"  width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@include('commonForm.CustomerDisplay')


{{--
<div class="p-sm-3 float-right">
    <button type="submit" class="btn  btn-success "><i class="fa fa-save"></i>{{$button_caption}}</button>
    <button type="button" class="btn  btn-danger"><i class="fa fa-refresh"></i>Reset</button>

</div>--}}
