{{--add new document--}}
<div class="text-left">
    <h8 class="h8 text-gray-600 mb-4">Add Documents</h8>
</div>
<div class="card-header py-3">
    <div class="float-right">
        <a id="btnAddDocument" class="btn btn-sm btn-info add-new text-gray-100"  data-target="#myModal2" data-toggle="modal"><i
                    class="fa fa-plus "></i> Add New Doc</a>
    </div>
</div>
<div class="card-body">
    <div class="table-sm">
        <table id="tblAddDocument" class="table-sm table-bordered"  width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Description</th>
                <th>Ref Link</th>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@include('commonForm.LoanDocumentAdd')