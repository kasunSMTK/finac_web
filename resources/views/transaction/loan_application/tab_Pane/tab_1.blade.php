<div class="p-0">
    <div class="text-center">
        <h9 class="h9 text-gray-800 mb-4">Loan Application Details</h9>
    </div>

    {!! Form::model($loan, ['url' => ['loan_application']]) !!}

    @include('transaction.loan_application._form')

    {!! Form::close() !!}

</div>