<div class="p-0">
    <div class="text-center">
        <h9 class="h9 text-gray-800 mb-4">Loan Schedule</h9>
        <div class="float-right">
            <a id="btnSchedule" class="btn btn-sm btn-light " ><i
                        class="fa fa-refresh"></i>View Schedule</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-sm table-wrapper-scroll-y my-custom-scrollbar">
            <table id="table_schedule" class="table-sm table-bordered table-striped"  width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th> Date</th>
                    <th>#Inst</th>
                    <th class="text-sm-right">capital</th>
                    <th class="text-sm-right">interest</th>
                    <th class="text-sm-right">Amount</th>
                </tr>
                </thead>
             {{--   <tfoot>
                <tr>
                    <th>Total:</th>
                    <th>-</th>
                    <th>2000</th>
                    <th>1000</th>
                    <th>3000</th>
                </tr>
                </tfoot>--}}
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="table-sm ">
            <table id="table_schedule_footer" class="table-sm table-bordered table-striped"  width="100%" cellspacing="0">
                <tbody>

                </tbody>
            </table>
         {{--   {{ Form::hidden('invisible','', array('id' => 'invisible_id')) }}--}}

        </div>

    </div>
</div>