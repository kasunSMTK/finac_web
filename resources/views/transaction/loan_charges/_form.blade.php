{{ csrf_field() }}
<input type="hidden" name="transaction_type" id="transaction_type" value="{{$transaction_type}}"/>
<div class="row pb-0">
    <div class="col-sm-6 pb-0 border border-bottom-0 border-right-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('reference_no','Reference No ',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('reference_no', $referenceNo,['class'=>'form-control form-control-sm text-primary','readonly'=>'true']) !!}
        </div>
    </div>
    <div class="col-sm-6 border border-bottom-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('transaction_date','Transaction Date',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('transaction_date',Session::has('working_date')? Session::get('working_date') :'',['class'=>'form-control form-control-sm text-primary','readonly'=>'true','id'=>'transaction_date']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 border border-top-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('agreement_no','Agreement No',['class'=>'label-info mb-sm-0']) !!}
        <span id="span_agreement_no" class="alert-danger"> {{ $errors->first('agreement_no') }}</span>
        {!! Form::text('agreement_no','',['class'=>'form-control form-control-sm text-primary','id'=>'agreement_no']) !!}
    </div>
    </div>
    <input type="hidden" id="loan_index" name="loan_index"/>
    <div class="col-sm-6 border border-top-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('document_no','Document NO',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('document_no', '',['class'=>'form-control form-control-sm text-primary']) !!}
        </div>

    </div>
    <span id="agreement_details"></span>
</div>
<br>














