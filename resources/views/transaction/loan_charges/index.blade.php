@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">Loan Charge</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::open(['url' => ['loan_charge/save'] ,'id'=>'loan_charge']) !!}
                <div class="row mb-0">
                    <div class="col-sm-12">

                        {{--  <a href="{{url('payments/reports/print/ ' . 13292 .'/'.'RECEIPT')}}" name="ss" target="_blank" >reports</a>--}}
                        @include('transaction.loan_charges._form')
                        @include('commonForm.searchModel')
                        @include('commonForm.commonSearchModel')
                        @include('commonForm.ConfirmationMessage')

                    </div>
                    <div class="col-sm-12 mb-2">
                        <div class="float-right">

                            <a id="btnAddRaw" class="btn btn-sm btn-info text-gray-100 add-new"><i class="fa fa-plus"></i> Add New</a>

                        </div>
                    </div>
                    <div class="col-sm-12">


                        <div class="card shadow mb-4">

                            <div class="table-responsive receipt-custom-scrollbar-300">
                                <table name="table_settlement" id="table_settlement" class="table-sm table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th width="30%">Account</th>
                                        <th width="50%">Description</th>
                                        <th width="20%" class="text-md-right">Amount</th>

                                    </tr>
                                    </thead>

                                    <tbody>


                                    </tbody>

                                </table>


                            </div>
                            <div class="row">
                                <div class="col-sm-7">

                                </div>
                                <div class="col-sm-2 float-right">
                                    Total Amount:
                                </div>
                                <div class="col-sm-3 float-right">
                                    {!! Form::text('total_amount', '',['class'=>'form-control form-control-sm text-primary text-md-right','id'=>'total_amount','readonly']) !!}
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group mb-sm-1">
                                    {!! Form::label('note','Note',['class'=>'label-info mb-sm-0']) !!}
                                    {!! Form::text('note', '',['class'=>'form-control form-control-sm text-primary text-md-left']) !!}
                                </div>
                            </div>
                            <input type="hidden" id="settlement"  name="settlement" >
                        </div>
                        <div class="float-right">

                            <button type="button" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i> Save
                            </button>
                            <button type="button" class="btn  btn-primary"><i class="fa fa-refresh"></i>Reset</button>
                        </div>

                    </div>

                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/loan_charges.js')}}" type="text/javascript"></script>

@stop

