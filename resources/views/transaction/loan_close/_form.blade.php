{{ csrf_field() }}
<input type="hidden" name="transaction_type" id="transaction_type" value="{{$transaction_type}}"/>

<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('agreement_no','Agreement No',['class'=>'label-info mb-sm-0']) !!}
        <span id="span_agreement_no" class="alert-danger"> {{ $errors->first('agreement_no') }}</span>
        {!! Form::text('agreement_no','',['class'=>'form-control form-control-sm text-primary','placeholder'=>'Click Here','id'=>'agreement_no']) !!}
    </div>
    <input type="hidden" id="loan_index" name="loan_index"/>
</div>
<div class="row pb-0">
    <div class="col-sm-12 pb-0 ">
        <div class="form-group mb-sm-1">
            {!! Form::label('reference_no','Reference No ',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('reference_no', '',['class'=>'form-control form-control-sm text-primary','readonly'=>'true']) !!}
        </div>
    </div>
</div>

<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('client','Client',['class'=>'label-info mb-sm-0']) !!}
        <span class="icon-case"><i class="fa fa-male"></i></span>
        {!! Form::text('client', '',['class'=>'form-control form-control-sm text-primary','readonly','id'=>'client']) !!}
    </div>
    <input type="hidden" id="client_code" name="client_code"/>

</div>
<div class="row">
    <div class="col-sm-12 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('address_line','Address',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('address_line','',['class'=>'form-control form-control-sm text-primary','readonly','data-live-search'=>'true']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1 ">
            {!! Form::label('loan_amount','Loan Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('loan_amount','',['class'=>'form-control form-control-sm text-md-right text-primary loan_amount','readonly','step'=>'any','id'=>'loan_amount']) !!}
        </div>

    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('loan_balance','Loan Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('loan_balance','',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('penalty_balance','Penalty Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('penalty_balance', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('installment_amount','Installment Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('installment_amount', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any','id'=>'installment_amount']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::label('cap_bal','Capital Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('cap_bal', '',['class'=>'form-control form-control-sm text-primary text-md-right ','readonly']) !!}
        </div>
    </div>
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::label('int_bal','Interest Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('int_bal', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group mb-sm-1">
            {!! Form::label('des','Description',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('des', '',['class'=>'form-control form-control-sm text-primary text-md-left']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::radio('rdo_close[]','settlement_close',false,['id'=>'settlement_close']) !!}
            {!! Form::label('lb_close1','Settlement Close',['class'=>'label-info mb-sm-0']) !!}
        </div>
        <div class="form-group mb-sm-1">
            {!! Form::radio('rdo_close[]','suspend_close',false,['id'=>'suspend_close']) !!}
            {!! Form::label('lb_close2','Suspend Close',['class'=>'label-info mb-sm-0']) !!}
        </div>
    </div>
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::radio('rdo_close[]','reschedule_close',false,['id'=>'reschedule_close']) !!}
            {!! Form::label('lb_close3','Reschedule Close',['class'=>'label-info mb-sm-0']) !!}
        </div>
        <div class="form-group mb-sm-1">
            {!! Form::radio('rdo_close[]','error_close',false,['id'=>'error_close']) !!}
            {!! Form::label('lb_close4','Error Close',['class'=>'label-info mb-sm-0 text-danger']) !!}
        </div>

    </div>
</div>
<div class="row pt-1">
    {!! Form::label('note','Reason',['class'=>'label-info mb-sm-0']) !!}
    <div class="col-sm-5 pb-0">
            {!! Form::select('note',$reasons,'',['class'=>'form-control form-control-sm','data-live-search'=>'true']) !!}
    </div>
    <div class="col-sm-4 pb-0">
    </div>
</div>
<div class="row">
    <span id="span_rdo_close" class="alert-danger"> {{ $errors->first('rdo_close') }}</span>
</div>
<div class="row">
    <span id="span_message" class="alert-danger"></span>
</div>









