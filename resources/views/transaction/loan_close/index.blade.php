@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">Loan Close</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::model($loan, ['url' => ['loan_close/save'] ,'id'=>'f_payment']) !!}
                <div class="row mb-0">
                    <div class="col-sm-5">

                      {{--  <a href="{{url('payments/reports/print/ ' . 13292 .'/'.'RECEIPT')}}" name="ss" target="_blank" >reports</a>--}}
                        @include('transaction.loan_close._form')
                        @include('commonForm.searchModel')
                        @include('commonForm.ConfirmationMessage')

                    </div>
                    <div class="col-sm-7">
                        <div class="card shadow mb-4">
                            <div class="table-responsive receipt-custom-scrollbar">
                                <table name="table_settlement" id="table_settlement" class="table-sm table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Inst</th>
                                        <th>Description</th>
                                        <th class="text-md-right">Amount</th>
                                        <th class="text-md-right">Balance</th>
                                        <th>Status</th>

                                    </tr>
                                    </thead>

                                    <tbody>


                                    </tbody>
                                </table>


                            </div>

                        </div>
                        <div class="float-right">

                            <button type="submit" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i> Save
                            </button>
                            <button type="button" class="btn  btn-primary"><i class="fa fa-refresh"></i>Reset</button>
                        </div>

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/loan_close.js')}}" type="text/javascript"></script>

@stop

