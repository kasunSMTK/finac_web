@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">Rebate Approve</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::model($loan, ['url' => ['rebate_approve/save'] ,'id'=>'f_payment']) !!}
                <div class="row mb-0">
                    <div class="col-sm-5">

                      {{--  <a href="{{url('payments/reports/print/ ' . 13292 .'/'.'RECEIPT')}}" name="ss" target="_blank" >reports</a>--}}
                        @include('transaction.loan_rebate_approve._form')
                        @include('commonForm.ConfirmationMessage')
                        @include('commonForm.RejectMessage')

                    </div>
                    <div class="col-sm-7">
                        <div class="card shadow mb-4">
                            <div class="table-responsive receipt-custom-scrollbar">
                                <table name="table_settlement" id="table_settlement" class="table-sm table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Inst</th>
                                        <th>Description</th>
                                        <th class="text-md-right">Amount</th>
                                        <th class="text-md-right">Balance</th>
                                        <th class="text-md-right">Settlement</th>

                                    </tr>
                                    </thead>

                                    <tbody>


                                    </tbody>
                                </table>


                            </div>

                        </div>
                        <div class="float-right">

                            <button type="button" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i> Save
                            </button>
                            <button type="button" id="btnReject" class="btn  btn-danger "><i class="fa fa-cross"></i> Reject
                            </button>
                            <button type="button" class="btn  btn-primary"><i class="fa fa-close"></i>Cancel</button>
                        </div>

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/rebate_approve.js')}}" type="text/javascript"></script>

@stop

