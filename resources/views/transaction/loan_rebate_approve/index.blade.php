@extends('layouts.main')
@section('content')
    <div class="container-fluid">

    <!-- Page Heading -->
        <h6 class="h6 mb-2 text-gray-800">Loan Rebate Approval</h6>

        <!-- DataTales Example -->
{{--        {!! Form::model([url('loan_application_approval/store')]) !!}--}}
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>AgreementNo</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>AgreementNo</th>
                            <th>DocumentNo</th>
                            <th>Customer</th>
                            <th>Loan Type</th>
                            <th class="text-md-right">Loan Amount</th>
                            <th class="col-md-1 text-md-center">Inst</th>
                            <th class="text-md-right">Installment</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($approval_list as $loanList)
                            <tr>
                                <td class="col-sm-1">{{$loanList->agreement_no}}</td>
                                <td class="col-md-1">{{$loanList->application_document_no}}</td>
                                <td class="col-md-1">{{$loanList->clientString->name}}</td>
                                <td class="col-md-1">{{$loanList->loan_type}}</td>
                                <td class="col-md-1 text-md-right">{{$loanList->loan_amount}}</td>
                                <td class="col-md-1 text-md-center">{{$loanList->installment_count}}</td>
                                <td class="col-md-1 text-md-right">{{round($loanList->installment_amount,2)}}</td>
                                <td class="col-md-1">
                                    <a type="button" href="{{route('rebate_approval.edit',$loanList->index_no)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        {{--{!! Form::close() !!}--}}

    </div>

@stop
