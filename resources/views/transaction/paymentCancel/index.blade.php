@extends('layouts.main')
@section('content')
    <div class="container-fluid">

        <h6 class="h4 mb-2 text-gray-800">Payment Cancel </h6 >
        <input type="hidden" id="transaction_type" value="{{$transaction_type}}">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">

                <div class="row pt-1">
                    <div class="col-sm-2 pb-0 ">
                        <div class="form-group mb-sm-1">
                            {!! Form::label('transaction_date','Transaction Date : ',['class'=>'label-info mb-sm-0']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group mb-sm-1">
                            <div class="input-group date" data-provide="datepicker">
                                {!! Form::text('transaction_date', $transaction_date,['class'=>'form-control form-control-sm','id'=>'transaction_date']) !!}
                                <span class="input-group-addon">
                                         <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                    <h6 class="h-25 mb-2 text-gray-500 text-right">{{$transaction_type}}</h6>
                    </div>
                </div>

            </div>

            <div class="card-body">
                <div class="table-responsive my-custom-scrollbar">
                    <table id="cancel_table" class="table-sm table-bordered " id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Reference</th>
                            <th>Document</th>
                            <th>Client</th>
                            <th>Agreement</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                       {{-- @foreach($transaction as $cllist)
                            <tr>
                                <td class="col-md-1 p-1">{{$cllist->transaction_date}}</td>
                                <td class="col-md-1 p-1">{{$cllist->reference_no}}</td>
                                <td class="col-md-2 p-1">{{$cllist->document_no}}</td>
                                <td class="col-md-3 p-1 text-nowrap">{{$cllist->clients->toString()}}</td>
                                <td class="col-md-2 p-1">{{$cllist->loans->agreement_no}}</td>
                                <td class="col-md-1 p-1 text-md-right">{{number_format($cllist->amounts['amount'],2)}}</td>
                                <td class="col-md-1 p-1">
                                    <a type="button" href="{{url('cash_payment_cancel/edit',$cllist->index_no)}}"
                                       class="btn btn-sm btn-warning -edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach--}}


                        </tbody>
                    </table>
                    {{--  <div>{{$Clients->links()}}</div>--}}
                </div>
            </div>
        </div>

    </div>
@stop
@section('alert')

@stop
@section('application_js')
    <script src="{{asset('/js/application/transaction_cancel.js')}}" type="text/javascript"></script>
@stop
