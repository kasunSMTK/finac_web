{{ csrf_field() }}
<input type="hidden" name="transaction_type" id="transaction_type" value="{{$transaction_type}}"/>
<div class="row pb-0">
    <div class="col-sm-12 pb-0 border border-bottom-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('reference_no','Reference No ',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('reference_no', $referenceNo,['class'=>'form-control form-control-sm text-primary','readonly'=>'true']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 border border-top-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('transaction_date','Transaction Date',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('transaction_date',Session::has('working_date')? Session::get('working_date') :'',['class'=>'form-control form-control-sm text-primary','readonly'=>'true','id'=>'transaction_date']) !!}
        </div>
    </div>
    <div class="col-sm-6 border border-top-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('document_no','Document NO',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('document_no', '',['class'=>'form-control form-control-sm text-primary']) !!}
        </div>

    </div>
</div>

<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('agreement_no','Agreement No',['class'=>'label-info mb-sm-0']) !!}
        <span id="span_agreement_no" class="alert-danger"> {{ $errors->first('agreement_no') }}</span>
        {!! Form::text('agreement_no','',['class'=>'form-control form-control-sm text-primary','id'=>'agreement_no']) !!}
    </div>
    <input type="hidden" id="loan_index" name="loan_index"/>
</div>
<div class="col-sm-0">
    <div class="form-group mb-sm-1">
        {!! Form::label('client','Client',['class'=>'label-info mb-sm-0']) !!}
        <span class="icon-case"><i class="fa fa-male"></i></span>
        {!! Form::text('client', '',['class'=>'form-control form-control-sm text-primary','readonly','id'=>'client']) !!}
    </div>
    <input type="hidden" id="client_code" name="client_code"/>

</div>
<div class="row">
    <div class="col-sm-12 pb-0">
        <div class="form-group mb-sm-1">
            {!! Form::label('address_line','Address',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('address_line','',['class'=>'form-control form-control-sm text-primary','readonly','data-live-search'=>'true']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1 ">
            {!! Form::label('loan_amount','Loan Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('loan_amount','',['class'=>'form-control form-control-sm text-md-right text-primary loan_amount','readonly','step'=>'any','id'=>'loan_amount']) !!}
        </div>

    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('loan_balance','Loan Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('loan_balance','',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('penalty_balance','Penalty Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('penalty_balance', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('installment_amount','Installment Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('installment_amount', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any','id'=>'installment_amount']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group mb-sm-1  ">
            {!! Form::label('payment_amount','Payment Amount',['class'=>'label-info mb-sm-0']) !!}
            <span class="icon-case text-success"><i class="fa fa-coins"></i></span>
            {!! Form::number('payment_amount', '',['class'=>'form-control form-control-sm text-primary text-lg-right border border-success','step'=>'any','min'=>'0']) !!}
        </div>
        <span id="span_payment_amount" class="alert-danger"> {{ $errors->first('payment_amount') }}</span>
        <input type="hidden" name="chq_payment_amount" id="chq_payment_amount"/>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('after_balance','After Balance',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('after_balance', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group mb-sm-1">
            {!! Form::label('advance_amount','Advance Payment',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::number('advance_amount', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','step'=>'any']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::label('areas_vol','Areas Volume',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('areas_vol', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','readonly']) !!}
        </div>
    </div>
    <div class="col-sm-6 border ">
        <div class="form-group mb-sm-1">
            {!! Form::label('areas_amount','Areas Amount',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('areas_amount', '',['class'=>'form-control form-control-sm text-primary text-md-right','readonly','readonly']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group mb-sm-1">
            {!! Form::label('note','Note',['class'=>'label-info mb-sm-0']) !!}
            {!! Form::text('note', '',['class'=>'form-control form-control-sm text-primary text-md-left']) !!}
        </div>
    </div>
</div>

<input type="hidden" name="overAmount" id="overAmount"/>










