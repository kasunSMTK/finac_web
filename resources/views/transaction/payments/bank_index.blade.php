@extends('layouts.main')
@section('content')
    <div class="container">
        <h6 class="h6 mb-2 font-weight-bold text-primary">Bank Payment</h6>
        <div class="card o-hidden border-0 shadow-lg my-1 mt-0">
            <div class="card-body">
                <!-- Nested Row within Card Body -->
                {!! Form::model($loan, ['url' => ['bank_payment/store'] ,'id'=>'f_payment']) !!}
                <div class="row mb-0">
                    <div class="col-sm-5">
                        @include('transaction.payments._form')
                        @include('commonForm.searchModel')
                        @include('commonForm.PaymentOption')

                    </div>
                    <div class="col-sm-7">
                        <div class="card shadow mb-4">
                            <div class="table-responsive receipt-custom-scrollbar">
                                <table name="table_settlement" id="table_settlement" class="table-sm table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Inst</th>
                                        <th>Description</th>
                                        <th class="text-md-right">Amount</th>
                                        <th class="text-md-right">Balance</th>
                                        <th class="text-md-right">Settlement</th>

                                    </tr>
                                    </thead>

                                    <tbody>


                                    </tbody>
                                </table>


                            </div>

                        </div>
                        <div class="float-right">

                            <button type="button" id="btnSave" class="btn  btn-success "><i class="fa fa-save"></i> Save
                            </button>
                            <button type="button" class="btn  btn-primary"><i class="fa fa-refresh"></i>Reset</button>
                        </div>

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop

@section('alert')

@stop

@section('application_js')
    <script src="{{asset('/js/application/searchModel.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/application/cash_receipt.js')}}" type="text/javascript"></script>

@stop