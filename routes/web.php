<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('main_page', 'LoginController@index');
Route::post('main_page/login', 'UserController@login');
Route::post('main_page/logout', 'UserController@logout');
Route::post('/main_page/login/permission', 'UserModuleController@loadPermission');
Route::post('/{name}/main_page/login/permission', 'UserModuleController@loadPermission');
Route::post('/{name}/{id}/main_page/login/permission', 'UserModuleController@loadPermission');
Route::group(['middleware' => ['auth_user']], function () {

    Route::get('loan_applications', 'ComApplicationController@referenceNoGenerate');

//loan Application
    Route::resource('loan_application', 'LoanApplicationController');
    Route::post('loan_application/store', 'LoanApplicationController@store');
    Route::post('loan_application/loan_types/get_loan_type', 'LoanTypeController@getLoanType');
    Route::post('loan_application/charge_scheme/get_loan_scheme', 'LoanTypeChargeSchemeController@getLoanCharge');
    Route::post('loan_application/leave_day/getLeaveDays', 'CompanyLeaveDayController@getLeaveDays');
    Route::post('loan_application/clients/getClients', 'ClientController@getClients');
    Route::post('loan_application/file/upload', 'DocumentController@upload');
    Route::get('loan_application/loan/remove/{index_no}', 'LoanApplicationController@remove');
    Route::post('loan_application/get_loan_types_obj', 'LoanTypeController@loanTypeObj');

//loan Application Approval


    Route::get('loan_application_approval', 'LoanApplicationApprovalController@index');
    Route::post('loan_application_approval/bind/data', 'LoanApplicationApprovalController@targetView');
    Route::get('loan_application_approval/approve/{index_no}', 'LoanApplicationApprovalController@approve');
    Route::get('loan_application_approval/reject/{index_no}', 'LoanApplicationApprovalController@reject');
    Route::get('loan_application_approval/edit/{index_no}', 'LoanApprovalDetailController@edit');

//loan Start
    Route::get('loan_start', 'LoanStartController@index');
    Route::post('loan_start/store', 'LoanStartController@store');


    /*Route::post('user/login', 'LoginController@store');*/
//client


    Route::resource('clients', 'ClientController');
    Route::post('clients/save', 'ClientController@store');
    Route::put('clients/edit/{code}', 'ClientController@update');

//payments

    Route::get('cash_payment', 'CashPaymentController@index');
    Route::post('cash_payment/data_set', 'ComApplicationController@getData');
    Route::post('cash_payment/store', 'CashPaymentController@store');
  //  Route::post('payments/reports/print', 'ReportGenerateController@reports');
    Route::get('payments/reports/print/{transaction_no}/{transaction_type}', 'ReportGenerateController@reports');

//bank payment
    Route::get('bank_payment', 'BankPaymentController@index');
    Route::post('bank_payment/store', 'BankPaymentController@store');

    // cheque payment
    Route::get('cheque_payment', 'TemporaryReceiptController@index');
    Route::post('cheque_payment/store', 'TemporaryReceiptController@store');
    Route::get('cheque_payment_issue', 'IssueReceiptChequeController@index');
    Route::post('cheque_payment_issue/store', 'IssueReceiptChequeController@store');

    //check transactions
    Route::get('cheque_deposit','ChequeDepositController@index');
    Route::post('cheque_transaction/table_data','ChequeTransactionController@generateTopTableData');
    Route::post('cheque_transaction/save_data','ChequeTransactionController@store');

    Route::get('cheque_realize','ChequeRealizeController@index');
    Route::get('cheque_return','ChequeReturnController@index');


    Route::get('cash_receipt_cancel','TransactionCancelController@indexCash');
    Route::post('payment_cancel/RECEIPT','TransactionCancelController@indexCash');
    Route::get('payment_cancel/RECEIPT/edit/{transaction_type}/{index}/{a}/{b}/{c}','TransactionCancelController@cancel');


    Route::get('bank_receipt_cancel','TransactionCancelController@indexBank');
    Route::post('payment_cancel/BANK_RECEIPT','TransactionCancelController@indexBank');
    Route::get('payment_cancel/BANK_RECEIPT/edit/{transaction_type}/{index}/{a}/{b}/{c}','TransactionCancelController@cancel');

    Route::get('temp_receipt_cancel','TransactionCancelController@indexTemp');
    Route::post('payment_cancel/TEMP_RECEIPT','TransactionCancelController@indexTemp');
    Route::get('payment_cancel/TEMP_RECEIPT/edit/{transaction_type}/{index}/{a}/{b}/{c}','TransactionCancelController@cancel');

    Route::get('loan_voucher','VoucherController@index');
    Route::POST('loan_voucher/data_voucher','ComApplicationController@getDataVoucher');
    Route::POST('loan_voucher/save','VoucherController@store');


    Route::get('loan_charge','LoanChargeController@index');
    Route::post('loan_charge/save','LoanChargeController@store');


    Route::get('rebate','RebateController@index');
    Route::post('rebate/data_set', 'ComApplicationController@getDataRebate');
    Route::post('rebate/save', 'RebateController@store');

    Route::get('rebate_approval','RebateApproveController@index');
    Route::resource('rebate_approval','RebateApproveController');
    Route::post('rebate_approval/{index}/edit/dataSet','RebateApproveController@dataSet');
    Route::post('rebate_approve/save','RebateApproveController@store');

    Route::get('loan_close','LoanCloseController@index');
    Route::post('loan_close/data_set', 'ComApplicationController@getData');
    Route::post('loan_close/save', 'LoanCloseController@store');

    Route::post('notification/get', 'NotificationController@get');
    Route::post('notification/read', 'NotificationController@read');


    Route::get('loan_report', 'ReportGenerateController@reportView');
    Route::post('report_view/loan','ReportGenerateController@loan');
    Route::post('reports/print', 'ReportGenerateController@reportPdf');

});




